import numpy as np
import temex_structures as ts

class temexd:
    # Local structures

    const_ = ts.const_class()
    randsd_ = ts.randsd_class()
    wlk_ = ts.wlk_class()
    dvec_ = ts.dvec_class()
    teproc_ = ts.teproc_class()
    pv_ = ts.pv_class()


    # Table of constant values */

    c__50 = 50
    c__12 = 12
    c__21 = 21
    c__153 = 153
    c__586 = 586
    c__139 = 139
    c__6 = 6
    c__1 = 1
    c__0 = 0
    c__41 = 41
    c__2 = 2
    c__3 = 3
    c__4 = 4
    c__5 = 5
    c__7 = 7
    c__8 = 8
    c_b73 = 1.1544
    c_b74 = .3735
    c__9 = 9
    c__10 = 10
    c__11 = 11
    c_b123 = 4294967296.

    # Main TE functions
    def teinit(self, nn, time, yy, yp):
        '''

        /*       Initialization */

        /*         Inputs: */

        /*           NN   = Number of differential equations */

        /*         Outputs: */

        /*           Time = Current time(hrs) */
        /*           YY   = Current state values */
        /*           YP   = Current derivative values */

        /*  MEASUREMENT AND VALVE COMMON BLOCK */


        /*   DISTURBANCE VECTOR COMMON BLOCK */

        /* 	NOTE: I have included isd in the /IDV/ common.  This is set */
        /* 		non-zero when the process is shutting down. */
        /* 		Output XMEAS(42) is for cost [cents/kmol product]. */
        /* 		Output XMEAS(43) is production rate of G [kmol G generated/h] */
        /* 		Output XMEAS(44) is production rate of H [kmol H generated/h] */
        /* 		Output XMEAS(45) is production rate of F [kmol F generated/h] */
        /* 		Output XMEAS(46) is partial pressure of A in reactor [kPa] */
        /* 		Output XMEAS(47) is partial pressure of C in reactor [kPa] */
        /* 		Output XMEAS(48) is partial pressure of D in reactor [kPa] */
        /* 		Output XMEAS(49) is partial pressure of E in reactor [kPa] */
        /* 		Output XMEAS(50) is true (delay free) mole % G in product */
        /* 		Output XMEAS(51) is true (delay free) mole % H in product */

        :param nn:
        :param time:
        :param yy:
        :param yp:
        :return:
        '''
        i__ = 0

        # / *Function Body * /
        self.const_.xmw[0] = 2.
        self.const_.xmw[1] =25.4
        self.const_.xmw[2] = 28.
        self.const_.xmw[3] = 32.
        self.const_.xmw[4] = 46.
        self.const_.xmw[5] = 48.
        self.const_.xmw[6] = 62.
        self.const_.xmw[7] = 76.
        self.const_.avp[0] = 0.
        self.const_.avp[1] = 0.
        self.const_.avp[2] = 0.
        self.const_.avp[3] = 15.92
        self.const_.avp[4] = 16.35
        self.const_.avp[5] = 16.35
        self.const_.avp[6] = 16.43
        self.const_.avp[7] = 17.21
        self.const_.bvp[0] = 0.
        self.const_.bvp[1] = 0.
        self.const_.bvp[2] = 0.
        self.const_.bvp[3] =  - 1444.
        self.const_.bvp[4] =  - 2114.
        self.const_.bvp[5] =  - 2114.
        self.const_.bvp[6] =  - 2748.
        self.const_.bvp[7] =  - 3318.
        self.const_.cvp[0] = 0.
        self.const_.cvp[1] = 0.
        self.const_.cvp[2] = 0.
        self.const_.cvp[3] = 259.
        self.const_.cvp[4] = 265.5
        self.const_.cvp[5] = 265.5
        self.const_.cvp[6] = 232.9
        self.const_.cvp[7] = 249.6
        self.const_.ad[0] = 1.
        self.const_.ad[1] = 1.
        self.const_.ad[2] = 1.
        self.const_.ad[3] = 23.3
        self.const_.ad[4] = 33.9
        self.const_.ad[5] = 32.8
        self.const_.ad[6] = 49.9
        self.const_.ad[7] = 50.5
        self.const_.bd[0] = 0.
        self.const_.bd[1] = 0.
        self.const_.bd[2] = 0.
        self.const_.bd[3] =  - .07
        self.const_.bd[4] =  - .0957
        self.const_.bd[5] =  - .0995
        self.const_.bd[6] =  - .0191
        self.const_.bd[7] =  - .0541
        self.const_.cd[0] = 0.
        self.const_.cd[1] = 0.
        self.const_.cd[2] = 0.
        self.const_.cd[3] =  - 2e-4
        self.const_.cd[4] =  - 1.52e-4
        self.const_.cd[5] =  - 2.33e-4
        self.const_.cd[6] =  - 4.25e-4
        self.const_.cd[7] =  - 1.5e-4
        self.const_.ah[0] = 1e-6
        self.const_.ah[1] = 1e-6
        self.const_.ah[2] = 1e-6
        self.const_.ah[3] = 9.6e-7
        self.const_.ah[4] = 5.73e-7
        self.const_.ah[5] = 6.52e-7
        self.const_.ah[6] = 5.15e-7
        self.const_.ah[7] = 4.71e-7
        self.const_.bh[0] = 0.
        self.const_.bh[1] = 0.
        self.const_.bh[2] = 0.
        self.const_.bh[3] = 8.7e-9
        self.const_.bh[4] = 2.41e-9
        self.const_.bh[5] = 2.18e-9
        self.const_.bh[6] = 5.65e-10
        self.const_.bh[7] = 8.7e-10
        self.const_.ch[0] = 0.
        self.const_.ch[1] = 0.
        self.const_.ch[2] = 0.
        self.const_.ch[3] = 4.81e-11
        self.const_.ch[4] = 1.82e-11
        self.const_.ch[5] = 1.94e-11
        self.const_.ch[6] = 3.82e-12
        self.const_.ch[7] = 2.62e-12
        self.const_.av[0] = 1e-6
        self.const_.av[1] = 1e-6
        self.const_.av[2] = 1e-6
        self.const_.av[3] = 8.67e-5
        self.const_.av[4] = 1.6e-4
        self.const_.av[5] = 1.6e-4
        self.const_.av[6] = 2.25e-4
        self.const_.av[7] = 2.09e-4
        self.const_.ag[0] = 3.411e-6
        self.const_.ag[1] = 3.799e-7
        self.const_.ag[2] = 2.491e-7
        self.const_.ag[3] = 3.567e-7
        self.const_.ag[4] = 3.463e-7
        self.const_.ag[5] = 3.93e-7
        self.const_.ag[6] = 1.7e-7
        self.const_.ag[7] = 1.5e-7
        self.const_.bg[0] = 7.18e-10
        self.const_.bg[1] = 1.08e-9
        self.const_.bg[2] = 1.36e-11
        self.const_.bg[3] = 8.51e-10
        self.const_.bg[4] = 8.96e-10
        self.const_.bg[5] = 1.02e-9
        self.const_.bg[6] = 0.
        self.const_.bg[7] = 0.
        self.const_.cg[0] = 6e-13
        self.const_.cg[1] = -3.98e-13
        self.const_.cg[2] = -3.93e-14
        self.const_.cg[3] = -3.12e-13
        self.const_.cg[4] = -3.27e-13
        self.const_.cg[5] = -3.12e-13
        self.const_.cg[6] = 0.
        self.const_.cg[7] = 0.
        # Здесь может быть косяк, т.к. в оригинальном коде на си здесь индексация идет с 1. Хотя в фортране тоже.
        #
        yy[0] = 10.40491389
        yy[1] = 4.363996017
        yy[2] = 7.570059737
        yy[3] = .4230042431
        yy[4] = 24.15513437
        yy[5] = 2.942597645
        yy[6] = 154.3770655
        yy[7] = 159.186596
        yy[8] = 2.808522723
        yy[9] = 63.75581199
        yy[10] = 26.74026066
        yy[11] = 46.38532432
        yy[12] = .2464521543
        yy[13] = 15.20484404
        yy[14] = 1.852266172
        yy[15] = 52.44639459
        yy[16] = 41.20394008
        yy[17] = .569931776
        yy[18] = .4306056376
        yy[19] = .0079906200783
        yy[20] = .9056036089
        yy[21] = .016054258216
        yy[22] = .7509759687
        yy[23] = .088582855955
        yy[24] = 48.27726193
        yy[25] = 39.38459028
        yy[26] = .3755297257
        yy[27] = 107.7562698
        yy[28] = 29.77250546
        yy[29] = 88.32481135
        yy[30] = 23.03929507
        yy[31] = 62.85848794
        yy[32] = 5.546318688
        yy[33] = 11.92244772
        yy[34] = 5.555448243
        yy[35] = .9218489762
        yy[36] = 94.59927549
        yy[37] = 77.29698353
        yy[38] = 63.05263039
        yy[39] = 53.97970677
        yy[40] = 24.64355755
        yy[41] = 61.30192144
        yy[42] = 22.21
        yy[43] = 40.06374673
        yy[44] = 38.1003437
        yy[45] = 46.53415582
        yy[46] = 47.44573456
        yy[47] = 41.10581288
        yy[48] = 18.11349055
        yy[49] = 50.
        # for (i__ = 1 i__ <= 12 ++i__)
        for i__ in range(1, 13):
            self.pv_.xmv[i__ - 1] = yy[i__ + 38 -1]
            self.teproc_.vcv[i__ - 1] = self.pv_.xmv[i__ - 1]
            self.teproc_.vst[i__ - 1] = 2.
            self.teproc_.ivst[i__ - 1] = 0
            # / * L200: * /

        self.teproc_.vrng[0] = 400.
        self.teproc_.vrng[1] = 400.
        self.teproc_.vrng[2] = 100.
        self.teproc_.vrng[3] = 1500.
        self.teproc_.vrng[6] = 1500.
        self.teproc_.vrng[7] = 1e3
        self.teproc_.vrng[8] = .03
        self.teproc_.vrng[9] = 1e3
        self.teproc_.vrng[10] = 1200.
        self.teproc_.vtr = 1300.
        self.teproc_.vts = 3500.
        self.teproc_.vtc = 156.5
        self.teproc_.vtv = 5e3
        self.teproc_.htr[0] = .06899381054
        self.teproc_.htr[1] = .05
        self.teproc_.hwr = 7060.
        self.teproc_.hws = 11138.
        self.teproc_.sfr[0] = .995
        self.teproc_.sfr[1] = .991
        self.teproc_.sfr[2] = .99
        self.teproc_.sfr[3] = .916
        self.teproc_.sfr[4] = .936
        self.teproc_.sfr[5] = .938
        self.teproc_.sfr[6] = .058
        self.teproc_.sfr[7] = .0301
        self.teproc_.xst[0] = 0.
        self.teproc_.xst[1] = 1e-4
        self.teproc_.xst[2] = 0.
        self.teproc_.xst[3] = .9999
        self.teproc_.xst[4] = 0.
        self.teproc_.xst[5] = 0.
        self.teproc_.xst[6] = 0.
        self.teproc_.xst[7] = 0.
        self.teproc_.tst[0] = 45.
        self.teproc_.xst[8] = 0.
        self.teproc_.xst[9] = 0.
        self.teproc_.xst[10] = 0.
        self.teproc_.xst[11] = 0.
        self.teproc_.xst[12] = .9999
        self.teproc_.xst[13] = 1e-4
        self.teproc_.xst[14] = 0.
        self.teproc_.xst[15] = 0.
        self.teproc_.tst[1] = 45.
        self.teproc_.xst[16] = .9999
        self.teproc_.xst[17] = 1e-4
        self.teproc_.xst[18] = 0.
        self.teproc_.xst[19] = 0.
        self.teproc_.xst[20] = 0.
        self.teproc_.xst[21] = 0.
        self.teproc_.xst[22] = 0.
        self.teproc_.xst[23] = 0.
        self.teproc_.tst[2] = 45.
        self.teproc_.xst[24] = .485
        self.teproc_.xst[25] = .005
        self.teproc_.xst[26] = .51
        self.teproc_.xst[27] = 0.
        self.teproc_.xst[28] = 0.
        self.teproc_.xst[29] = 0.
        self.teproc_.xst[30] = 0.
        self.teproc_.xst[31] = 0.
        self.teproc_.tst[3] = 45.
        self.teproc_.cpflmx = 280275.
        self.teproc_.cpprmx = 1.3
        self.teproc_.vtau[0] = 8.
        self.teproc_.vtau[1] = 8.
        self.teproc_.vtau[2] = 6.
        self.teproc_.vtau[3] = 9.
        self.teproc_.vtau[4] = 7.
        self.teproc_.vtau[5] = 5.
        self.teproc_.vtau[6] = 5.
        self.teproc_.vtau[7] = 5.
        self.teproc_.vtau[8] = 120.
        self.teproc_.vtau[9] = 5.
        self.teproc_.vtau[10] = 5.
        self.teproc_.vtau[11] = 5.
        # for (i__ = 1 i__ <= 12 ++i__)
        for i__ in range (1, 13):
            self.teproc_.vtau[i__ - 1] /= 3600.
            # / * L300: * /

        self.randsd_.g = 1431655765.
        self.teproc_.xns[0] = .0012
        self.teproc_.xns[1] = 18.
        self.teproc_.xns[2] = 22.
        self.teproc_.xns[3] = .05
        self.teproc_.xns[4] = .2
        self.teproc_.xns[5] = .21
        self.teproc_.xns[6] = .3
        self.teproc_.xns[7] = .5
        self.teproc_.xns[8] = .01
        self.teproc_.xns[9] = .0017
        self.teproc_.xns[10] = .01
        self.teproc_.xns[11] = 1.
        self.teproc_.xns[12] = .3
        self.teproc_.xns[13] = .125
        self.teproc_.xns[14] = 1.
        self.teproc_.xns[15] = .3
        self.teproc_.xns[16] = .115
        self.teproc_.xns[17] = .01
        self.teproc_.xns[18] = 1.15
        self.teproc_.xns[19] = .2
        self.teproc_.xns[20] = .01
        self.teproc_.xns[21] = .01
        self.teproc_.xns[22] = .25
        self.teproc_.xns[23] = .1
        self.teproc_.xns[24] = .25
        self.teproc_.xns[25] = .1
        self.teproc_.xns[26] = .25
        self.teproc_.xns[27] = .025
        self.teproc_.xns[28] = .25
        self.teproc_.xns[29] = .1
        self.teproc_.xns[30] = .25
        self.teproc_.xns[31] = .1
        self.teproc_.xns[32] = .25
        self.teproc_.xns[33] = .025
        self.teproc_.xns[34] = .05
        self.teproc_.xns[35] = .05
        self.teproc_.xns[36] = .01
        self.teproc_.xns[37] = .01
        self.teproc_.xns[38] = .01
        self.teproc_.xns[39] = .5
        self.teproc_.xns[40] = .5
        # for (i__ = 1 i__ <= 20 ++i__)
        for i__ in range (1, 21):
            self.dvec_.idv[i__ - 1] = 0
            # / * L500: * /

        self.wlk_.hspan[0] = .2
        self.wlk_.hzero[0] = .5
        self.wlk_.sspan[0] = .03
        self.wlk_.szero[0] = .485
        self.wlk_.spspan[0] = 0.
        self.wlk_.hspan[1] = .7
        self.wlk_.hzero[1] = 1.
        self.wlk_.sspan[1] = .003
        self.wlk_.szero[1] = .005
        self.wlk_.spspan[1] = 0.
        self.wlk_.hspan[2] = .25
        self.wlk_.hzero[2] = .5
        self.wlk_.sspan[2] = 10.
        self.wlk_.szero[2] = 45.
        self.wlk_.spspan[2] = 0.
        self.wlk_.hspan[3] = .7
        self.wlk_.hzero[3] = 1.
        self.wlk_.sspan[3] = 10.
        self.wlk_.szero[3] = 45.
        self.wlk_.spspan[3] = 0.
        self.wlk_.hspan[4] = .15
        self.wlk_.hzero[4] = .25
        self.wlk_.sspan[4] = 10.
        self.wlk_.szero[4] = 35.
        self.wlk_.spspan[4] = 0.
        self.wlk_.hspan[5] = .15
        self.wlk_.hzero[5] = .25
        self.wlk_.sspan[5] = 10.
        self.wlk_.szero[5] = 40.
        self.wlk_.spspan[5] = 0.
        self.wlk_.hspan[6] = 1.
        self.wlk_.hzero[6] = 2.
        self.wlk_.sspan[6] = .25
        self.wlk_.szero[6] = 1.
        self.wlk_.spspan[6] = 0.
        self.wlk_.hspan[7] = 1.
        self.wlk_.hzero[7] = 2.
        self.wlk_.sspan[7] = .25
        self.wlk_.szero[7] = 1.
        self.wlk_.spspan[7] = 0.
        self.wlk_.hspan[8] = .4
        self.wlk_.hzero[8] = .5
        self.wlk_.sspan[8] = .25
        self.wlk_.szero[8] = 0.
        self.wlk_.spspan[8] = 0.
        self.wlk_.hspan[9] = 1.5
        self.wlk_.hzero[9] = 2.
        self.wlk_.sspan[9] = 0.
        self.wlk_.szero[9] = 0.
        self.wlk_.spspan[9] = 0.
        self.wlk_.hspan[10] = 2.
        self.wlk_.hzero[10] = 3.
        self.wlk_.sspan[10] = 0.
        self.wlk_.szero[10] = 0.
        self.wlk_.spspan[10] = 0.
        self.wlk_.hspan[11] = 1.5
        self.wlk_.hzero[11] = 2.
        self.wlk_.sspan[11] = 0.
        self.wlk_.szero[11] = 0.
        self.wlk_.spspan[11] = 0.
        # for (i__ = 1 i__ <= 12 ++i__)
        for i__ in range (1, 13):
            self.wlk_.tlast[i__ - 1] = 0.
            self.wlk_.tnext[i__ - 1] = .1
            self.wlk_.adist[i__ - 1] = self.wlk_.szero[i__ - 1]
            self.wlk_.bdist[i__ - 1] = 0.
            self.wlk_.cdist[i__ - 1] = 0.
            self.wlk_.ddist[i__ - 1] = 0.
            # / * L550: * /

        time = 0.

        return self.tefunc(nn, time, yy, yp)
        # return yy

    def tefunc(self, nn, time, yy, yp):
        '''
        Main function.
        Variables descriptions put from teprob.f
        :param nn: Number of differential equations
        :param time: Current time(hrs)
        :param yy: Current state values
        :param yp: Output
        :return:
        '''
        #/* System generated locals */
        i__1 = 0
        d__1 = 0
        #/* Local variables */
        flms = 0
        xcmp = np.zeros(41)
        hwlk = 0
        vpos = np.zeros(12)
        xmns = 0
        swlk = 0
        i__ = 0
        spwlk = 0
        vovrl = 0
        rg = flcoef = pr = 0
        tmpfac = uarlev = r1f = r2f = uac = 0
        fin = np.zeros(8)

        dlp = vpr = uas = 0

        # / *Parameter  adjustments * /
        # --yp
        # --yy

        # function body
        # for (i__ = 1 i__ <= 20 ++i__)
        for i__ in range (1, 21):
            if self.dvec_.idv[i__ - 1] > 0:
                self.dvec_.idv[i__ - 1] = 1
            else:
                self.dvec_.idv[i__ - 1] = 0

            # / * L500: * /
        self.wlk_.idvwlk[0] = self.dvec_.idv[7]
        self.wlk_.idvwlk[1] = self.dvec_.idv[7]
        self.wlk_.idvwlk[2] = self.dvec_.idv[8]
        self.wlk_.idvwlk[3] = self.dvec_.idv[9]
        self.wlk_.idvwlk[4] = self.dvec_.idv[10]
        self.wlk_.idvwlk[5] = self.dvec_.idv[11]
        self.wlk_.idvwlk[6] = self.dvec_.idv[12]
        self.wlk_.idvwlk[7] = self.dvec_.idv[12]
        self.wlk_.idvwlk[8] = self.dvec_.idv[15]
        self.wlk_.idvwlk[9] = self.dvec_.idv[16]
        self.wlk_.idvwlk[10] = self.dvec_.idv[17]
        self.wlk_.idvwlk[11] = self.dvec_.idv[19]

        # for (i__ = 1 i__ <= 9 ++i__)
        for i__ in range(1, 10):
            if ( time >= self.wlk_.tnext[i__ - 1]):
                hwlk = self.wlk_.tnext[i__ - 1] - self.wlk_.tlast[i__ - 1]
                swlk = self.wlk_.adist[i__ - 1] + hwlk * (self.wlk_.bdist[i__ - 1] + hwlk * (self.wlk_.cdist[i__ - 1] + hwlk * self.wlk_.ddist[i__ - 1]))
                spwlk = self.wlk_.bdist[i__ - 1] + hwlk * (self.wlk_.cdist[i__ - 1] * 2.  + hwlk * 3. * self.wlk_.ddist[i__ - 1])
                self.wlk_.tlast[i__ - 1] = self.wlk_.tnext[i__ - 1]
                self.wlk_.adist[i__ - 1], self.wlk_.bdist[i__ - 1], self.wlk_.cdist[i__ - 1], self.wlk_.ddist[i__ - 1], self.wlk_.tnext[i__ - 1] = self.tesub5_( swlk, spwlk, self.wlk_.adist[i__ - 1], self.wlk_.bdist[i__ -
                1], self.wlk_.cdist[i__ - 1], self.wlk_.ddist[i__ - 1], self.wlk_.tlast[i__ - 1], self.wlk_.tnext[i__ - 1], self.wlk_.hspan[
                i__ - 1], self.wlk_.hzero[i__ - 1], self.wlk_.sspan[i__ - 1],
                self.wlk_.szero[i__ - 1], self.wlk_.spspan[i__ - 1],
                self.wlk_.idvwlk[i__ - 1])
            # / * L900: * /
        # for (i__ = 10 i__ <= 12 ++i__)
        for i__ in range (10, 13):
            if ( time >= self.wlk_.tnext[i__ - 1]):
                hwlk = self.wlk_.tnext[i__ - 1] - self.wlk_.tlast[i__ - 1]
                swlk = self.wlk_.adist[i__ - 1] + hwlk * (self.wlk_.bdist[i__ - 1] + hwlk
                * (self.wlk_.cdist[i__ - 1] + hwlk * self.wlk_.ddist[i__ - 1]))
                spwlk = self.wlk_.bdist[i__ - 1] + hwlk * (self.wlk_.cdist[i__ - 1] * 2.
                + hwlk * 3. * self.wlk_.ddist[i__ - 1])
                self.wlk_.tlast[i__ - 1] = self.wlk_.tnext[i__ - 1]
                if (swlk > .1):
                    self.wlk_.adist[i__ - 1] = swlk
                    self.wlk_.bdist[i__ - 1] = spwlk
                    self.wlk_.cdist[i__ - 1] = -(swlk * 3. + spwlk * .2) / .01
                    self.wlk_.ddist[i__ - 1] = (swlk * 2. + spwlk * .1) / .001
                    self.wlk_.tnext[i__ - 1] = self.wlk_.tlast[i__ - 1] + .1
                else:
                    isd = -1
                    hwlk = self.wlk_.hspan[i__ - 1] * self.tesub7_(isd) + self.wlk_.hzero[i__ - 1]
                    self.wlk_.adist[i__ - 1] = 0.
                    self.wlk_.bdist[i__ - 1] = 0.
                    ### / * Computing 2nd power * /
                    d__1 = hwlk
                    self.wlk_.cdist[i__ - 1] = float(self.wlk_.idvwlk[i__ - 1]) / (d__1 * d__1)
                    self.wlk_.ddist[i__ - 1] = 0.
                    self.wlk_.tnext[i__ - 1] = self.wlk_.tlast[i__ - 1] + hwlk
            # / * L910: * /
        if (time == 0.):
            # for (i__ = 1 i__ <= 12 ++i__)
            for i__ in range (1, 13):
                self.wlk_.adist[i__ - 1] = self.wlk_.szero[i__ - 1]
                self.wlk_.bdist[i__ - 1] = 0.
                self.wlk_.cdist[i__ - 1] = 0.
                self.wlk_.ddist[i__ - 1] = 0.
                self.wlk_.tlast[i__ - 1] = 0.
                self.wlk_.tnext[i__ - 1] = .1
                # / * L950: * /
        # self.teproc_.esr = self.teproc_.etr / self.teproc_.utlr
        self.teproc_.xst[24] = self.tesub8_(self.c__1, time) - self.dvec_.idv[0] * .03 - self.dvec_.idv[1] * .00243719

        self.teproc_.xst[25] = self.tesub8_(self.c__2, time) + self.dvec_.idv[1] * .005
        self.teproc_.xst[26] = 1. - self.teproc_.xst[24] - self.teproc_.xst[25]
        self.teproc_.tst[0] = self.tesub8_(self.c__3, time) + self.dvec_.idv[2] * 5.
        self.teproc_.tst[3] = self.tesub8_(self.c__4, time)
        self.teproc_.tcwr = self.tesub8_(self.c__5, time) + self.dvec_.idv[3] * 5.
        self.teproc_.tcws = self.tesub8_(self.c__6, time) + self.dvec_.idv[4] * 5.
        r1f = self.tesub8_(self.c__7, time)
        r2f = self.tesub8_(self.c__8, time)
        # for (i__ = 1 i__ <= 3 ++i__)
        for i__ in range(1, 4):
            self.teproc_.ucvr[i__ - 1] = yy[i__-1]
            self.teproc_.ucvs[i__ - 1] = yy[i__ + 9-1]
            self.teproc_.uclr[i__ - 1] = 0.
            self.teproc_.ucls[i__ - 1] = 0.
            # / * L1010: * /
        # for (i__ = 4 i__ <= 8 ++i__)
        for i__ in range (4, 9):
            self.teproc_.uclr[i__ - 1] = yy[i__-1]
            self.teproc_.ucls[i__ - 1] = yy[i__ + 9-1]
            # / * L1020: * /

        # for (i__ = 1 i__ <= 8 ++i__)
        for i__ in range(1, 9):
            self.teproc_.uclc[i__ - 1] = yy[i__ + 18-1]
            self.teproc_.ucvv[i__ - 1] = yy[i__ + 27-1]
            # / * L1030: * /

        self.teproc_.etr = yy[9-1]
        self.teproc_.ets = yy[18-1]
        self.teproc_.etc = yy[27-1]
        self.teproc_.etv = yy[36-1]
        self.teproc_.twr = yy[37-1]
        self.teproc_.tws = yy[38-1]
        # for (i__ = 1 i__ <= 12 ++i__)
        for i__ in range (1, 13):
            vpos[i__ - 1] = yy[i__ + 38-1]
            # / * L1035: * /
        self.teproc_.utlr = 0.
        self.teproc_.utls = 0.
        self.teproc_.utlc = 0.
        self.teproc_.utvv = 0.
        # for (i__ = 1 i__ <= 8 ++i__)
        for i__ in range (1, 9):
            self.teproc_.utlr += self.teproc_.uclr[i__ - 1]
            self.teproc_.utls += self.teproc_.ucls[i__ - 1]
            self.teproc_.utlc += self.teproc_.uclc[i__ - 1]
            self.teproc_.utvv += self.teproc_.ucvv[i__ - 1]
            # / * L1040: * /

        # for (i__ = 1 i__ <= 8 ++i__)
        for i__ in range (1, 9):
            self.teproc_.xlr[i__ - 1] = self.teproc_.uclr[i__ - 1] / self.teproc_.utlr
            self.teproc_.xls[i__ - 1] = self.teproc_.ucls[i__ - 1] / self.teproc_.utls
            self.teproc_.xlc[i__ - 1] = self.teproc_.uclc[i__ - 1] / self.teproc_.utlc
            self.teproc_.xvv[i__ - 1] = self.teproc_.ucvv[i__ - 1] / self.teproc_.utvv
            # / * L1050: * /
        self.teproc_.esr = self.teproc_.etr / self.teproc_.utlr
        self.teproc_.ess = self.teproc_.ets / self.teproc_.utls
        self.teproc_.esc = self.teproc_.etc / self.teproc_.utlc
        self.teproc_.esv = self.teproc_.etv / self.teproc_.utvv
        self.teproc_.tcr = self.tesub2_(self.teproc_.xlr, self.teproc_.tcr, self.teproc_.esr, self.c__0)
        self.teproc_.tkr = self.teproc_.tcr + 273.15
        self.teproc_.tcs = self.tesub2_(self.teproc_.xls, self.teproc_.tcs, self.teproc_.ess, self.c__0)
        self.teproc_.tks = self.teproc_.tcs + 273.15
        self.teproc_.tcc = self.tesub2_(self.teproc_.xlc, self.teproc_.tcc, self.teproc_.esc, self.c__0)
        self.teproc_.tcv = self.tesub2_(self.teproc_.xvv, self.teproc_.tcv, self.teproc_.esv, self.c__2)
        self.teproc_.tkv = self.teproc_.tcv + 273.15
        self.teproc_.dlr = self.tesub4_(self.teproc_.xlr, self.teproc_.tcr, self.teproc_.dlr)
        self.teproc_.dls = self.tesub4_(self.teproc_.xls, self.teproc_.tcs, self.teproc_.dls)
        self.teproc_.dlc = self.tesub4_(self.teproc_.xlc, self.teproc_.tcc, self.teproc_.dlc)
        self.teproc_.vlr = self.teproc_.utlr / self.teproc_.dlr
        self.teproc_.vls = self.teproc_.utls / self.teproc_.dls
        self.teproc_.vlc = self.teproc_.utlc / self.teproc_.dlc
        self.teproc_.vvr = self.teproc_.vtr - self.teproc_.vlr
        self.teproc_.vvs = self.teproc_.vts - self.teproc_.vls
        rg = 998.9
        self.teproc_.ptr = 0.
        self.teproc_.pts = 0.
        # for (i__ = 1 i__ <= 3 ++i__)
        for i__ in range (1, 4):
            self.teproc_.ppr[i__ - 1] = self.teproc_.ucvr[i__ - 1] * rg * self.teproc_.tkr / self.teproc_.vvr
            self.teproc_.ptr += self.teproc_.ppr[i__ - 1]
            self.teproc_.pps[i__ - 1] = self.teproc_.ucvs[i__ - 1] * rg * self.teproc_.tks / self.teproc_.vvs
            self.teproc_.pts += self.teproc_.pps[i__ - 1]
            # / * L1110: * /

        # for (i__ = 4 i__ <= 8 ++i__)
        for i__ in range (4, 9):
            vpr = np.exp(self.const_.avp[i__ - 1] + self.const_.bvp[i__ - 1] / (self.teproc_.tcr + self.const_.cvp[i__ - 1]))
            self.teproc_.ppr[i__ - 1] = vpr * self.teproc_.xlr[i__ - 1]
            self.teproc_.ptr += self.teproc_.ppr[i__ - 1]
            vpr = np.exp(self.const_.avp[i__ - 1] + self.const_.bvp[i__ - 1] / (self.teproc_.tcs + self.const_.cvp[i__ - 1]))
            self.teproc_.pps[i__ - 1] = vpr * self.teproc_.xls[i__ - 1]
            self.teproc_.pts += self.teproc_.pps[i__ - 1]
            # / * L1120: * /

        self.teproc_.ptv = self.teproc_.utvv * rg * self.teproc_.tkv / self.teproc_.vtv
        # for (i__ = 1 i__ <= 8 ++i__)
        for i__ in range (1, 9):
            self.teproc_.xvr[i__ - 1] = self.teproc_.ppr[i__ - 1] / self.teproc_.ptr
            self.teproc_.xvs[i__ - 1] = self.teproc_.pps[i__ - 1] / self.teproc_.pts
            # / * L1130: * /
        self.teproc_.utvr = self.teproc_.ptr * self.teproc_.vvr / rg / self.teproc_.tkr
        self.teproc_.utvs = self.teproc_.pts * self.teproc_.vvs / rg / self.teproc_.tks
        # for (i__ = 4 i__ <= 8 ++i__)
        for i__ in range (4, 9):
            self.teproc_.ucvr[i__ - 1] = self.teproc_.utvr * self.teproc_.xvr[i__ - 1]
            self.teproc_.ucvs[i__ - 1] = self.teproc_.utvs * self.teproc_.xvs[i__ - 1]
            # / * L1140: * /

        self.teproc_.rr[0] = np.exp(31.5859536 - 20130.85052843482 / self.teproc_.tkr) *r1f
        self.teproc_.rr[1] = np.exp(3.00094014 - 10065.42526421741 / self.teproc_.tkr) *r2f
        self.teproc_.rr[2] = np.exp(53.4060443 - 30196.27579265224 / self.teproc_.tkr)
        self.teproc_.rr[3] = self.teproc_.rr[2] * .767488334
        if (self.teproc_.ppr[0] > 0. and self.teproc_.ppr[2] > 0.):
            r1f = np.power(self.teproc_.ppr[0], self.c_b73)
            r2f = np.power(self.teproc_.ppr[2], self.c_b74)
            self.teproc_.rr[0] = self.teproc_.rr[0] * r1f * r2f * self.teproc_.ppr[3]
            self.teproc_.rr[1] = self.teproc_.rr[1] * r1f * r2f * self.teproc_.ppr[4]
        else:
            self.teproc_.rr[0] = 0.
            self.teproc_.rr[1] = 0.

        self.teproc_.rr[2] = self.teproc_.rr[2] * self.teproc_.ppr[0] * self.teproc_.ppr[4]
        self.teproc_.rr[3] = self.teproc_.rr[3] * self.teproc_.ppr[0] * self.teproc_.ppr[3]
        # for (i__ = 1 i__ <= 4 ++i__)
        for i__ in range (1, 5):
            self.teproc_.rr[i__ - 1] *= self.teproc_.vvr
            # / * L1200: * /

        self.teproc_.crxr[0] = -self.teproc_.rr[0] - self.teproc_.rr[1] - self.teproc_.rr[2]
        self.teproc_.crxr[2] = -self.teproc_.rr[0] - self.teproc_.rr[1]
        self.teproc_.crxr[3] = -self.teproc_.rr[0] - self.teproc_.rr[3] * 1.5
        self.teproc_.crxr[4] = -self.teproc_.rr[1] - self.teproc_.rr[2]
        self.teproc_.crxr[5] = self.teproc_.rr[2] + self.teproc_.rr[3]
        self.teproc_.crxr[6] = self.teproc_.rr[0]
        self.teproc_.crxr[7] = self.teproc_.rr[1]
        self.teproc_.rh = self.teproc_.rr[0] * self.teproc_.htr[0] + self.teproc_.rr[1] * self.teproc_.htr[1]
        self.teproc_.xmws[0] = 0
        self.teproc_.xmws[1] = 0
        self.teproc_.xmws[5] = 0
        self.teproc_.xmws[7] = 0
        self.teproc_.xmws[8] = 0
        self.teproc_.xmws[9] = 0
        # for (i__ = 1 i__ <= 8 ++i__)
        for i__ in range(1, 9):
            self.teproc_.xst[i__ + 39] = self.teproc_.xvv[i__ - 1]
            self.teproc_.xst[i__ + 55] = self.teproc_.xvr[i__ - 1]
            self.teproc_.xst[i__ + 63] = self.teproc_.xvs[i__ - 1]
            self.teproc_.xst[i__ + 71] = self.teproc_.xvs[i__ - 1]
            self.teproc_.xst[i__ + 79] = self.teproc_.xls[i__ - 1]
            self.teproc_.xst[i__ + 95] = self.teproc_.xlc[i__ - 1]
            self.teproc_.xmws[0] += self.teproc_.xst[i__ - 1] * self.const_.xmw[i__ - 1]
            self.teproc_.xmws[1] += self.teproc_.xst[i__ + 7] * self.const_.xmw[i__ - 1]
            self.teproc_.xmws[5] += self.teproc_.xst[i__ + 39] * self.const_.xmw[i__ - 1]
            self.teproc_.xmws[7] += self.teproc_.xst[i__ + 55] * self.const_.xmw[i__ - 1]
            self.teproc_.xmws[8] += self.teproc_.xst[i__ + 63] * self.const_.xmw[i__ - 1]
            self.teproc_.xmws[9] += self.teproc_.xst[i__ + 71] * self.const_.xmw[i__ - 1]
            # / * L2010: * /

        self.teproc_.tst[5] = self.teproc_.tcv
        self.teproc_.tst[7] = self.teproc_.tcr
        self.teproc_.tst[8] = self.teproc_.tcs
        self.teproc_.tst[9] = self.teproc_.tcs
        self.teproc_.tst[10] = self.teproc_.tcs
        self.teproc_.tst[12] = self.teproc_.tcc
        self.teproc_.hst = self.tesub1_(self.teproc_.xst, self.teproc_.tst, self.teproc_.hst, self.c__1)
        # self.teproc_.hst[1] = self.tesub1_(self.teproc_.xst[8], self.teproc_.tst[1], self.teproc_.hst[1], self.c__1)
        # self.teproc_.hst[2] = self.tesub1_(self.teproc_.xst[16], self.teproc_.tst[2], self.teproc_.hst[2], self.c__1)
        # self.teproc_.hst[3] = self.tesub1_(self.teproc_.xst[24], self.teproc_.tst[3], self.teproc_.hst[3], self.c__1)
        # self.teproc_.hst[5] = self.tesub1_(self.teproc_.xst[40], self.teproc_.tst[5], self.teproc_.hst[5], self.c__1)
        # self.teproc_.hst[7] = self.tesub1_(self.teproc_.xst[56], self.teproc_.tst[7], self.teproc_.hst[7], self.c__1)
        # self.teproc_.hst[8] = self.tesub1_(self.teproc_.xst[64], self.teproc_.tst[8], self.teproc_.hst[8], self.c__1)
        self.teproc_.hst[1] = self.tesub1_(self.teproc_.xst, self.teproc_.tst[1], self.teproc_.hst[1], self.c__1, 8)
        self.teproc_.hst[2] = self.tesub1_(self.teproc_.xst, self.teproc_.tst[2], self.teproc_.hst[2], self.c__1, 16)
        self.teproc_.hst[3] = self.tesub1_(self.teproc_.xst, self.teproc_.tst[3], self.teproc_.hst[3], self.c__1, 24)
        self.teproc_.hst[5] = self.tesub1_(self.teproc_.xst, self.teproc_.tst[5], self.teproc_.hst[5], self.c__1, 40)
        self.teproc_.hst[7] = self.tesub1_(self.teproc_.xst, self.teproc_.tst[7], self.teproc_.hst[7], self.c__1, 56)
        self.teproc_.hst[8] = self.tesub1_(self.teproc_.xst, self.teproc_.tst[8], self.teproc_.hst[8], self.c__1, 64)
        self.teproc_.hst[9] = self.teproc_.hst[8]
        self.teproc_.hst[10] = self.tesub1_(self.teproc_.xst, self.teproc_.tst[10], self.teproc_.hst[10], self.c__0, 80)
        self.teproc_.hst[12] = self.tesub1_(self.teproc_.xst, self.teproc_.tst[12], self.teproc_.hst[12], self.c__0, 96)
        self.teproc_.ftm[0] = vpos[0] * self.teproc_.vrng[0] / 100.
        self.teproc_.ftm[1] = vpos[1] * self.teproc_.vrng[1] / 100.
        self.teproc_.ftm[2] = vpos[2] * (1. - self.dvec_.idv[5]) * self.teproc_.vrng[2] / 100.
        self.teproc_.ftm[3] = vpos[3] * (1. - self.dvec_.idv[6] * .2) * self.teproc_.vrng[3] / 100. + 1e-10
        self.teproc_.ftm[10] = vpos[6] * self.teproc_.vrng[6] / 100.
        self.teproc_.ftm[12] = vpos[7] * self.teproc_.vrng[7] / 100.
        uac = vpos[8] * self.teproc_.vrng[8] * (self.tesub8_(self.c__9, time) + 1.) / 100.
        self.teproc_.fwr = vpos[9] * self.teproc_.vrng[9] / 100.
        self.teproc_.fws = vpos[10] * self.teproc_.vrng[10] / 100.
        self.teproc_.agsp = (vpos[11] + 150.) / 100.
        dlp = self.teproc_.ptv - self.teproc_.ptr
        if (dlp < 0.):
            dlp = 0.

        flms = np.sqrt(dlp) * 1937.6
        self.teproc_.ftm[5] = flms / self.teproc_.xmws[5]
        dlp = self.teproc_.ptr - self.teproc_.pts
        if (dlp < 0.):
            dlp = 0.

        flms = np.sqrt(dlp) * 4574.21 * (1. - self.tesub8_(self.c__12, time) * .25)
        self.teproc_.ftm[7] = flms / self.teproc_.xmws[7]
        dlp = self.teproc_.pts - 760.
        if (dlp < 0.):
            dlp = 0.

        flms = vpos[5] * .151169 * np.sqrt(dlp)
        self.teproc_.ftm[9] = flms / self.teproc_.xmws[9]
        pr = self.teproc_.ptv / self.teproc_.pts
        if (pr < 1.):
            pr = 1.
        if (pr > self.teproc_.cpprmx):
            pr = self.teproc_.cpprmx

        flcoef = self.teproc_.cpflmx / 1.197
        # / *Computing 3rd power * /
        d__1 = pr
        flms = self.teproc_.cpflmx + flcoef * (1. - d__1 * (d__1 * d__1))
        self.teproc_.cpdh = flms * (self.teproc_.tcs + 273.15) * 1.8e-6 * 1.9872 * (self.teproc_.ptv - self.teproc_.pts) / (self.teproc_.xmws[8] * self.teproc_.pts)
        dlp = self.teproc_.ptv - self.teproc_.pts
        if (dlp < 0.):
            dlp = 0.

        flms -= vpos[4] * 53.349 * np.sqrt(dlp)
        if (flms < .001):
            flms = .001

        self.teproc_.ftm[8] = flms / self.teproc_.xmws[8]
        self.teproc_.hst[8] += self.teproc_.cpdh / self.teproc_.ftm[8]
        # for (i__ = 1 i__ <= 8 ++i__)
        for i__ in range (1, 9):
            self.teproc_.fcm[i__ - 1] = self.teproc_.xst[i__ - 1] * self.teproc_.ftm[0]
            self.teproc_.fcm[i__ + 7] = self.teproc_.xst[i__ + 7] * self.teproc_.ftm[1]
            self.teproc_.fcm[i__ + 15] = self.teproc_.xst[i__ + 15] * self.teproc_.ftm[2]
            self.teproc_.fcm[i__ + 23] = self.teproc_.xst[i__ + 23] * self.teproc_.ftm[3]
            self.teproc_.fcm[i__ + 39] = self.teproc_.xst[i__ + 39] * self.teproc_.ftm[5]
            self.teproc_.fcm[i__ + 55] = self.teproc_.xst[i__ + 55] * self.teproc_.ftm[7]
            self.teproc_.fcm[i__ + 63] = self.teproc_.xst[i__ + 63] * self.teproc_.ftm[8]
            self.teproc_.fcm[i__ + 71] = self.teproc_.xst[i__ + 71] * self.teproc_.ftm[9]
            self.teproc_.fcm[i__ + 79] = self.teproc_.xst[i__ + 79] * self.teproc_.ftm[10]
            self.teproc_.fcm[i__ + 95] = self.teproc_.xst[i__ + 95] * self.teproc_.ftm[12]
            # / * L5020: * /

        if (self.teproc_.ftm[10] > .1):
            if (self.teproc_.tcc > 170.):
                tmpfac = self.teproc_.tcc - 120.262
            elif (self.teproc_.tcc < 5.292):
                tmpfac = .1
            else:
                tmpfac = 363.744 / (177. - self.teproc_.tcc) - 2.22579488
            vovrl = self.teproc_.ftm[3] / self.teproc_.ftm[10] * tmpfac
            self.teproc_.sfr[3] = vovrl * 8.501 / (vovrl * 8.501 + 1.)
            self.teproc_.sfr[4] = vovrl * 11.402 / (vovrl * 11.402 + 1.)
            self.teproc_.sfr[5] = vovrl * 11.795 / (vovrl * 11.795 + 1.)
            self.teproc_.sfr[6] = vovrl * .048 / (vovrl * .048 + 1.)
            self.teproc_.sfr[7] = vovrl * .0242 / (vovrl * .0242 + 1.)
        else:
            self.teproc_.sfr[3] = .9999
            self.teproc_.sfr[4] = .999
            self.teproc_.sfr[5] = .999
            self.teproc_.sfr[6] = .99
            self.teproc_.sfr[7] = .98

        # for (i__ = 1 i__ <= 8 ++i__)
        for i__ in range (1, 9):
            fin[i__ - 1] = 0.
            fin[i__ - 1] += self.teproc_.fcm[i__ + 23]
            fin[i__ - 1] += self.teproc_.fcm[i__ + 79]
            # / * L6010: * /

        self.teproc_.ftm[4] = 0.
        self.teproc_.ftm[11] = 0.
        # for (i__ = 1 i__ <= 8 ++i__)
        for i__ in range (1, 9):
            self.teproc_.fcm[i__ + 31] = self.teproc_.sfr[i__ - 1] * fin[i__ - 1]
            self.teproc_.fcm[i__ + 87] = fin[i__ - 1] - self.teproc_.fcm[i__ + 31]
            self.teproc_.ftm[4] += self.teproc_.fcm[i__ + 31]
            self.teproc_.ftm[11] += self.teproc_.fcm[i__ + 87]
            # / * L6020: * /

        # for (i__ = 1 i__ <= 8 ++i__)
        for i__ in range (1, 9):
            self.teproc_.xst[i__ + 31] = self.teproc_.fcm[i__ + 31] / self.teproc_.ftm[4]
            self.teproc_.xst[i__ + 87] = self.teproc_.fcm[i__ + 87] / self.teproc_.ftm[11]
            # / * L6030: * /

        self.teproc_.tst[4] = self.teproc_.tcc
        self.teproc_.tst[11] = self.teproc_.tcc
        self.teproc_.hst[4] = self.tesub1_(self.teproc_.xst, self.teproc_.tst[4], self.teproc_.hst[4], self.c__1, 32)
        self.teproc_.hst[11] = self.tesub1_(self.teproc_.xst, self.teproc_.tst[11], self.teproc_.hst[11], self.c__0, 88)
        self.teproc_.ftm[6] = self.teproc_.ftm[5]
        self.teproc_.hst[6] = self.teproc_.hst[5]
        self.teproc_.tst[6] = self.teproc_.tst[5]
        # for (i__ = 1 i__ <= 8 ++i__)
        for i__ in range (1, 9):
            self.teproc_.xst[i__ + 47] = self.teproc_.xst[i__ + 39]
            self.teproc_.fcm[i__ + 47] = self.teproc_.fcm[i__ + 39]
            # / * L6130: * /

        if (self.teproc_.vlr / 7.8 > 50.):
            uarlev = 1.
        elif (self.teproc_.vlr / 7.8 < 10.):
            uarlev = 0.
        else:
            uarlev = self.teproc_.vlr * .025 / 7.8 - .25

        # / *Computing 2nd power * /
        d__1 = self.teproc_.agsp
        self.teproc_.uar = uarlev * (d__1 * d__1 * (-0.5) + self.teproc_.agsp * 2.75 - 2.5) *.85549
        self.teproc_.qur = self.teproc_.uar * (self.teproc_.twr - self.teproc_.tcr) * (1. - self.tesub8_(self.c__10, time) * .35)
        # / *Computing 4th power * /
        d__1 = self.teproc_.ftm[7] / 3528.73
        d__1 *= d__1
        uas = (1. - 1. / (d__1 * d__1 + 1.)) *.404655
        self.teproc_.qus = uas * (self.teproc_.tws - self.teproc_.tst[7]) * (1. - self.tesub8_(self.c__11, time) * .25)
        self.teproc_.quc = 0.
        if (self.teproc_.tcc < 100.):
            self.teproc_.quc = uac * (100. - self.teproc_.tcc)

        self.pv_.xmeas[0] = self.teproc_.ftm[2] * .359 / 35.3145
        self.pv_.xmeas[1] = self.teproc_.ftm[0] * self.teproc_.xmws[0] * .454
        self.pv_.xmeas[2] = self.teproc_.ftm[1] * self.teproc_.xmws[1] * .454
        self.pv_.xmeas[3] = self.teproc_.ftm[3] * .359 / 35.3145
        self.pv_.xmeas[4] = self.teproc_.ftm[8] * .359 / 35.3145
        self.pv_.xmeas[5] = self.teproc_.ftm[5] * .359 / 35.3145
        self.pv_.xmeas[6] = (self.teproc_.ptr - 760.) / 760. * 101.325
        self.pv_.xmeas[7] = (self.teproc_.vlr - 84.6) / 666.7 * 100.
        self.pv_.xmeas[8] = self.teproc_.tcr
        self.pv_.xmeas[9] = self.teproc_.ftm[9] * .359 / 35.3145
        self.pv_.xmeas[10] = self.teproc_.tcs
        self.pv_.xmeas[11] = (self.teproc_.vls - 27.5) / 290. * 100.
        self.pv_.xmeas[12] = (self.teproc_.pts - 760.) / 760. * 101.325
        self.pv_.xmeas[13] = self.teproc_.ftm[10] / self.teproc_.dls / 35.3145
        self.pv_.xmeas[14] = (self.teproc_.vlc - 78.25) / self.teproc_.vtc * 100.
        self.pv_.xmeas[15] = (self.teproc_.ptv - 760.) / 760. * 101.325
        self.pv_.xmeas[16] = self.teproc_.ftm[12] / self.teproc_.dlc / 35.3145
        self.pv_.xmeas[17] = self.teproc_.tcc
        self.pv_.xmeas[18] = self.teproc_.quc * 1040. * .454

        self.pv_.xmeas[19] = self.teproc_.cpdh * 392.7
        self.pv_.xmeas[19] = self.teproc_.cpdh * 293.07
        self.pv_.xmeas[20] = self.teproc_.twr
        self.pv_.xmeas[21] = self.teproc_.tws
        ts.global_vars.isd = 0
        if (self.pv_.xmeas[6] > 3e3):
            ts.global_vars.isd = 1
            print("High Reactor Pressure!!  Shutting down.")

        if (self.teproc_.vlr / 35.3145 > 24.):
            ts.global_vars.isd = 2
            print("High Reactor Liquid Level!!  Shutting down.")

        if (self.teproc_.vlr / 35.3145 < 2.):
            ts.global_vars.isd = 3
            print("Low Reactor Liquid Level!!  Shutting down.")

        if (self.pv_.xmeas[8] > 175.):
            print("High Reactor Temperature!!  Shutting down.")
            ts.global_vars.isd = 4

        if (self.teproc_.vls / 35.3145 > 12.):
            ts.global_vars.isd = 5
            print("High Separator Liquid Level!!  Shutting down.")

        if (self.teproc_.vls / 35.3145 < 1.):
            ts.global_vars.isd = 6
            print("Low Separator Liquid Level!!  Shutting down.")

        if (self.teproc_.vlc / 35.3145 > 8.):
            print("High Stripper Liquid Level!!  Shutting down.")
            ts.global_vars.isd = 7

        if (self.teproc_.vlc / 35.3145 < 1.):
            ts.global_vars.isd = 8
            print("Low Stripper Liquid Level!!  Shutting down.")
        if (time > 0. and  ts.global_vars.isd == 0):
            # for (i__ = 1 i__ <= 22 ++i__)
            for i__ in range (1, 23):
                xmns = self.tesub6_(self.teproc_.xns[i__ - 1], xmns)
                self.pv_.xmeas[i__ - 1] += xmns
                # / * L6500: * /


        xcmp[22] = self.teproc_.xst[48] * 100.
        xcmp[23] = self.teproc_.xst[49] * 100.
        xcmp[24] = self.teproc_.xst[50] * 100.
        xcmp[25] = self.teproc_.xst[51] * 100.
        xcmp[26] = self.teproc_.xst[52] * 100.
        xcmp[27] = self.teproc_.xst[53] * 100.
        xcmp[28] = self.teproc_.xst[72] * 100.
        xcmp[29] = self.teproc_.xst[73] * 100.
        xcmp[30] = self.teproc_.xst[74] * 100.
        xcmp[31] = self.teproc_.xst[75] * 100.
        xcmp[32] = self.teproc_.xst[76] * 100.
        xcmp[33] = self.teproc_.xst[77] * 100.
        xcmp[34] = self.teproc_.xst[78] * 100.
        xcmp[35] = self.teproc_.xst[79] * 100.
        xcmp[36] = self.teproc_.xst[99] * 100.
        xcmp[37] = self.teproc_.xst[100] * 100.
        xcmp[38] = self.teproc_.xst[101] * 100.
        xcmp[39] = self.teproc_.xst[102] * 100.
        xcmp[40] = self.teproc_.xst[103] * 100.
        if (time == 0.):
            # for (i__ = 23 i__ <= 41 ++i__)
            for i__ in range (23, 42):
                self.teproc_.xdel[i__ - 1] = xcmp[i__ - 1]
                self.pv_.xmeas[i__ - 1] = xcmp[i__ - 1]
                # / * L7010: * /

            self.teproc_.tgas = .1
            self.teproc_.tprod = .25

        if (time >= self.teproc_.tgas):
            # for (i__ = 23 i__ <= 36 ++i__)
            for i__ in range (23, 37):
                self.pv_.xmeas[i__ - 1] = self.teproc_.xdel[i__ - 1]
                xmns =  self.tesub6_(self.teproc_.xns[i__ - 1], xmns)
                self.pv_.xmeas[i__ - 1] += xmns
                self.teproc_.xdel[i__ - 1] = xcmp[i__ - 1]
                # / * L7020: * /
            self.teproc_.tgas += .1

        if (time >= self.teproc_.tprod):
            # for (i__ = 37 i__ <= 41 ++i__)
            for i__ in range (37, 42):
                self.pv_.xmeas[i__ - 1] = self.teproc_.xdel[i__ - 1]
                xmns = self.tesub6_(self.teproc_.xns[i__ - 1], xmns)
                self.pv_.xmeas[i__ - 1] += xmns
                self.teproc_.xdel[i__ - 1] = xcmp[i__ - 1]
                # / * L7030: * /
            self.teproc_.tprod += .25
        # for (i__ = 1 i__ <= 8 ++i__)
        for i__ in range (1, 9):
            yp[i__-1] = self.teproc_.fcm[i__ + 47] - self.teproc_.fcm[i__ + 55] + self.teproc_.crxr[i__ - 1]
            yp[i__ + 9 -1] = self.teproc_.fcm[i__ + 55] - self.teproc_.fcm[i__ + 63] -  self.teproc_.fcm[i__ + 71] - self.teproc_.fcm[i__ + 79]
            yp[i__ + 18 -1] = self.teproc_.fcm[i__ + 87] - self.teproc_.fcm[i__ + 95]
            yp[i__ + 27 -1] = self.teproc_.fcm[i__ - 1] + self.teproc_.fcm[i__ + 7] +  self.teproc_.fcm[i__ + 15] + self.teproc_.fcm[i__ + 31] + self.teproc_.fcm[i__ + 63] - self.teproc_.fcm[i__ + 39]
            # / * L9010: * /

        yp[9 -1] = self.teproc_.hst[6] * self.teproc_.ftm[6] - self.teproc_.hst[7] * self.teproc_.ftm[7] + self.teproc_.rh + self.teproc_.qur
        # / * 	Here is the "correct" version of the separator  energy balance: */
        # /* 	YP( 18 )=HST( 8 )*FTM( 8)- */
        # /*    .(HST( 9 )*FTM( 9 )- cpdh)- */
        # /*    .HST( 10 )*FTM( 10)- */
        # /*    .HST( 11 )*FTM( 11)+ */
        # /*    .QUS */
        # /* 		Here is the original version */
        yp[18 -1] = self.teproc_.hst[7] * self.teproc_.ftm[7] - self.teproc_.hst[8] * \
        self.teproc_.ftm[8] - self.teproc_.hst[9] * self.teproc_.ftm[9] - \
        self.teproc_.hst[10] * self.teproc_.ftm[10] + self.teproc_.qus
        yp[27 -1] = self.teproc_.hst[3] * self.teproc_.ftm[3] + self.teproc_.hst[10] * \
        self.teproc_.ftm[10] - self.teproc_.hst[4] * self.teproc_.ftm[4] - \
        self.teproc_.hst[12] * self.teproc_.ftm[12] + self.teproc_.quc
        yp[36 -1] = self.teproc_.hst[0] * self.teproc_.ftm[0] + self.teproc_.hst[1] * \
        self.teproc_.ftm[1] + self.teproc_.hst[2] * self.teproc_.ftm[2] + \
        self.teproc_.hst[4] * self.teproc_.ftm[4] + self.teproc_.hst[8] * \
        self.teproc_.ftm[8] - self.teproc_.hst[5] * self.teproc_.ftm[5]
        yp[37 -1] = (self.teproc_.fwr * 500.53 * (self.teproc_.tcwr - self.teproc_.twr) -
                  self.teproc_.qur * 1e6 / 1.8) / self.teproc_.hwr
        yp[38 -1] = (self.teproc_.fws * 500.53 * (self.teproc_.tcws - self.teproc_.tws) -
                                       self.teproc_.qus * 1e6 / 1.8) / self.teproc_.hws
        self.teproc_.ivst[9] = self.dvec_.idv[13]
        self.teproc_.ivst[10] = self.dvec_.idv[14]
        self.teproc_.ivst[4] = self.dvec_.idv[18]
        self.teproc_.ivst[6] = self.dvec_.idv[18]
        self.teproc_.ivst[7] = self.dvec_.idv[18]
        self.teproc_.ivst[8] = self.dvec_.idv[18]
        # for (i__ = 1 i__ <= 12 ++i__)
        for i__ in range (1, 13):
            if (time == 0. or (abs(self.teproc_.vcv[i__ - 1] - self.pv_.xmv[i__ - 1]) > (self.teproc_.vst[i__ - 1] * self.teproc_.ivst[i__ - 1]))):
                self.teproc_.vcv[i__ - 1] = self.pv_.xmv[i__ - 1]

            if (self.teproc_.vcv[i__ - 1] < 0.):
                self.teproc_.vcv[i__ - 1] = 0.

            if (self.teproc_.vcv[i__ - 1] > 100.):
                self.teproc_.vcv[i__ - 1] = 100.

            yp[i__ + 38 -1] = (self.teproc_.vcv[i__ - 1] - vpos[i__ - 1]) / self.teproc_.vtau[i__ - 1]
            # / * L9020: * /

        if (time > 0. and ts.global_vars.isd != 0):
            i__1 = nn
            # for (i__ = 1 i__ <= i__1 ++i__)
            for i__ in range(1, i__1 + 1):
                yp[i__ -1] = 0.
                # / * L9030: * /


        return yp


    # Tesubroutines
    def tesub1_(self, z__, t, h__, ity, start_index = 0):
        '''

        :param z__: float array
        :param t: const float variable
        :param h__: float variable
        :param ity: const integer variable
        :return: z__ and h__ - variables, that changes in method
        '''

        # System generated locals
        d__1 = 0

        # / *Local variables * /
        i__ = 0
        r__ = 0
        hi = 0

        # / *Parameter adjustments * /
        # z__ -= 1

        # / *Function Body * /
        if (ity == 0):
            h__ = 0.
            for i__ in range(1, 9):
                # / * Computing 2nd power * /
                d__1 = t
                hi = t * (self.const_.ah[i__ - 1] + self.const_.bh[i__ - 1] * t / 2. + self.const_.ch[i__ - 1] * (d__1 * d__1) / 3.)
                hi *= 1.8
                h__ += z__[i__ - 1 + start_index] * self.const_.xmw[i__ - 1] * hi
                # / * L100: *
        else:
            h__ = 0.
            for i__ in range(1, 9):
                # / * Computing 2nd power * /
                d__1 = t
                hi = t * (self.const_.ag[i__ - 1] + self.const_.bg[i__ - 1] * t / 2. + self.const_.cg[i__ - 1] * (d__1 * d__1) / 3.)
                hi *= 1.8
                hi += self.const_.av[i__ - 1]
                h__ += z__[i__ - 1 + start_index] * self.const_.xmw[i__ - 1] * hi
                # / * L200: * /
        if (ity == 2):
            r__ = 3.57696e-6
            h__ -= r__ * ( t + 273.15)

        return h__

    def tesub2_(self, z__, t, h__, ity):
        '''

        :param z__: float array
        :param t: float value
        :param h__: float value
        :param ity: const integer value
        :return: t
        '''
        j = 0
        htest = 0
        dh = 0
        dt = 0
        err = 0
        tin = 0

        # / *Parameter adjustments * /
        # z__ -= 1

        # / *Function   Body * /
        tin = t
        for j in range(1, 101):
            htest = self.tesub1_(z__, t, htest, ity)
            err = htest - h__
            dh = self.tesub3_(z__, t, dh, ity)
            dt = -err / dh
            t += dt
        # / * L250: * /
        if (abs(dt) < 1e-12):
            return t
        t = tin

        return t

    def tesub3_(self, z__, t, dh, ity):
        '''

        :param z__: float array
        :param t: float variable
        :param dh: float variable
        :param ity: int variable
        :return: dh
        '''
        # / *System   generated locals * /
        d__1 = 0
        # / *Local  variables * /
        i__ = 0

        r__ = 0
        dhi = 0

        # / *Parameter  adjustments * /
        # --z__

        # / *Function  Body * /
        if (ity == 0):
            dh = 0.
            for i__ in range(1, 9):
                # / * Computing 2nd power * /
                d__1 = t
                dhi = self.const_.ah[i__ - 1] + self.const_.bh[i__ - 1] * t + self.const_.ch[i__ - 1] * (d__1 * d__1)
                dhi *= 1.8
                dh += z__[i__-1] * self.const_.xmw[i__ - 1] * dhi
                # / * L100: *
        else:
            dh = 0.
            for i__ in range(1, 9):
                # / * Computing 2nd power * /
                d__1 = t
                dhi = self.const_.ag[i__ - 1] + self.const_.bg[i__ - 1] * t + self.const_.cg[i__ - 1] * (d__1 * d__1)
                dhi *= 1.8
                dh += z__[i__-1] * self.const_.xmw[i__ - 1] * dhi
                # / * L200: * /
        if (ity == 2):
            r__ = 3.57696e-6
            dh -= r__

        return dh

    def tesub4_(self, x, t, r__):
        i__ = 0
        v = 0

        # / *Parameter adjustments * /
        # --x

        # / *Function Body * /
        v = 0.
        for i__ in range(1, 9):
            v += x[i__-1] * self.const_.xmw[i__ - 1] / (self.const_.ad[i__ - 1] + (self.const_.bd[i__ - 1] + self.const_.cd[i__ - 1] * t) * t)
        # / * L10: * /

        r__ = 1. / v

        return r__

    def  tesub5_(self, s, sp, adist, bdist,
                cdist, ddist, tlast,
                tnext, hspan, hzero,
                sspan, szero, spspan,
                idvflag):
        # / *System   generated locals * /
        d__1 = 0

        # / *Local  variables * /
        h__ = 0
        i__ = 0
        s1 = 0
        s1p = 0

        i__ = -1
        h__ = hspan * self.tesub7_(i__) + hzero
        s1 = sspan * self.tesub7_(i__) * idvflag + szero
        s1p = spspan * self.tesub7_(i__) * idvflag
        adist = s
        bdist = sp

        # / *Computing 2nd power * /
        d__1 = h__
        cdist = ((s1 - s) * 3. - h__ * (s1p + sp * 2.)) / (d__1 * d__1)
        # / *Computing 3rd  power * /
        d__1 = h__
        ddist = ((s - s1) * 2. + h__ * (s1p + sp)) / (d__1 * (d__1 * d__1))
        tnext = tlast + h__

        # return [s, sp, adist, bdist, cdist, ddist, tlast, tnext]
        return [adist, bdist, cdist, ddist, tnext]


    def tesub6_(self, std, x):
        i__ = 0

        x = 0.
        for i__ in range(1, 13):
            x += self.tesub7_(i__)
        x = (x - 6.) * std
        return x

    def tesub7_(self, i__):
        # / *System generated locals * /
        ret_val = 0
        d__1 = 0
        c_b78 = 0

        def d_mod(a, b):
            return a - (np.int(a/b)*b)

        d__1 = self.randsd_.g * 9228907.
        c_b78 = 4294967296.
        self.randsd_.g = d_mod(d__1, c_b78)
        if (i__ >= 0):
            ret_val = self.randsd_.g / 4294967296.
        if (i__ < 0):
            ret_val = self.randsd_.g * 2. / 4294967296. - 1.

        return ret_val

    def tesub8_(self, i__, t):
        '''

        :param i__: int variable
        :param t: float variable
        :return: float variable
        '''
        # /* System generated locals */
        ret_val = 0

        # /* Local variables */
        h__ = 0
        h__ = t - self.wlk_.tlast[i__ - 1]
        ret_val = self.wlk_.adist[i__ - 1] + h__ * (self.wlk_.bdist[i__ - 1] + h__ * (self.wlk_.cdist[i__ - 1] + h__ * self.wlk_.ddist[i__ - 1]))

        return ret_val
