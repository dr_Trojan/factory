import numpy as np

# Pointer to the first element of the real data. Returns NULL in C (0 in Fortran) if there is no real data.
def mxGetPr(some_array):
    return some_array

class teprob_mod:
    # #/**************************************************************************
    # *                                                                         *
    # *             D E F I N I T I O N S   O F   C O N S T A N T S             *
    # *                                                                         *
    # ***************************************************************************
    #
    # ---------------------------------------------------------------------------
    # -                 S - F u n c t i o n - C o n s t a n t s                 -
    # -------------------------------------------------------------------------*/
    NX     = 50;
    NU         = 12;
    NIDV       = 28;
    NY         = 41;
    NYADD      = 32;
    NYDIST     = 21;
    NYMONITOR  = 62;
    NYCOMP     = 96;
    NPAR       = 3;
    #
    # #/*-------------------------------------------------------------------------
    # -            C o n s t a n t s   o f   P r o c e s s m o d e l            -
    # -------------------------------------------------------------------------*/
    c__50 = 50;
    c__12 = 12;
    c__21 = 21;
    c__153 = 153;
    c__586 = 586;
    c__139 = 139;
    c__6 = 6;
    c__1 = 1;
    c__0 = 0;
    c__41 = 41;
    c__2 = 2;
    c__3 = 3;
    c__4 = 4;
    c__5 = 5;
    c__7 = 7;
    c__8 = 8;
    c_b73 = 1.1544;
    c_b74 = .3735
    c__9 = 9;
    c__10 = 10;
    c__11 = 11;
    c_b123 = 4294967296.;
    c__13 = 13;
    c__14 = 14;
    c__15 = 15;
    c__16 = 16;
    c__17 = 17;
    c__18 = 18;
    c__19 = 19;
    c__20 = 20;

    SS_OPTION_EXCEPTION_FREE_CODE = 0x00000002
		
    def teinit(self, ModelData, nn, time, yy, yp, rseed, MSFlag):
        # #/*----------------------------- Variables -----------------------------*/
        # tcModelData
        #define isd ((doublereal *)&(tcModelData).dvec_ + 28)
        i__ = 0

        # #/*Typcast of Dataset Pointer*/
        tcModelData = ModelData

        # #/*Parameter adjustments*/
        # --yp;
        # --yy;
        #
        # #/*--------------------------- Function Body ---------------------------*/
        # #/*Evaluation of Structure Parameter*/
        if (MSFlag is None):
            tcModelData.MSFlag = 0;
        else:
            tcModelData.MSFlag = int(MSFlag);
        # } #//if (MSFlag == NULL){

        if((tcModelData.MSFlag & 0x8000) > 0):
            tcModelData.MSFlag = int(0x8000);
        # } #//if((tcModelData).MSFlag & 0x8000) > 0){

        # #/*Component Data*/
        (tcModelData).const_.xmw[0] = 2.;           #//in g/mol / kg/kmol
        (tcModelData).const_.xmw[1] = 25.4;         #//in g/mol / kg/kmol
        (tcModelData).const_.xmw[2] = 28.;          #//in g/mol / kg/kmol
        (tcModelData).const_.xmw[3] = 32.;          #//in g/mol / kg/kmol
        (tcModelData).const_.xmw[4] = 46.;          #//in g/mol / kg/kmol
        (tcModelData).const_.xmw[5] = 48.;          #//in g/mol / kg/kmol
        (tcModelData).const_.xmw[6] = 62.;          #//in g/mol / kg/kmol
        (tcModelData).const_.xmw[7] = 76.;          #//in g/mol / kg/kmol

        (tcModelData).const_.avp[0] = 0.;           #//in ln(Torr)
        (tcModelData).const_.avp[1] = 0.;           #//in ln(Torr)
        (tcModelData).const_.avp[2] = 0.;           #//in ln(Torr)
        (tcModelData).const_.avp[3] = 15.92;        #//in ln(Torr)
        (tcModelData).const_.avp[4] = 16.35;        #//in ln(Torr)
        (tcModelData).const_.avp[5] = 16.35;        #//in ln(Torr)
        (tcModelData).const_.avp[6] = 16.43;        #//in ln(Torr)
        (tcModelData).const_.avp[7] = 17.21;        #//in ln(Torr)
        (tcModelData).const_.bvp[0] = 0.;           #//in ln(Torr)*°C
        (tcModelData).const_.bvp[1] = 0.;           #//in ln(Torr)*°C
        (tcModelData).const_.bvp[2] = 0.;           #//in ln(Torr)*°C
        (tcModelData).const_.bvp[3] = -1444.;       #//in ln(Torr)*°C
        (tcModelData).const_.bvp[4] = -2114.;       #//in ln(Torr)*°C
        (tcModelData).const_.bvp[5] = -2114.;       #//in ln(Torr)*°C
        (tcModelData).const_.bvp[6] = -2748.;       #//in ln(Torr)*°C
        (tcModelData).const_.bvp[7] = -3318.;       #//in ln(Torr)*°C
        (tcModelData).const_.cvp[0] = 0.;           #//in °C
        (tcModelData).const_.cvp[1] = 0.;           #//in °C
        (tcModelData).const_.cvp[2] = 0.;           #//in °C
        (tcModelData).const_.cvp[3] = 259.;         #//in °C
        (tcModelData).const_.cvp[4] = 265.5;        #//in °C
        (tcModelData).const_.cvp[5] = 265.5;        #//in °C
        (tcModelData).const_.cvp[6] = 232.9;        #//in °C
        (tcModelData).const_.cvp[7] = 249.6;        #//in °C

        (tcModelData).const_.ad[0] = 1.;            #//in ?
        (tcModelData).const_.ad[1] = 1.;            #//in ?
        (tcModelData).const_.ad[2] = 1.;            #//in ?
        (tcModelData).const_.ad[3] = 23.3;          #//in ?
        (tcModelData).const_.ad[4] = 33.9;          #//in ?
        (tcModelData).const_.ad[5] = 32.8;          #//in ?
        (tcModelData).const_.ad[6] = 49.9;          #//in ?
        (tcModelData).const_.ad[7] = 50.5;          #//in ?
        (tcModelData).const_.bd[0] = 0.;            #//in ?
        (tcModelData).const_.bd[1] = 0.;            #//in ?
        (tcModelData).const_.bd[2] = 0.;            #//in ?
        (tcModelData).const_.bd[3] = -.07;          #//in ?
        (tcModelData).const_.bd[4] = -.0957;        #//in ?
        (tcModelData).const_.bd[5] = -.0995;        #//in ?
        (tcModelData).const_.bd[6] = -.0191;        #//in ?
        (tcModelData).const_.bd[7] = -.0541;        #//in ?
        (tcModelData).const_.cd[0] = 0.;            #//in ?
        (tcModelData).const_.cd[1] = 0.;            #//in ?
        (tcModelData).const_.cd[2] = 0.;            #//in ?
        (tcModelData).const_.cd[3] = -2e-4;         #//in ?
        (tcModelData).const_.cd[4] = -1.52e-4;      #//in ?
        (tcModelData).const_.cd[5] = -2.33e-4;      #//in ?
        (tcModelData).const_.cd[6] = -4.25e-4;      #//in ?
        (tcModelData).const_.cd[7] = -1.5e-4;       #//in ?

        (tcModelData).const_.ah[0] = 1e-6;                 #//in ?
        (tcModelData).const_.ah[1] = 1e-6;                 #//in ?
        (tcModelData).const_.ah[2] = 1e-6;                 #//in ?
        (tcModelData).const_.ah[3] = 9.6e-7;               #//in ?
        (tcModelData).const_.ah[4] = 5.73e-7;              #//in ?
        (tcModelData).const_.ah[5] = 6.52e-7;              #//in ?
        (tcModelData).const_.ah[6] = 5.15e-7;              #//in ?
        (tcModelData).const_.ah[7] = 4.71e-7;              #//in ?
        (tcModelData).const_.bh[0] = 0.;            #//in ?
        (tcModelData).const_.bh[1] = 0.;            #//in ?
        (tcModelData).const_.bh[2] = 0.;            #//in ?
        (tcModelData).const_.bh[3] = 8.7e-9;               #//in ?
        (tcModelData).const_.bh[4] = 2.41e-9;              #//in ?
        (tcModelData).const_.bh[5] = 2.18e-9;              #//in ?
        (tcModelData).const_.bh[6] = 5.65e-10;             #//in ?
        (tcModelData).const_.bh[7] = 8.7e-10;              #//in ?
        (tcModelData).const_.ch[0] = 0.;            #//in ?
        (tcModelData).const_.ch[1] = 0.;            #//in ?
        (tcModelData).const_.ch[2] = 0.;            #//in ?
        (tcModelData).const_.ch[3] = 4.81e-11;             #//in ?
        (tcModelData).const_.ch[4] = 1.82e-11;             #//in ?
        (tcModelData).const_.ch[5] = 1.94e-11;             #//in ?
        (tcModelData).const_.ch[6] = 3.82e-12;             #//in ?
        (tcModelData).const_.ch[7] = 2.62e-12;             #//in ?

        (tcModelData).const_.av[0] = 1e-6;                 #//in ?
        (tcModelData).const_.av[1] = 1e-6;                 #//in ?
        (tcModelData).const_.av[2] = 1e-6;                 #//in ?
        (tcModelData).const_.av[3] = 8.67e-5;              #//in ?
        (tcModelData).const_.av[4] = 1.6e-4;               #//in ?
        (tcModelData).const_.av[5] = 1.6e-4;               #//in ?
        (tcModelData).const_.av[6] = 2.25e-4;              #//in ?
        (tcModelData).const_.av[7] = 2.09e-4;              #//in ?
        (tcModelData).const_.ag[0] = 3.411e-6;             #//in ?
        (tcModelData).const_.ag[1] = 3.799e-7;             #//in ?
        (tcModelData).const_.ag[2] = 2.491e-7;             #//in ?
        (tcModelData).const_.ag[3] = 3.567e-7;             #//in ?
        (tcModelData).const_.ag[4] = 3.463e-7;             #//in ?
        (tcModelData).const_.ag[5] = 3.93e-7;              #//in ?
        (tcModelData).const_.ag[6] = 1.7e-7;               #//in ?
        (tcModelData).const_.ag[7] = 1.5e-7;               #//in ?
        (tcModelData).const_.bg[0] = 7.18e-10;             #//in ?
        (tcModelData).const_.bg[1] = 1.08e-9;              #//in ?
        (tcModelData).const_.bg[2] = 1.36e-11;             #//in ?
        (tcModelData).const_.bg[3] = 8.51e-10;             #//in ?
        (tcModelData).const_.bg[4] = 8.96e-10;             #//in ?
        (tcModelData).const_.bg[5] = 1.02e-9;              #//in ?
        (tcModelData).const_.bg[6] = 0.;                   #//in ?
        (tcModelData).const_.bg[7] = 0.;                   #//in ?
        (tcModelData).const_.cg[0] = 6e-13;                #//in ?
        (tcModelData).const_.cg[1] = -3.98e-13;            #//in ?
        (tcModelData).const_.cg[2] = -3.93e-14;            #//in ?
        (tcModelData).const_.cg[3] = -3.12e-13;            #//in ?
        (tcModelData).const_.cg[4] = -3.27e-13;            #//in ?
        (tcModelData).const_.cg[5] = -3.12e-13;            #//in ?
        (tcModelData).const_.cg[6] = 0.;                   #//in ?
        (tcModelData).const_.cg[7] = 0.;                   #//in ?

        #/*Initial State of Process (Mode 1)*/
        yy[-1+1] = 10.40491389;                         #//in lb-mol
        yy[-1+2] = 4.363996017;                         #//in lb-mol
        yy[-1+3] = 7.570059737;                         #//in lb-mol
        yy[-1+4] = .4230042431;                         #//in lb-mol
        yy[-1+5] = 24.15513437;                         #//in lb-mol
        yy[-1+6] = 2.942597645;                         #//in lb-mol
        yy[-1+7] = 154.3770655;                         #//in lb-mol
        yy[-1+8] = 159.186596;                          #//in lb-mol
        yy[-1+9] = 2.808522723;                         #//in ?
        yy[-1+10] = 63.75581199;                        #//in lb-mol
        yy[-1+11] = 26.74026066;                        #//in lb-mol
        yy[-1+12] = 46.38532432;                        #//in lb-mol
        yy[-1+13] = .2464521543;                        #//in lb-mol
        yy[-1+14] = 15.20484404;                        #//in lb-mol
        yy[-1+15] = 1.852266172;                        #//in lb-mol
        yy[-1+16] = 52.44639459;                        #//in lb-mol
        yy[-1+17] = 41.20394008;                        #//in lb-mol
        yy[-1+18] = .569931776;                         #//in ?
        yy[-1+19] = .4306056376;                        #//in ?
        yy[-1+20] = .0079906200783;                            #//in ?
        yy[-1+21] = .9056036089;                        #//in ?
        yy[-1+22] = .016054258216;                             #//in ?
        yy[-1+23] = .7509759687;                        #//in ?
        yy[-1+24] = .088582855955;                             #//in ?
        yy[-1+25] = 48.27726193;                        #//in ?
        yy[-1+26] = 39.38459028;                        #//in ?
        yy[-1+27] = .3755297257;                        #//in ?
        yy[-1+28] = 107.7562698;                        #//in ?
        yy[-1+29] = 29.77250546;                        #//in ?
        yy[-1+30] = 88.32481135;                        #//in ?
        yy[-1+31] = 23.03929507;                        #//in ?
        yy[-1+32] = 62.85848794;                        #//in ?
        yy[-1+33] = 5.546318688;                        #//in ?
        yy[-1+34] = 11.92244772;                        #//in ?
        yy[-1+35] = 5.555448243;                        #//in ?
        yy[-1+36] = .9218489762;                        #//in ?
        #/*Cooling Water Outlet Temperatures*/
        yy[-1+37] = 94.59927549;                        #//in °C
        yy[-1+38] = 77.29698353;                        #//in °C
        #/*Valve Positions*/
        yy[-1+39] = 63.05263039;                        #//in %
        yy[-1+40] = 53.97970677;                        #//in %
        yy[-1+41] = 24.64355755;                        #//in %
        yy[-1+42] = 61.30192144;                        #//in %
        yy[-1+43] = 22.21;                              #//in %
        yy[-1+44] = 40.06374673;                        #//in %
        yy[-1+45] = 38.1003437;                         #//in %
        yy[-1+46] = 46.53415582;                        #//in %
        yy[-1+47] = 47.44573456;                        #//in %
        yy[-1+48] = 41.10581288;                        #//in %
        yy[-1+49] = 18.11349055;                        #//in %
        yy[-1+50] = 50.;                                #//in %


        # for (i__ = 1; i__ <= 12; ++i__):
        for i__ in range(1,13):
            (tcModelData).pv_.xmv[i__ - 1] = yy[-1+i__ + 38];

            (tcModelData).teproc_.vcv[i__ - 1] = (tcModelData).pv_.xmv[i__ - 1];
            (tcModelData).teproc_.vst[i__ - 1] = 2.;         #//in %

            (tcModelData).teproc_.ivst[i__ - 1] = 0.; #//in 1
        #/* L200: */


        #/*Nominal Flowrate through Valves*/
        (tcModelData).teproc_.vrng[0] = 400.;       #//in lb-mol/h
        (tcModelData).teproc_.vrng[1] = 400.;       #//in lb-mol/h
        (tcModelData).teproc_.vrng[2] = 100.;       #//in lb-mol/h
        (tcModelData).teproc_.vrng[3] = 1500.;      #//in lb-mol/h
        (tcModelData).teproc_.vrng[6] = 1500.;      #//in lb-mol/h
        (tcModelData).teproc_.vrng[7] = 1e3;        #//in lb-mol/h
        (tcModelData).teproc_.vrng[8] = .03;        #//in ?
        (tcModelData).teproc_.vrng[9] = 1e3;        #//in gpm
        (tcModelData).teproc_.vrng[10] = 1200.;     #//in gpm

        #/*Volumes of Vessels*/
        (tcModelData).teproc_.vtr = 1300.;          #//in ftі
        (tcModelData).teproc_.vts = 3500.;          #//in ftі
        (tcModelData).teproc_.vtc = 156.5;          #//in ftі
        (tcModelData).teproc_.vtv = 5e3;            #//in ?

        (tcModelData).teproc_.htr[0] = .06899381054;       #//in ?
        (tcModelData).teproc_.htr[1] = .05;                #//in ?
        (tcModelData).teproc_.hwr = 7060.;          #//in ?
        (tcModelData).teproc_.hws = 11138.;         #//in ?
        (tcModelData).teproc_.sfr[0] = .995;        #//in 1
        (tcModelData).teproc_.sfr[1] = .991;        #//in 1
        (tcModelData).teproc_.sfr[2] = .99;         #//in 1
        (tcModelData).teproc_.sfr[3] = .916;        #//in 1
        (tcModelData).teproc_.sfr[4] = .936;        #//in 1
        (tcModelData).teproc_.sfr[5] = .938;        #//in 1
        (tcModelData).teproc_.sfr[6] = .058;               #//in 1
        (tcModelData).teproc_.sfr[7] = .0301;              #//in 1

        #/*Stream 2*/
        (tcModelData).teproc_.xst[0] = 0.;          #//in mol%
        (tcModelData).teproc_.xst[1] = 1e-4;        #//in mol%
        (tcModelData).teproc_.xst[2] = 0.;          #//in mol%
        (tcModelData).teproc_.xst[3] = .9999;       #//in mol%
        (tcModelData).teproc_.xst[4] = 0.;          #//in mol%
        (tcModelData).teproc_.xst[5] = 0.;          #//in mol%
        (tcModelData).teproc_.xst[6] = 0.;          #//in mol%
        (tcModelData).teproc_.xst[7] = 0.;          #//in mol%
        (tcModelData).teproc_.tst[0] = 45.;         #//in °C

        #/*Stream 3*/
        (tcModelData).teproc_.xst[8] = 0.;          #//in mol%
        (tcModelData).teproc_.xst[9] = 0.;          #//in mol%
        (tcModelData).teproc_.xst[10] = 0.;         #//in mol%
        (tcModelData).teproc_.xst[11] = 0.;         #//in mol%
        (tcModelData).teproc_.xst[12] = .9999;      #//in mol%
        (tcModelData).teproc_.xst[13] = 1e-4;       #//in mol%
        (tcModelData).teproc_.xst[14] = 0.;         #//in mol%
        (tcModelData).teproc_.xst[15] = 0.;         #//in mol%
        (tcModelData).teproc_.tst[1] = 45.;         #//in °C

        #/*Stream 1*/
        (tcModelData).teproc_.xst[16] = .9999;      #//in mol%
        (tcModelData).teproc_.xst[17] = 1e-4;       #//in mol%
        (tcModelData).teproc_.xst[18] = 0.;         #//in mol%
        (tcModelData).teproc_.xst[19] = 0.;         #//in mol%
        (tcModelData).teproc_.xst[20] = 0.;         #//in mol%
        (tcModelData).teproc_.xst[21] = 0.;         #//in mol%
        (tcModelData).teproc_.xst[22] = 0.;         #//in mol%
        (tcModelData).teproc_.xst[23] = 0.;         #//in mol%
        (tcModelData).teproc_.tst[2] = 45.;         #//in °C

        #/*Stream 4*/
        (tcModelData).teproc_.xst[24] = .485;       #//in mol%
        (tcModelData).teproc_.xst[25] = .005;       #//in mol%
        (tcModelData).teproc_.xst[26] = .51;        #//in mol%
        (tcModelData).teproc_.xst[27] = 0.;         #//in mol%
        (tcModelData).teproc_.xst[28] = 0.;         #//in mol%
        (tcModelData).teproc_.xst[29] = 0.;         #//in mol%
        (tcModelData).teproc_.xst[30] = 0.;         #//in mol%
        (tcModelData).teproc_.xst[31] = 0.;         #//in mol%
        (tcModelData).teproc_.tst[3] = 45.;         #//in °C

        (tcModelData).teproc_.cpflmx = 280275.;     #//in ?
        (tcModelData).teproc_.cpprmx = 1.3;         #//in ?

        #/*Time Constants of Valves*/
        (tcModelData).teproc_.vtau[0] = 8.;         #//in seconds
        (tcModelData).teproc_.vtau[1] = 8.;         #//in seconds
        (tcModelData).teproc_.vtau[2] = 6.;         #//in seconds
        (tcModelData).teproc_.vtau[3] = 9.;         #//in seconds
        (tcModelData).teproc_.vtau[4] = 7.;         #//in seconds
        (tcModelData).teproc_.vtau[5] = 5.;         #//in seconds
        (tcModelData).teproc_.vtau[6] = 5.;         #//in seconds
        (tcModelData).teproc_.vtau[7] = 5.;         #//in seconds
        (tcModelData).teproc_.vtau[8] = 120.;       #//in seconds
        (tcModelData).teproc_.vtau[9] = 5.;         #//in seconds
        (tcModelData).teproc_.vtau[10] = 5.;        #//in seconds
        (tcModelData).teproc_.vtau[11] = 5.;        #//in seconds
        for i__ in range(1,13):
            (tcModelData).teproc_.vtau[i__ - 1] /= 3600.;
        #/* L300: */
        # } #//for (i__ = 1; i__ <= 12; ++i__){

        #/*Seed of Random Generator*/
        if (rseed is None):
            (tcModelData).randsd_.g = 1431655765.;
            (tcModelData).randsd_.measnoise = 1431655765.;
            (tcModelData).randsd_.procdist = 1431655765.;
        else:# В нижней части может быть херня
            (tcModelData).randsd_.g = rseed;
            (tcModelData).randsd_.measnoise = rseed;
            (tcModelData).randsd_.procdist = rseed;
         #//if (rseed == NULL){

        #/*Amplitudes of Measurement Noise*/
        if (((tcModelData).MSFlag & 0x10) > 0):
            for i__ in range(1,42):
                (tcModelData).teproc_.xns[i__ - 1] = 0.;


            for i__ in range (1,35):
              (tcModelData).teproc_.xnsadd[i__ - 1] = 0.;
                 #//for (i__ = 1; i__ <= 13; ++i__){
        else:
            (tcModelData).teproc_.xns[0] = .0012;            #//in kscmh
            (tcModelData).teproc_.xns[1] = 18.;              #//in kg/h
            (tcModelData).teproc_.xns[2] = 22.;              #//in kg/h
            (tcModelData).teproc_.xns[3] = .05;              #//in kscmh
            (tcModelData).teproc_.xns[4] = .2;               #//in kscmh
            (tcModelData).teproc_.xns[5] = .21;              #//in kscmh
            (tcModelData).teproc_.xns[6] = .3;               #//in kPa gauge
            (tcModelData).teproc_.xns[7] = .5;               #//in %
            (tcModelData).teproc_.xns[8] = .01;              #//in °C
            (tcModelData).teproc_.xns[9] = .0017;            #//in kscmh
            (tcModelData).teproc_.xns[10] = .01;             #//in °C
            (tcModelData).teproc_.xns[11] = 1.;              #//in %
            (tcModelData).teproc_.xns[12] = .3;              #//in kPa gauge
            (tcModelData).teproc_.xns[13] = .125;            #//in mі/h
            (tcModelData).teproc_.xns[14] = 1.;              #//in %
            (tcModelData).teproc_.xns[15] = .3;              #//in kPa gauge
            (tcModelData).teproc_.xns[16] = .115;            #//in mі/h
            (tcModelData).teproc_.xns[17] = .01;             #//in °C
            (tcModelData).teproc_.xns[18] = 1.15;            #//in kg/h
            (tcModelData).teproc_.xns[19] = .2;              #//in kW
            (tcModelData).teproc_.xns[20] = .01;             #//in °C
            (tcModelData).teproc_.xns[21] = .01;             #//in °C
            (tcModelData).teproc_.xns[22] = .25;             #//in mol %
            (tcModelData).teproc_.xns[23] = .1;              #//in mol %
            (tcModelData).teproc_.xns[24] = .25;             #//in mol %
            (tcModelData).teproc_.xns[25] = .1;              #//in mol %
            (tcModelData).teproc_.xns[26] = .25;             #//in mol %
            (tcModelData).teproc_.xns[27] = .025;            #//in mol %
            (tcModelData).teproc_.xns[28] = .25;             #//in mol %
            (tcModelData).teproc_.xns[29] = .1;              #//in mol %
            (tcModelData).teproc_.xns[30] = .25;             #//in mol %
            (tcModelData).teproc_.xns[31] = .1;              #//in mol %
            (tcModelData).teproc_.xns[32] = .25;             #//in mol %
            (tcModelData).teproc_.xns[33] = .025;            #//in mol %
            (tcModelData).teproc_.xns[34] = .05;             #//in mol %
            (tcModelData).teproc_.xns[35] = .05;             #//in mol %
            (tcModelData).teproc_.xns[36] = .01;             #//in mol %
            (tcModelData).teproc_.xns[37] = .01;             #//in mol %
            (tcModelData).teproc_.xns[38] = .01;             #//in mol %
            (tcModelData).teproc_.xns[39] = .5;              #//in mol %
            (tcModelData).teproc_.xns[40] = .5;              #//in mol %

            (tcModelData).teproc_.xnsadd[0] = .01;           #//in °C
            (tcModelData).teproc_.xnsadd[1] = .01;           #//in °C
            (tcModelData).teproc_.xnsadd[2] = .01;           #//in °C
            (tcModelData).teproc_.xnsadd[3] = .01;           #//in °C
            (tcModelData).teproc_.xnsadd[4] = .01;           #//in °C
            (tcModelData).teproc_.xnsadd[5] = .125;          #//in mі/h
            (tcModelData).teproc_.xnsadd[6] = .01;           #//in °C
            (tcModelData).teproc_.xnsadd[7] = .125;          #//in mі/h
            (tcModelData).teproc_.xnsadd[8] = .01;           #//in °C
            (tcModelData).teproc_.xnsadd[9] = .01;           #//in °C
            (tcModelData).teproc_.xnsadd[10] = .25;          #//in mol %
            (tcModelData).teproc_.xnsadd[11] = .1;           #//in mol %
            (tcModelData).teproc_.xnsadd[12] = .25;          #//in mol %
            (tcModelData).teproc_.xnsadd[13] = .1;           #//in mol %
            (tcModelData).teproc_.xnsadd[14] = .25;          #//in mol %
            (tcModelData).teproc_.xnsadd[15] = .025;         #//in mol %
            (tcModelData).teproc_.xnsadd[16] = .25;          #//in mol %
            (tcModelData).teproc_.xnsadd[17] = .1;           #//in mol %
            (tcModelData).teproc_.xnsadd[18] = .25;          #//in mol %
            (tcModelData).teproc_.xnsadd[19] = .1;           #//in mol %
            (tcModelData).teproc_.xnsadd[20] = .25;          #//in mol %
            (tcModelData).teproc_.xnsadd[21] = .025;         #//in mol %
            (tcModelData).teproc_.xnsadd[22] = .25;          #//in mol %
            (tcModelData).teproc_.xnsadd[23] = .1;           #//in mol %
            (tcModelData).teproc_.xnsadd[24] = .25;          #//in mol %
            (tcModelData).teproc_.xnsadd[25] = .1;           #//in mol %
            (tcModelData).teproc_.xnsadd[26] = .25;          #//in mol %
            (tcModelData).teproc_.xnsadd[27] = .025;         #//in mol %
            (tcModelData).teproc_.xnsadd[28] = .25;          #//in mol %
            (tcModelData).teproc_.xnsadd[29] = .1;           #//in mol %
            (tcModelData).teproc_.xnsadd[30] = .25;          #//in mol %
            (tcModelData).teproc_.xnsadd[31] = .1;           #//in mol %
            (tcModelData).teproc_.xnsadd[32] = .25;          #//in mol %
            (tcModelData).teproc_.xnsadd[33] = .025;         #//in mol %

        #/*Initilization of Disturbance Flags*/
        for i__ in range (1,29):
            (tcModelData).dvec_.idv[i__ - 1] = 0.;    #//in 1
        #/* L500: */

        #/*Data of Disturbance Processes*/
        (tcModelData).wlk_.hspan[0] = .2;                  #//in h
        (tcModelData).wlk_.hzero[0] = .5;                  #//in h
        (tcModelData).wlk_.sspan[0] = .03;                 #//in mole-ratio
        (tcModelData).wlk_.szero[0] = .485;                #//in mole-ratio
        (tcModelData).wlk_.spspan[0] = 0.;                 #//in mole-ratio/h

        (tcModelData).wlk_.hspan[1] = .7;                  #//in h
        (tcModelData).wlk_.hzero[1] = 1.;                  #//in h
        (tcModelData).wlk_.sspan[1] = .003;                #//in mole-ratio
        (tcModelData).wlk_.szero[1] = .005;                #//in mole-ratio
        (tcModelData).wlk_.spspan[1] = 0.;                 #//in mole-ratio/h

        (tcModelData).wlk_.hspan[2] = .25;                 #//in h
        (tcModelData).wlk_.hzero[2] = .5;                  #//in h
        (tcModelData).wlk_.sspan[2] = 10.;                 #//in °C
        (tcModelData).wlk_.szero[2] = 45.;                 #//in °C
        (tcModelData).wlk_.spspan[2] = 0.;                 #//in °C/h

        (tcModelData).wlk_.hspan[3] = .7;                  #//in h
        (tcModelData).wlk_.hzero[3] = 1.;                  #//in h
        (tcModelData).wlk_.sspan[3] = 10.;                 #//in °C
        (tcModelData).wlk_.szero[3] = 45.;                 #//in °C
        (tcModelData).wlk_.spspan[3] = 0.;                 #//in °C/h

        (tcModelData).wlk_.hspan[4] = .15;                 #//in h
        (tcModelData).wlk_.hzero[4] = .25;                 #//in h
        (tcModelData).wlk_.sspan[4] = 10.;                 #//in °C
        (tcModelData).wlk_.szero[4] = 35.;                 #//in °C
        (tcModelData).wlk_.spspan[4] = 0.;                 #//in °C/h

        (tcModelData).wlk_.hspan[5] = .15;                 #//in h
        (tcModelData).wlk_.hzero[5] = .25;                 #//in h
        (tcModelData).wlk_.sspan[5] = 10.;                 #//in °C
        (tcModelData).wlk_.szero[5] = 40.;                 #//in °C
        (tcModelData).wlk_.spspan[5] = 0.;                 #//in °C/h

        (tcModelData).wlk_.hspan[6] = 1.;                  #//in h
        (tcModelData).wlk_.hzero[6] = 2.;                  #//in h
        (tcModelData).wlk_.sspan[6] = .25;                 #//in 1
        (tcModelData).wlk_.szero[6] = 1.;                  #//in 1
        (tcModelData).wlk_.spspan[6] = 0.;                 #//in 1/h

        (tcModelData).wlk_.hspan[7] = 1.;                  #//in h
        (tcModelData).wlk_.hzero[7] = 2.;                  #//in h
        (tcModelData).wlk_.sspan[7] = .25;                 #//in 1
        (tcModelData).wlk_.szero[7] = 1.;                  #//in 1
        (tcModelData).wlk_.spspan[7] = 0.;                 #//in 1/h

        (tcModelData).wlk_.hspan[8] = .4;                  #//in h
        (tcModelData).wlk_.hzero[8] = .5;                  #//in h
        (tcModelData).wlk_.sspan[8] = .25;                 #//in ?
        (tcModelData).wlk_.szero[8] = 0.;                  #//in ?
        (tcModelData).wlk_.spspan[8] = 0.;                 #//in ?/h

        (tcModelData).wlk_.hspan[9] = 1.5;                 #//in h
        (tcModelData).wlk_.hzero[9] = 2.;                  #//in h
        (tcModelData).wlk_.sspan[9] = 0.;                  #//in ?
        (tcModelData).wlk_.szero[9] = 0.;                  #//in ?
        (tcModelData).wlk_.spspan[9] = 0.;                 #//in ?/h

        (tcModelData).wlk_.hspan[10] = 2.;                 #//in h
        (tcModelData).wlk_.hzero[10] = 3.;                 #//in h
        (tcModelData).wlk_.sspan[10] = 0.;                 #//in ?
        (tcModelData).wlk_.szero[10] = 0.;                 #//in ?
        (tcModelData).wlk_.spspan[10] = 0.;                #//in ?/h

        (tcModelData).wlk_.hspan[11] = 1.5;                #//in h
        (tcModelData).wlk_.hzero[11] = 2.;                 #//in h
        (tcModelData).wlk_.sspan[11] = 0.;                 #//in ?
        (tcModelData).wlk_.szero[11] = 0.;                 #//in ?
        (tcModelData).wlk_.spspan[11] = 0.;                #//in ?/h

        (tcModelData).wlk_.hspan[12] = .15;                #//in h
        (tcModelData).wlk_.hzero[12] = .25;                #//in h
        (tcModelData).wlk_.sspan[12] = 10.;                #//in °C
        (tcModelData).wlk_.szero[12] = 45.;                #//in °C
        (tcModelData).wlk_.spspan[12] = 0.;                #//in °C/h

        (tcModelData).wlk_.hspan[13] = .25;                #//in h
        (tcModelData).wlk_.hzero[13] = .5;                 #//in h
        (tcModelData).wlk_.sspan[13] = 10.;                #//in °C
        (tcModelData).wlk_.szero[13] = 45.;                #//in °C
        (tcModelData).wlk_.spspan[13] = 0.;                #//in °C/h

        (tcModelData).wlk_.hspan[14] = .15;                #//in h
        (tcModelData).wlk_.hzero[14] = .25;                #//in h
        (tcModelData).wlk_.sspan[14] = 5.;                 #//in lb-mol/h
        (tcModelData).wlk_.szero[14] = 100.;               #//in lb-mol/h
        (tcModelData).wlk_.spspan[14] = 0.;                #//in lb-mol/hІ

        (tcModelData).wlk_.hspan[15] = .25;                #//in h
        (tcModelData).wlk_.hzero[15] = .5;                 #//in h
        (tcModelData).wlk_.sspan[15] = 20.;                #//in lb-mol/h
        (tcModelData).wlk_.szero[15] = 400.;               #//in lb-mol/h
        (tcModelData).wlk_.spspan[15] = 0.;                #//in lb-mol/hІ

        (tcModelData).wlk_.hspan[16] = .25;                #//in h
        (tcModelData).wlk_.hzero[16] = .5;                 #//in h
        (tcModelData).wlk_.sspan[16] = 20.;                #//in lb-mol/h
        (tcModelData).wlk_.szero[16] = 400.;               #//in lb-mol/h
        (tcModelData).wlk_.spspan[16] = 0.;                #//in lb-mol/hІ

        (tcModelData).wlk_.hspan[17] = .7;                 #//in h
        (tcModelData).wlk_.hzero[17] = 1.;                 #//in h
        (tcModelData).wlk_.sspan[17] = 75.;                #//in lb-mol/h
        (tcModelData).wlk_.szero[17] = 1500.;              #//in lb-mol/h
        (tcModelData).wlk_.spspan[17] = 0.;                #//in lb-mol/hІ

        (tcModelData).wlk_.hspan[18] = .1;                 #//in h
        (tcModelData).wlk_.hzero[18] = .2;                 #//in h
        (tcModelData).wlk_.sspan[18] = 50.;                #//in gpm
        (tcModelData).wlk_.szero[18] = 1e3;         #//in gpm
        (tcModelData).wlk_.spspan[18] = 0.;                #//in gpm/h

        (tcModelData).wlk_.hspan[19] = .1;                 #//in h
        (tcModelData).wlk_.hzero[19] = .2;                 #//in h
        (tcModelData).wlk_.sspan[19] = 60.;                #//in gpm
        (tcModelData).wlk_.szero[19] = 1200.;              #//in gpm
        (tcModelData).wlk_.spspan[19] = 0.;                #//in gpm/h

        #/*Initilization of Disturbance Processes Parameters*/
        for i__ in range (1,21):
            (tcModelData).wlk_.tlast[i__ - 1] = 0.;          #//in h
            (tcModelData).wlk_.tnext[i__ - 1] = .1;          #//in h
            (tcModelData).wlk_.adist[i__ - 1] =  (tcModelData).wlk_.szero[i__ - 1]; #//unit depends on value
            (tcModelData).wlk_.bdist[i__ - 1] = 0.;          #//in x/h
            (tcModelData).wlk_.cdist[i__ - 1] = 0.;          #//in x/hІ
            (tcModelData).wlk_.ddist[i__ - 1] = 0.;          #//in x/hі
            #/* L550: */

        (tcModelData).tlastcomp = -1.;
        time = 0.;
        # return tcModelData
        return self.tefunc(ModelData, nn, time, yy, yp, 0)


    def tefunc(self, ModelData, nn, time, yy, yp, Callflag):
# /*----------------------------- Variables -----------------------------*/
        # integer i__1;
        # doublereal d__1;
        #
        # struct stModelData *tcModelData;
        # int distende;
        # int distindex[16];
        # int distnum;
        # int distindch;
        # doublereal flms;
        # doublereal xcmp[41];
        # doublereal xcmpadd[24];
        # doublereal hwlk;
        # doublereal vpos[12];
        # doublereal xmns;
        # doublereal swlk;
        # doublereal spwlk;
        # doublereal vovrl;
        # doublereal rg;
        # doublereal flcoef;
        # doublereal pr;
        # doublereal tmpfac;
        # doublereal uarlev;
        # doublereal r1f;
        # doublereal r2f;
        # doublereal uac;
        # doublereal fin[8];
        # doublereal dlp;
        # doublereal vpr;
        # doublereal uas;
        # doublereal prate;
        # integer aux;
        # integer i__;
        # integer j__;
        # #define isd ((doublereal *)&(tcModelData).dvec_ + 28)
        xcmp = np.zeros(41)
        distindex = np.zeros(16)
        xcmpadd = np.zeros(24)
        vpos = np.zeros(12)
        fin = np.zeros(8)

# /*Typcast of Dataset Pointer*/
        tcModelData = ModelData;

        # /*Parameter adjustments*/
        # --yp;
        # --yy;

# /*--------------------------- Function Body ---------------------------*/
        if((((tcModelData).tlastcomp != time) or (Callflag == 1)) and
        (((tcModelData).MSFlag & 0x8000) == 0)):
            # /*Limiting of Disturbance Activations*/
            for i__ in range (1,29):
            # for (i__ = 1; i__ <= 28; ++i__)
                if ((tcModelData).dvec_.idv[i__ - 1] < 0.):
                    (tcModelData).dvec_.idv[i__ - 1] = 0.;

                if ((tcModelData).dvec_.idv[i__ - 1] > 1.):
                    (tcModelData).dvec_.idv[i__ - 1] = 1.;

                # /* L500: */


            # /*Assignment of Disturbance Activations*/
            (tcModelData).wlk_.idvwlk[0]  = (tcModelData).dvec_.idv[7];
            (tcModelData).wlk_.idvwlk[1]  = (tcModelData).dvec_.idv[7];
            (tcModelData).wlk_.idvwlk[2]  = (tcModelData).dvec_.idv[8];
            (tcModelData).wlk_.idvwlk[3]  = (tcModelData).dvec_.idv[9];
            (tcModelData).wlk_.idvwlk[4]  = (tcModelData).dvec_.idv[10];
            (tcModelData).wlk_.idvwlk[5]  = (tcModelData).dvec_.idv[11];
            (tcModelData).wlk_.idvwlk[6]  = (tcModelData).dvec_.idv[12];
            (tcModelData).wlk_.idvwlk[7]  = (tcModelData).dvec_.idv[12];
            (tcModelData).wlk_.idvwlk[8]  = (tcModelData).dvec_.idv[15];
            (tcModelData).wlk_.idvwlk[9]  = (tcModelData).dvec_.idv[16];
            (tcModelData).wlk_.idvwlk[10] = (tcModelData).dvec_.idv[17];
            (tcModelData).wlk_.idvwlk[11] = (tcModelData).dvec_.idv[19];
            (tcModelData).wlk_.idvwlk[12] = (tcModelData).dvec_.idv[20];
            (tcModelData).wlk_.idvwlk[13] = (tcModelData).dvec_.idv[21];
            (tcModelData).wlk_.idvwlk[14] = (tcModelData).dvec_.idv[22];
            (tcModelData).wlk_.idvwlk[15] = (tcModelData).dvec_.idv[23];
            (tcModelData).wlk_.idvwlk[16] = (tcModelData).dvec_.idv[24];
            (tcModelData).wlk_.idvwlk[17] = (tcModelData).dvec_.idv[25];
            (tcModelData).wlk_.idvwlk[18] = (tcModelData).dvec_.idv[26];
            (tcModelData).wlk_.idvwlk[19] = (tcModelData).dvec_.idv[27];

            if (((tcModelData).MSFlag & 0x40) > 0):
                # /*Recalculation of Disturbance Process Parameters - Determination of Processes to be Updated (1 - 9/13 - 20)*/
                distnum = 0;
                for i__ in range (1,21):
                # for (i__ = 1; i__ <= 20; ++i__)
                    if (time >= (tcModelData).wlk_.tnext[i__ - 1]):
                        distindex[distnum] = i__ - 1;
                        distnum+=1;


                    # /*Step-over of 10 through 12*/
                    if(i__ == 9):
                        i__ = i__ + 3;


                # /*Recalculation of Disturbance Process Parameters - Sorting (1 - 9/13 - 20)*/
                # for(i__ = 1; i__ < distnum; ++i__)
                for i__ in range(1,distnum):
                    distindch = distindex[i__];
                    j__ = i__;
                    while((j__ > 0) and
                    ((tcModelData).wlk_.tnext[j__ - 1] >
                                        (tcModelData).wlk_.tnext[distindch])):
                        distindex[j__] = distindex[j__ - 1];
                        j__-=1;
                    distindex[j__] = distindch;


                # /*Recalculation of Disturbance Process Parameters - Update (1 - 9/ 13 - 20)*/
                for i__ in range (distnum):
                # for(i__ = 0; i__ < distnum; ++i__){
                    hwlk = (tcModelData).wlk_.tnext[distindex[i__]] -(tcModelData).wlk_.tlast[distindex[i__]];

                    swlk = (tcModelData).wlk_.adist[distindex[i__]] +\
                           hwlk * ((tcModelData).wlk_.bdist[distindex[i__]] +
                                                                              hwlk * ((tcModelData).wlk_.cdist[distindex[i__]] +
                                                                                      hwlk * (tcModelData).wlk_.ddist[distindex[i__]]))

                    spwlk = \
                    (tcModelData).wlk_.bdist[distindex[i__]] + \
                    hwlk * ((tcModelData).wlk_.cdist[distindex[i__]] * 2. +
                        hwlk * 3. * (tcModelData).wlk_.ddist[distindex[i__]]);

                    (tcModelData).wlk_.tlast[distindex[i__]] =\
                                     (tcModelData).wlk_.tnext[distindex[i__]];

                    [(tcModelData).wlk_.adist[distindex[i__]],
                    (tcModelData).wlk_.bdist[distindex[i__]],
                    (tcModelData).wlk_.cdist[distindex[i__]],
                    (tcModelData).wlk_.ddist[distindex[i__]],
                     (tcModelData).wlk_.tnext[distindex[i__]]] = self.tesub5_(ModelData, swlk, spwlk,
                    (tcModelData).wlk_.adist[distindex[i__]],
                    (tcModelData).wlk_.bdist[distindex[i__]],
                    (tcModelData).wlk_.cdist[distindex[i__]],
                    (tcModelData).wlk_.ddist[distindex[i__]],
                    (tcModelData).wlk_.tlast[distindex[i__]],
                    (tcModelData).wlk_.tnext[distindex[i__]],
                    (tcModelData).wlk_.hspan[distindex[i__]],
                    (tcModelData).wlk_.hzero[distindex[i__]],
                    (tcModelData).wlk_.sspan[distindex[i__]],
                    (tcModelData).wlk_.szero[distindex[i__]],
                    (tcModelData).wlk_.spspan[distindex[i__]],
                    (tcModelData).wlk_.idvwlk[distindex[i__]]);


                # /*Recalculation of Disturbance Process Parameters - Determination of
                # Processes to be Updated (10 - 12)*/
                distnum = 0;
                for i__ in range (10,13):
                # for (i__ = 10; i__ <= 12; ++i__){
                    if (time >= (tcModelData).wlk_.tnext[i__ - 1]):
                        distindex[distnum] = i__ - 1;
                        distnum+=1 ;



                # /*Recalculation of Disturbance Process Parameters - Sorting (10 -
                # 12)*/
                for i__ in range (1, distnum):
                # for(i__ = 1; i__ < distnum; ++i__)
                    distindch = distindex[i__];
                    j__ = i__;
                    while((j__ > 0) and ((tcModelData).wlk_.tnext[j__ - 1] > (tcModelData).wlk_.tnext[distindch])):
                        distindex[j__] = distindex[j__ - 1];
                        j__-=1;

                    distindex[j__] = distindch;


                # /*Recalculation of Disturbance Process Parameters - Update (10 -
                # 12)*/
                for i__ in range (distnum):
                # for(i__ = 0; i__ < distnum; ++i__):
                    hwlk = (tcModelData).wlk_.tnext[distindex[i__]] -\
                                     (tcModelData).wlk_.tlast[distindex[i__]];

                    swlk = (tcModelData).wlk_.adist[distindex[i__]] +\
                    hwlk * ((tcModelData).wlk_.bdist[distindex[i__]] +\
                       hwlk * ((tcModelData).wlk_.cdist[distindex[i__]] +\
                           hwlk * (tcModelData).wlk_.ddist[distindex[i__]]));

                    spwlk =\
                    (tcModelData).wlk_.bdist[distindex[i__]] +\
                    hwlk * ((tcModelData).wlk_.cdist[distindex[i__]] * 2. +\
                        hwlk * 3. * (tcModelData).wlk_.ddist[distindex[i__]]);

                    (tcModelData).wlk_.tlast[distindex[i__]] =\
                                     (tcModelData).wlk_.tnext[distindex[i__]];
                    if (swlk > .1):
                        (tcModelData).wlk_.adist[distindex[i__]] = swlk;
                        (tcModelData).wlk_.bdist[distindex[i__]] = spwlk;
                        (tcModelData).wlk_.cdist[distindex[i__]] =\
                                                   -(swlk * 3. + spwlk * .2) / .01;
                        (tcModelData).wlk_.ddist[distindex[i__]] =\
                                                   (swlk * 2. + spwlk * .1) / .001;
                        (tcModelData).wlk_.tnext[distindex[i__]] =\
                                    (tcModelData).wlk_.tlast[distindex[i__]] + .1;
                    else:
                        aux = -1;
                        hwlk = (tcModelData).wlk_.hspan[distindex[i__]] *\
                                   self.tesub7_(ModelData, aux) +\
                                   (tcModelData).wlk_.hzero[distindex[i__]];
                        (tcModelData).wlk_.adist[distindex[i__]] = swlk;
                        (tcModelData).wlk_.bdist[distindex[i__]] = spwlk;

                        # /* Computing 2nd power */
                        d__1 = hwlk;
                        (tcModelData).wlk_.cdist[distindex[i__]] =\
                        ((tcModelData).wlk_.idvwlk[distindex[i__]] -
                                                     2*spwlk*d__1) / (d__1 * d__1);
                        (tcModelData).wlk_.ddist[distindex[i__]] =\
                                                             spwlk / (d__1 * d__1);
                        (tcModelData).wlk_.tnext[distindex[i__]] =\
                                  (tcModelData).wlk_.tlast[distindex[i__]] + hwlk;


                else:
                    # /* Original Code of J.J. Downs & E.F. Vogel and N.L. Ricker [2]*/
                    # /*Recalculation of Disturbance Process Parameters (1 - 9/13 - 20)*/
                    if(((tcModelData).MSFlag & 0x8000) > 0):
                        distende = 9;
                    else:
                        distende = 20;

                    for i__ in range (1, distende+1):
                    # for (i__ = 1; i__ <= distende; ++i__):
                        if (time >= (tcModelData).wlk_.tnext[i__ - 1]):
                            hwlk = (tcModelData).wlk_.tnext[i__ - 1] -\
                                                    (tcModelData).wlk_.tlast[i__ - 1];

                            swlk = (tcModelData).wlk_.adist[i__ - 1] +\
                             hwlk * ((tcModelData).wlk_.bdist[i__ - 1] +\
                                     hwlk * ((tcModelData).wlk_.cdist[i__ - 1] +\
                                           hwlk * (tcModelData).wlk_.ddist[i__ - 1]));

                            spwlk = (tcModelData).wlk_.bdist[i__ - 1] +\
                              hwlk * ((tcModelData).wlk_.cdist[i__ - 1] * 2. +\
                                      hwlk * 3. * (tcModelData).wlk_.ddist[i__ - 1]);

                            (tcModelData).wlk_.tlast[i__ - 1] =\
                                                    (tcModelData).wlk_.tnext[i__ - 1];
                            [(tcModelData).wlk_.adist[i__ - 1],
                              (tcModelData).wlk_.bdist[i__ - 1],
                              (tcModelData).wlk_.cdist[i__ - 1],
                              (tcModelData).wlk_.ddist[i__ - 1],
                             (tcModelData).wlk_.tnext[i__ - 1]
                             ] = self.tesub5_(ModelData, swlk, spwlk,
                              (tcModelData).wlk_.adist[i__ - 1],
                              (tcModelData).wlk_.bdist[i__ - 1],
                              (tcModelData).wlk_.cdist[i__ - 1],
                              (tcModelData).wlk_.ddist[i__ - 1],
                              (tcModelData).wlk_.tlast[i__ - 1],
                              (tcModelData).wlk_.tnext[i__ - 1],
                              (tcModelData).wlk_.hspan[i__ - 1],
                              (tcModelData).wlk_.hzero[i__ - 1],
                              (tcModelData).wlk_.sspan[i__ - 1],
                              (tcModelData).wlk_.szero[i__ - 1],
                              (tcModelData).wlk_.spspan[i__ - 1],
                              (tcModelData).wlk_.idvwlk[i__ - 1]);


                        # /*Step-over of 10 through 12*/
                        if(i__ == 9):
                            i__ = i__ + 3;

                        # /* L900: */


                    # /*Recalculation of Disturbance Process Parameters (10 - 12)*/
                    for i__ in range (10,13):
                    # for (i__ = 10; i__ <= 12; ++i__)
                        if (time >= (tcModelData).wlk_.tnext[i__ - 1]):
                            hwlk = (tcModelData).wlk_.tnext[i__ - 1] -\
                                                    (tcModelData).wlk_.tlast[i__ - 1];

                            swlk = (tcModelData).wlk_.adist[i__ - 1] +\
                             hwlk * ((tcModelData).wlk_.bdist[i__ - 1] +\
                                    hwlk * ((tcModelData).wlk_.cdist[i__ - 1] +\
                                           hwlk * (tcModelData).wlk_.ddist[i__ - 1]));

                            spwlk = (tcModelData).wlk_.bdist[i__ - 1] +\
                              hwlk * ((tcModelData).wlk_.cdist[i__ - 1] * 2. +\
                                      hwlk * 3. * (tcModelData).wlk_.ddist[i__ - 1]);

                            (tcModelData).wlk_.tlast[i__ - 1] =\
                                                    (tcModelData).wlk_.tnext[i__ - 1];
                            if (swlk > .1):
                                (tcModelData).wlk_.adist[i__ - 1] = swlk;
                                (tcModelData).wlk_.bdist[i__ - 1] = spwlk;
                                (tcModelData).wlk_.cdist[i__ - 1] =\
                                                           -(swlk * 3. + spwlk * .2) / .01;
                                (tcModelData).wlk_.ddist[i__ - 1] =\
                                                           (swlk * 2. + spwlk * .1) / .001;
                                (tcModelData).wlk_.tnext[i__ - 1] =\
                                                   (tcModelData).wlk_.tlast[i__ - 1] + .1;

                            else:
                                aux = -1;
                                hwlk = (tcModelData).wlk_.hspan[i__ - 1] *\
                                   self.tesub7_(ModelData, aux) +\
                                                        (tcModelData).wlk_.hzero[i__ - 1];
                                (tcModelData).wlk_.adist[i__ - 1] = swlk;
                                (tcModelData).wlk_.bdist[i__ - 1] = spwlk;

                                # /* Computing 2nd power */
                                d__1 = hwlk;
                                (tcModelData).wlk_.cdist[i__ - 1] =\
                                       ((tcModelData).wlk_.idvwlk[i__ - 1] -\
                                                             2*spwlk*d__1) / (d__1 * d__1);
                                (tcModelData).wlk_.ddist[i__ - 1] = spwlk / (d__1 * d__1);
                                (tcModelData).wlk_.tnext[i__ - 1] =\
                                                 (tcModelData).wlk_.tlast[i__ - 1] + hwlk;


                    # /* L910: */


            # /*Initilization of Disturbance Processes Parameters*/
            if (time == 0.):
                for i__ in range (1,21):
                # for (i__ = 1; i__ <= 20; ++i__):
                    (tcModelData).wlk_.adist[i__ - 1] =\
                                            (tcModelData).wlk_.szero[i__ - 1];
                    (tcModelData).wlk_.bdist[i__ - 1] = 0.;
                    (tcModelData).wlk_.cdist[i__ - 1] = 0.;
                    (tcModelData).wlk_.ddist[i__ - 1] = 0.;
                    (tcModelData).wlk_.tlast[i__ - 1] = 0.;
                    (tcModelData).wlk_.tnext[i__ - 1] = .1;
                    # /* L950: */            }

            # /*Determination of Disturbed Values*/
            (tcModelData).teproc_.xst[24] = self.tesub8_(ModelData, self.c__1, time) -\
                               (tcModelData).dvec_.idv[0] * .03 -\
                               (tcModelData).dvec_.idv[1] * .00243719;
            (tcModelData).teproc_.xst[25] = self.tesub8_(ModelData, self.c__2, time) +\
                               (tcModelData).dvec_.idv[1] * .005;
            (tcModelData).teproc_.xst[26] = 1. - (tcModelData).teproc_.xst[24] -\
                               (tcModelData).teproc_.xst[25];
            (tcModelData).teproc_.tst[0]  = self.tesub8_(ModelData, self.c__3, time) +\
                               (tcModelData).dvec_.idv[2] * 5.;
            (tcModelData).teproc_.tst[3]  = self.tesub8_(ModelData, self.c__4, time);
            (tcModelData).teproc_.tcwr    = self.tesub8_(ModelData, self.c__5, time) +\
                               (tcModelData).dvec_.idv[3] * 5.;
            (tcModelData).teproc_.tcws    = self.tesub8_(ModelData, self.c__6, time) +\
                               (tcModelData).dvec_.idv[4] * 5.;
            r1f = self.tesub8_(ModelData, self.c__7, time);
            r2f = self.tesub8_(ModelData, self.c__8, time);
            (tcModelData).teproc_.tst[2]   = self.tesub8_(ModelData, self.c__13, time);
            (tcModelData).teproc_.tst[1]   = self.tesub8_(ModelData, self.c__14, time);
            (tcModelData).teproc_.vrng[2]  = self.tesub8_(ModelData, self.c__15, time);
            (tcModelData).teproc_.vrng[0]  = self.tesub8_(ModelData, self.c__16, time);
            (tcModelData).teproc_.vrng[1]  = self.tesub8_(ModelData, self.c__17, time);
            (tcModelData).teproc_.vrng[3]  = self.tesub8_(ModelData, self.c__18, time);
            (tcModelData).teproc_.vrng[9]  = self.tesub8_(ModelData, self.c__19, time);
            (tcModelData).teproc_.vrng[10] = self.tesub8_(ModelData, self.c__20, time);

            # /*Setting of Disturbance Outputs*/
            (tcModelData).pv_.xmeasdist[0]  = (tcModelData).teproc_.xst[24]*100;
            (tcModelData).pv_.xmeasdist[1]  = (tcModelData).teproc_.xst[25]*100;
            (tcModelData).pv_.xmeasdist[2]  = (tcModelData).teproc_.xst[26]*100;
            (tcModelData).pv_.xmeasdist[3]  = (tcModelData).teproc_.tst[0];
            (tcModelData).pv_.xmeasdist[4]  = (tcModelData).teproc_.tst[3];
            (tcModelData).pv_.xmeasdist[5]  = (tcModelData).teproc_.tcwr;
            (tcModelData).pv_.xmeasdist[6]  = (tcModelData).teproc_.tcws;
            (tcModelData).pv_.xmeasdist[7]  = r1f;
            (tcModelData).pv_.xmeasdist[8]  = r2f;
            (tcModelData).pv_.xmeasdist[9]  = self.tesub8_(ModelData, self.c__9, time);
            (tcModelData).pv_.xmeasdist[10] = self.tesub8_(ModelData, self.c__10, time);
            (tcModelData).pv_.xmeasdist[11] = self.tesub8_(ModelData, self.c__11, time);
            (tcModelData).pv_.xmeasdist[12] = self.tesub8_(ModelData, self.c__12, time);
            (tcModelData).pv_.xmeasdist[13] = (tcModelData).teproc_.tst[2];
            (tcModelData).pv_.xmeasdist[14] = (tcModelData).teproc_.tst[1];
            (tcModelData).pv_.xmeasdist[15] = (tcModelData).teproc_.vrng[2]*\
                                   .454;
            (tcModelData).pv_.xmeasdist[16] = (tcModelData).teproc_.vrng[0]*\
                                   .454;
            (tcModelData).pv_.xmeasdist[17] = (tcModelData).teproc_.vrng[1]*\
                                   .454;
            (tcModelData).pv_.xmeasdist[18] = (tcModelData).teproc_.vrng[3]*\
                                   .454;
            (tcModelData).pv_.xmeasdist[19] = (tcModelData).teproc_.vrng[9]*\
                                   0.003785411784 * 60.;
            (tcModelData).pv_.xmeasdist[20] = (tcModelData).teproc_.vrng[10]*\
                                   0.003785411784 * 60.;

            # /*Retrieving of Current States*/
            for i__ in range (1,4):
            # for (i__ = 1; i__ <= 3; ++i__):
                (tcModelData).teproc_.ucvr[i__ - 1] = yy[-1+i__];
                (tcModelData).teproc_.ucvs[i__ - 1] = yy[-1+i__ + 9];
                (tcModelData).teproc_.uclr[i__ - 1] = 0.;
                (tcModelData).teproc_.ucls[i__ - 1] = 0.;
                # /* L1010: */

            for i__ in range (4, 9):
            # for (i__ = 4; i__ <= 8; ++i__)
                (tcModelData).teproc_.uclr[i__ - 1] = yy[-1+i__];
                (tcModelData).teproc_.ucls[i__ - 1] = yy[-1+i__ + 9];
                # /* L1020: */

            for i__ in range (1,9):
            # for (i__ = 1; i__ <= 8; ++i__){
                (tcModelData).teproc_.uclc[i__ - 1] = yy[-1+i__ + 18];
                (tcModelData).teproc_.ucvv[i__ - 1] = yy[-1+i__ + 27];
            # /* L1030: */

            (tcModelData).teproc_.etr = yy[-1+9];
            (tcModelData).teproc_.ets = yy[-1+18];
            (tcModelData).teproc_.etc = yy[-1+27];
            (tcModelData).teproc_.etv = yy[-1+36];
            (tcModelData).teproc_.twr = yy[-1+37];
            (tcModelData).teproc_.tws = yy[-1+38];
            for i__ in range (1, 13):
            # for (i__ = 1; i__ <= 12; ++i__){
                vpos[i__ - 1] = yy[-1+i__ + 38];
            # /* L1035: */


            # /*Calculation of Collective Holdup*/
            (tcModelData).teproc_.utlr = 0.;
            (tcModelData).teproc_.utls = 0.;
            (tcModelData).teproc_.utlc = 0.;
            (tcModelData).teproc_.utvv = 0.;
            for i__ in range (1, 9):
            # for (i__ = 1; i__ <= 8; ++i__)
                (tcModelData).teproc_.utlr += (tcModelData).teproc_.uclr[i__ - 1];
                (tcModelData).teproc_.utls += (tcModelData).teproc_.ucls[i__ - 1];
                (tcModelData).teproc_.utlc += (tcModelData).teproc_.uclc[i__ - 1];
                (tcModelData).teproc_.utvv += (tcModelData).teproc_.ucvv[i__ - 1];
            # /* L1040: */


            # /*Calculation of Component Concentration*/
            # for (i__ = 1; i__ <= 8; ++i__){
            for i__ in range (1,9):
                (tcModelData).teproc_.xlr[i__ - 1] =\
                (tcModelData).teproc_.uclr[i__ - 1] / (tcModelData).teproc_.utlr;
                (tcModelData).teproc_.xls[i__ - 1] =\
                (tcModelData).teproc_.ucls[i__ - 1] / (tcModelData).teproc_.utls;
                (tcModelData).teproc_.xlc[i__ - 1] =\
                (tcModelData).teproc_.uclc[i__ - 1] / (tcModelData).teproc_.utlc;
                (tcModelData).teproc_.xvv[i__ - 1] =\
                (tcModelData).teproc_.ucvv[i__ - 1] / (tcModelData).teproc_.utvv;
                # /* L1050: */
            # } //for (i__ = 1; i__ <= 8; ++i__){

            # /*Calculation of Specific Internal Energy*/
            (tcModelData).teproc_.esr =\
              (tcModelData).teproc_.etr / (tcModelData).teproc_.utlr;
            (tcModelData).teproc_.ess =\
              (tcModelData).teproc_.ets / (tcModelData).teproc_.utls;
            (tcModelData).teproc_.esc =\
              (tcModelData).teproc_.etc / (tcModelData).teproc_.utlc;
            (tcModelData).teproc_.esv =\
              (tcModelData).teproc_.etv / (tcModelData).teproc_.utvv;

            # /*Calculation of Temperatures*/
            (tcModelData).teproc_.tcr = self.tesub2_(ModelData, (tcModelData).teproc_.xlr,
            (tcModelData).teproc_.tcr, (tcModelData).teproc_.esr,
            self.c__0);
            (tcModelData).teproc_.tkr = (tcModelData).teproc_.tcr + 273.15;

            (tcModelData).teproc_.tcs = self.tesub2_(ModelData, (tcModelData).teproc_.xls,
            (tcModelData).teproc_.tcs, (tcModelData).teproc_.ess,
            self.c__0);
            (tcModelData).teproc_.tks = (tcModelData).teproc_.tcs +\
                             273.15;

            (tcModelData).teproc_.tcc = self.tesub2_(ModelData, (tcModelData).teproc_.xlc,
            (tcModelData).teproc_.tcc, (tcModelData).teproc_.esc,
            self.c__0);

            (tcModelData).teproc_.tcv = self.tesub2_(ModelData, (tcModelData).teproc_.xvv,
            (tcModelData).teproc_.tcv, (tcModelData).teproc_.esv,
            self.c__2);
            (tcModelData).teproc_.tkv = (tcModelData).teproc_.tcv +\
                             273.15;

            # /*Calculation of Densities*/
            (tcModelData).teproc_.dlr = self.tesub4_(ModelData, (tcModelData).teproc_.xlr,
            (tcModelData).teproc_.tcr, (tcModelData).teproc_.dlr);
            (tcModelData).teproc_.dls = self.tesub4_(ModelData, (tcModelData).teproc_.xls,
            (tcModelData).teproc_.tcs, (tcModelData).teproc_.dls);
            (tcModelData).teproc_.dlc = self.tesub4_(ModelData, (tcModelData).teproc_.xlc,
            (tcModelData).teproc_.tcc, (tcModelData).teproc_.dlc);

            # /*Calculation of Volume of Liquid and Vapor Phase*/
            (tcModelData).teproc_.vlr =\
              (tcModelData).teproc_.utlr / (tcModelData).teproc_.dlr;
            (tcModelData).teproc_.vls =\
              (tcModelData).teproc_.utls / (tcModelData).teproc_.dls;
            (tcModelData).teproc_.vlc =\
              (tcModelData).teproc_.utlc / (tcModelData).teproc_.dlc;
            (tcModelData).teproc_.vvr =\
               (tcModelData).teproc_.vtr - (tcModelData).teproc_.vlr;
            (tcModelData).teproc_.vvs =\
               (tcModelData).teproc_.vts - (tcModelData).teproc_.vls;

            # /*Calculation of Pressure*/
            (tcModelData).teproc_.ptr = 0.;
            (tcModelData).teproc_.pts = 0.;

            rg = 998.9;
            for i__ in range (1,4):
            # for (i__ = 1; i__ <= 3; ++i__)
                (tcModelData).teproc_.ppr[i__ - 1] =\
                (tcModelData).teproc_.ucvr[i__ - 1] * rg *\
                (tcModelData).teproc_.tkr / (tcModelData).teproc_.vvr;
                (tcModelData).teproc_.ptr += (tcModelData).teproc_.ppr[i__ - 1];

                (tcModelData).teproc_.pps[i__ - 1] =\
                (tcModelData).teproc_.ucvs[i__ - 1] * rg *\
                (tcModelData).teproc_.tks / (tcModelData).teproc_.vvs;
                (tcModelData).teproc_.pts += (tcModelData).teproc_.pps[i__ - 1];
            # /* L1110: */

            for i__ in range (4,9):
            # for (i__ = 4; i__ <= 8; ++i__)
                vpr = np.exp((tcModelData).const_.avp[i__ - 1] +\
                (tcModelData).const_.bvp[i__ - 1] /\
                                     ((tcModelData).teproc_.tcr +\
                                      (tcModelData).const_.cvp[i__ - 1]));
                (tcModelData).teproc_.ppr[i__ - 1] =\
                                 vpr * (tcModelData).teproc_.xlr[i__ - 1];
                (tcModelData).teproc_.ptr += (tcModelData).teproc_.ppr[i__ - 1];

                vpr = np.exp((tcModelData).const_.avp[i__ - 1] +\
                (tcModelData).const_.bvp[i__ - 1] /
                                     ((tcModelData).teproc_.tcs +\
                                      (tcModelData).const_.cvp[i__ - 1]));
                (tcModelData).teproc_.pps[i__ - 1] =\
                                 vpr * (tcModelData).teproc_.xls[i__ - 1];
                (tcModelData).teproc_.pts += (tcModelData).teproc_.pps[i__ - 1];
                # /* L1120: */


            (tcModelData).teproc_.ptv = (tcModelData).teproc_.utvv * rg *\
              (tcModelData).teproc_.tkv / (tcModelData).teproc_.vtv;

            # /*Calculation of Component Concentration in Vapor Phase (Reactor and
            # Separator)*/
            for i__ in range (1,9):
            # for (i__ = 1; i__ <= 8; ++i__){
                (tcModelData).teproc_.xvr[i__ - 1] =\
                (tcModelData).teproc_.ppr[i__ - 1] / (tcModelData).teproc_.ptr;
                (tcModelData).teproc_.xvs[i__ - 1] =\
                (tcModelData).teproc_.pps[i__ - 1] / (tcModelData).teproc_.pts;
                # /* L1130: */


            # /*Calculation of Collective Holdup of Components in Vapor Phase (Reac-
            # tor and Separator)*/
            (tcModelData).teproc_.utvr = (tcModelData).teproc_.ptr *\
            (tcModelData).teproc_.vvr / rg / (tcModelData).teproc_.tkr;
            (tcModelData).teproc_.utvs = (tcModelData).teproc_.pts *\
            (tcModelData).teproc_.vvs / rg / (tcModelData).teproc_.tks;

            # /*Calculation of Single Holdup of Components in Vapor Phase (Reactor
            # and Separator)*/
            for i__ in range (4,9):
            # for (i__ = 4; i__ <= 8; ++i__)
                (tcModelData).teproc_.ucvr[i__ - 1] =\
                (tcModelData).teproc_.utvr * (tcModelData).teproc_.xvr[i__ - 1];
                (tcModelData).teproc_.ucvs[i__ - 1] =\
                (tcModelData).teproc_.utvs * (tcModelData).teproc_.xvs[i__ - 1];
                # /* L1140: */


            # /*Reaction kinetics*/
            (tcModelData).teproc_.rr[0] = np.exp(31.5859536 -
            20130.85052843482 / (tcModelData).teproc_.tkr) * r1f;
            (tcModelData).teproc_.rr[1] = np.exp(3.00094014 -
            10065.42526421741 / (tcModelData).teproc_.tkr) * r2f;
            (tcModelData).teproc_.rr[2] = np.exp(53.4060443 -
            30196.27579265224 / (tcModelData).teproc_.tkr);
            (tcModelData).teproc_.rr[3] = (tcModelData).teproc_.rr[2] *  .767488334;

            if ((tcModelData).teproc_.ppr[0] > 0. and
                            (tcModelData).teproc_.ppr[2] > 0.):
                r1f = np.power((tcModelData).teproc_.ppr[0], self.c_b73);
                r2f = np.power((tcModelData).teproc_.ppr[2], self.c_b74);

                (tcModelData).teproc_.rr[0] = (tcModelData).teproc_.rr[0] *\
                                 r1f * r2f * (tcModelData).teproc_.ppr[3];
                (tcModelData).teproc_.rr[1] = (tcModelData).teproc_.rr[1] *\
                                 r1f * r2f * (tcModelData).teproc_.ppr[4];

            else:
                (tcModelData).teproc_.rr[0] = 0.;
                (tcModelData).teproc_.rr[1] = 0.;
            (tcModelData).teproc_.rr[2] = (tcModelData).teproc_.rr[2] *\
            (tcModelData).teproc_.ppr[0] * (tcModelData).teproc_.ppr[4];
            (tcModelData).teproc_.rr[3] = (tcModelData).teproc_.rr[3] *\
            (tcModelData).teproc_.ppr[0] * (tcModelData).teproc_.ppr[3];

            for i__ in range (1,5):
            # for (i__ = 1; i__ <= 4; ++i__)
                (tcModelData).teproc_.rr[i__ - 1] *= (tcModelData).teproc_.vvr;
                # /* L1200: */


            # /*Consumption and Creation of Components in Reactor*/
            (tcModelData).teproc_.crxr[0] = -(tcModelData).teproc_.rr[0] -\
            (tcModelData).teproc_.rr[1] - (tcModelData).teproc_.rr[2];
            (tcModelData).teproc_.crxr[2] = -(tcModelData).teproc_.rr[0] -\
            (tcModelData).teproc_.rr[1];
            (tcModelData).teproc_.crxr[3] = -(tcModelData).teproc_.rr[0] -\
            (tcModelData).teproc_.rr[3] * 1.5;
            (tcModelData).teproc_.crxr[4] = -(tcModelData).teproc_.rr[1] -\
            (tcModelData).teproc_.rr[2];
            (tcModelData).teproc_.crxr[5] = (tcModelData).teproc_.rr[2] +\
            (tcModelData).teproc_.rr[3];
            (tcModelData).teproc_.crxr[6] = (tcModelData).teproc_.rr[0];
            (tcModelData).teproc_.crxr[7] = (tcModelData).teproc_.rr[1];
            (tcModelData).teproc_.rh =\
            (tcModelData).teproc_.rr[0] * (tcModelData).teproc_.htr[0] +\
            (tcModelData).teproc_.rr[1] * (tcModelData).teproc_.htr[1];


            (tcModelData).teproc_.xmws[0] = 0.;
            (tcModelData).teproc_.xmws[1] = 0.;
            (tcModelData).teproc_.xmws[5] = 0.;
            (tcModelData).teproc_.xmws[7] = 0.;
            (tcModelData).teproc_.xmws[8] = 0.;
            (tcModelData).teproc_.xmws[9] = 0.;
            for i__ in range (1,9):
            # for (i__ = 1; i__ <= 8; ++i__)
                (tcModelData).teproc_.xst[i__ + 39] =\
                                       (tcModelData).teproc_.xvv[i__ - 1];
                (tcModelData).teproc_.xst[i__ + 55] =\
                                       (tcModelData).teproc_.xvr[i__ - 1];
                (tcModelData).teproc_.xst[i__ + 63] =\
                                       (tcModelData).teproc_.xvs[i__ - 1];
                (tcModelData).teproc_.xst[i__ + 71] =\
                                       (tcModelData).teproc_.xvs[i__ - 1];
                (tcModelData).teproc_.xst[i__ + 79] =\
                                       (tcModelData).teproc_.xls[i__ - 1];
                (tcModelData).teproc_.xst[i__ + 95] =\
                                       (tcModelData).teproc_.xlc[i__ - 1];

                (tcModelData).teproc_.xmws[0] +=\
                                    (tcModelData).teproc_.xst[i__ - 1] *\
                                    (tcModelData).const_.xmw[i__ - 1];
                (tcModelData).teproc_.xmws[1] +=\
                                    (tcModelData).teproc_.xst[i__ + 7] *\
                                    (tcModelData).const_.xmw[i__ - 1];
                (tcModelData).teproc_.xmws[5] +=\
                                    (tcModelData).teproc_.xst[i__ + 39] *\
                                    (tcModelData).const_.xmw[i__ - 1];
                (tcModelData).teproc_.xmws[7] +=\
                                    (tcModelData).teproc_.xst[i__ + 55] *\
                                    (tcModelData).const_.xmw[i__ - 1];
                (tcModelData).teproc_.xmws[8] +=\
                                    (tcModelData).teproc_.xst[i__ + 63] *\
                                    (tcModelData).const_.xmw[i__ - 1];
                (tcModelData).teproc_.xmws[9] +=\
                                    (tcModelData).teproc_.xst[i__ + 71] *\
                                    (tcModelData).const_.xmw[i__ - 1];
                # /* L2010: */

            (tcModelData).teproc_.tst[5] = (tcModelData).teproc_.tcv;
            (tcModelData).teproc_.tst[7] = (tcModelData).teproc_.tcr;
            (tcModelData).teproc_.tst[8] = (tcModelData).teproc_.tcs;
            (tcModelData).teproc_.tst[9] = (tcModelData).teproc_.tcs;
            (tcModelData).teproc_.tst[10] = (tcModelData).teproc_.tcs;
            (tcModelData).teproc_.tst[12] = (tcModelData).teproc_.tcc;
            (tcModelData).teproc_.hst=self.tesub1_(ModelData, (tcModelData).teproc_.xst,
            (tcModelData).teproc_.tst, (tcModelData).teproc_.hst[0], self.c__1);
            (tcModelData).teproc_.hst[1]=self.tesub1_(ModelData, (tcModelData).teproc_.xst,
            (tcModelData).teproc_.tst[1], (tcModelData).teproc_.hst,
            self.c__1, start_index=8);
            (tcModelData).teproc_.hst[2]=self.tesub1_(ModelData, (tcModelData).teproc_.xst,
            (tcModelData).teproc_.tst[2], (tcModelData).teproc_.hst[2],
            self.c__1,start_index=16);
            (tcModelData).teproc_.hst[3]=self.tesub1_(ModelData, (tcModelData).teproc_.xst,
            (tcModelData).teproc_.tst[3], (tcModelData).teproc_.hst[3],
            self.c__1,start_index=24);
            (tcModelData).teproc_.hst[5]=self.tesub1_(ModelData, (tcModelData).teproc_.xst,
            (tcModelData).teproc_.tst[5], (tcModelData).teproc_.hst[5],
            self.c__1,start_index=40);
            (tcModelData).teproc_.hst[7]=self.tesub1_(ModelData, (tcModelData).teproc_.xst,
            (tcModelData).teproc_.tst[7], (tcModelData).teproc_.hst[7],
            self.c__1,start_index=56);
            (tcModelData).teproc_.hst[8]=self.tesub1_(ModelData, (tcModelData).teproc_.xst,
            (tcModelData).teproc_.tst[8], (tcModelData).teproc_.hst[8],
            self.c__1,start_index=64);
            (tcModelData).teproc_.hst[9] = (tcModelData).teproc_.hst[8];
            (tcModelData).teproc_.hst[10] = self.tesub1_(ModelData, (tcModelData).teproc_.xst,
            (tcModelData).teproc_.tst[10], (tcModelData).teproc_.hst[10],
            self.c__0,start_index=80);
            (tcModelData).teproc_.hst[12]=self.tesub1_(ModelData, (tcModelData).teproc_.xst,
            (tcModelData).teproc_.tst[12], (tcModelData).teproc_.hst[12],
            self.c__0,start_index=96);
            (tcModelData).teproc_.ftm[0] = vpos[0] *\
                        (tcModelData).teproc_.vrng[0] / 100.;
            (tcModelData).teproc_.ftm[1] = vpos[1] *\
                        (tcModelData).teproc_.vrng[1] / 100.;
            (tcModelData).teproc_.ftm[2] = vpos[2] *\
                        (1. - (tcModelData).dvec_.idv[5]) *\
                        (tcModelData).teproc_.vrng[2] / 100.;
            (tcModelData).teproc_.ftm[3] = vpos[3] *\
                        (1. - (tcModelData).dvec_.idv[6] * .2) *\
                        (tcModelData).teproc_.vrng[3] / 100. +\
                        1e-10;
            (tcModelData).teproc_.ftm[10] = vpos[6] *\
                        (tcModelData).teproc_.vrng[6] / 100.;
            (tcModelData).teproc_.ftm[12] = vpos[7] *\
                        (tcModelData).teproc_.vrng[7] / 100.;

            uac = vpos[8] * (tcModelData).teproc_.vrng[8] *\
                (self.tesub8_(ModelData, self.c__9, time) + 1.) / 100.;
            (tcModelData).teproc_.fwr = vpos[9] *\
                        (tcModelData).teproc_.vrng[9] / 100.;
            (tcModelData).teproc_.fws = vpos[10] *\
                        (tcModelData).teproc_.vrng[10] / 100.;
            (tcModelData).teproc_.agsp = (vpos[11] + 150.) / 100.;

            dlp = (tcModelData).teproc_.ptv - (tcModelData).teproc_.ptr;
            if (dlp < 0.):
                dlp = 0.;

            flms = np.sqrt(dlp) * 1937.6;
            (tcModelData).teproc_.ftm[5] = flms / (tcModelData).teproc_.xmws[5];

            dlp = (tcModelData).teproc_.ptr - (tcModelData).teproc_.pts;
            if (dlp < 0.):
                dlp = 0.;

            flms = np.sqrt(dlp) * 4574.21 *\
                        (1. - self.tesub8_(ModelData, self.c__12, time) * .25);
            (tcModelData).teproc_.ftm[7] = flms / (tcModelData).teproc_.xmws[7];

            dlp = (tcModelData).teproc_.pts - 760.;
            if (dlp < 0.):
                dlp = 0.;

            flms = vpos[5] * .151169 * np.sqrt(dlp);
            (tcModelData).teproc_.ftm[9] = flms / (tcModelData).teproc_.xmws[9];
            pr = (tcModelData).teproc_.ptv / (tcModelData).teproc_.pts;
            if (pr < 1.):
                pr = 1.;

            if (pr > (tcModelData).teproc_.cpprmx):
                pr = (tcModelData).teproc_.cpprmx;

            flcoef = (tcModelData).teproc_.cpflmx / 1.197;

            # /* Computing 3rd power */
            d__1 = pr;
            flms = (tcModelData).teproc_.cpflmx +\
            flcoef * (1. - d__1 * (d__1 * d__1));
            (tcModelData).teproc_.cpdh =\
            flms * ((tcModelData).teproc_.tcs + 273.15) * 1.8e-6 * 1.9872 *\
            ((tcModelData).teproc_.ptv - (tcModelData).teproc_.pts) /\
            ((tcModelData).teproc_.xmws[8] * (tcModelData).teproc_.pts);
            dlp = (tcModelData).teproc_.ptv - (tcModelData).teproc_.pts;
            if (dlp < 0.):
                dlp = 0.;

            flms -= vpos[4] * 53.349 * np.sqrt(dlp);
            if (flms < .001):
                flms = .001;

            (tcModelData).teproc_.ftm[8] = flms / (tcModelData).teproc_.xmws[8];
            (tcModelData).teproc_.hst[8] +=\
            (tcModelData).teproc_.cpdh / (tcModelData).teproc_.ftm[8];
            for i__ in range (1,9):
            # for (i__ = 1; i__ <= 8; ++i__){
                (tcModelData).teproc_.fcm[i__ - 1] =\
                                    (tcModelData).teproc_.xst[i__ - 1] *\
                                    (tcModelData).teproc_.ftm[0];
                (tcModelData).teproc_.fcm[i__ + 7] =\
                                    (tcModelData).teproc_.xst[i__ + 7] *\
                                    (tcModelData).teproc_.ftm[1];
                (tcModelData).teproc_.fcm[i__ + 15] =\
                                    (tcModelData).teproc_.xst[i__ + 15] *\
                                    (tcModelData).teproc_.ftm[2];
                (tcModelData).teproc_.fcm[i__ + 23] =\
                                    (tcModelData).teproc_.xst[i__ + 23] *\
                                    (tcModelData).teproc_.ftm[3];
                (tcModelData).teproc_.fcm[i__ + 39] =\
                                    (tcModelData).teproc_.xst[i__ + 39] *\
                                    (tcModelData).teproc_.ftm[5];
                (tcModelData).teproc_.fcm[i__ + 55] =\
                                    (tcModelData).teproc_.xst[i__ + 55] *\
                                    (tcModelData).teproc_.ftm[7];
                (tcModelData).teproc_.fcm[i__ + 63] =\
                                    (tcModelData).teproc_.xst[i__ + 63] *\
                                    (tcModelData).teproc_.ftm[8];
                (tcModelData).teproc_.fcm[i__ + 71] =\
                                    (tcModelData).teproc_.xst[i__ + 71] *\
                                    (tcModelData).teproc_.ftm[9];
                (tcModelData).teproc_.fcm[i__ + 79] =\
                                    (tcModelData).teproc_.xst[i__ + 79] *\
                                    (tcModelData).teproc_.ftm[10];
                (tcModelData).teproc_.fcm[i__ + 95] =\
                                    (tcModelData).teproc_.xst[i__ + 95] *\
                                    (tcModelData).teproc_.ftm[12];
            # /* L5020: */


            if ((tcModelData).teproc_.ftm[10] > .1):
                if ((tcModelData).teproc_.tcc > 170.):
                    tmpfac = (tcModelData).teproc_.tcc - 120.262;
                elif ((tcModelData).teproc_.tcc < 5.292):
                    tmpfac = .1;
                else:
                    tmpfac = 363.744 / (177. -
                    (tcModelData).teproc_.tcc) - 2.22579488;


                    vovrl = (tcModelData).teproc_.ftm[3] /\
                    (tcModelData).teproc_.ftm[10] * tmpfac;
                    (tcModelData).teproc_.sfr[3] = vovrl *  8.501  /\
                                         (vovrl * 8.501 + 1.);
                    (tcModelData).teproc_.sfr[4] = vovrl * 11.402  /\
                                         (vovrl * 11.402 + 1.);
                    (tcModelData).teproc_.sfr[5] = vovrl * 11.795  /\
                                         (vovrl * 11.795 + 1.);
                    (tcModelData).teproc_.sfr[6] = vovrl *   .048  /\
                                         (vovrl * .048 + 1.);
                    (tcModelData).teproc_.sfr[7] = vovrl *   .0242 /\
                                         (vovrl * .0242 + 1.);
            else:
                (tcModelData).teproc_.sfr[3] = .9999;
                (tcModelData).teproc_.sfr[4] = .999;
                (tcModelData).teproc_.sfr[5] = .999;
                (tcModelData).teproc_.sfr[6] = .99;
                (tcModelData).teproc_.sfr[7] = .98;

            for i__ in range (1,9):
            # for (i__ = 1; i__ <= 8; ++i__):
                fin[i__ - 1] = 0.;
                fin[i__ - 1] += (tcModelData).teproc_.fcm[i__ + 23];
                fin[i__ - 1] += (tcModelData).teproc_.fcm[i__ + 79];
                # /* L6010: */

            (tcModelData).teproc_.ftm[4] = 0.;
            (tcModelData).teproc_.ftm[11] = 0.;
            # for (i__ = 1; i__ <= 8; ++i__){
            for i__ in range (1,9):
                (tcModelData).teproc_.fcm[i__ + 31] =\
                       (tcModelData).teproc_.sfr[i__ - 1] * fin[i__ - 1];
                (tcModelData).teproc_.fcm[i__ + 87] =\
                       fin[i__ - 1] - (tcModelData).teproc_.fcm[i__ + 31];

                (tcModelData).teproc_.ftm[4]  +=\
                                      (tcModelData).teproc_.fcm[i__ + 31];
                (tcModelData).teproc_.ftm[11] +=\
                                      (tcModelData).teproc_.fcm[i__ + 87];
                # /* L6020: */

            # for (i__ = 1; i__ <= 8; ++i__){
            for i__ in range (1,9):
                (tcModelData).teproc_.xst[i__ + 31] =\
                                   (tcModelData).teproc_.fcm[i__ + 31] /\
                                   (tcModelData).teproc_.ftm[4];
                (tcModelData).teproc_.xst[i__ + 87] =\
                                   (tcModelData).teproc_.fcm[i__ + 87] /\
                                   (tcModelData).teproc_.ftm[11];
            # /* L6030: */

            (tcModelData).teproc_.tst[4] = (tcModelData).teproc_.tcc;
            (tcModelData).teproc_.tst[11] = (tcModelData).teproc_.tcc;
            (tcModelData).teproc_.hst[4]=self.tesub1_(ModelData, (tcModelData).teproc_.xst,
            (tcModelData).teproc_.tst[4], (tcModelData).teproc_.hst[4],
            self.c__1,start_index=32);
            (tcModelData).teproc_.hst[11]=self.tesub1_(ModelData, (tcModelData).teproc_.xst,
            (tcModelData).teproc_.tst[11],
            (tcModelData).teproc_.hst[11],self.c__0,start_index=88);
            (tcModelData).teproc_.ftm[6] = (tcModelData).teproc_.ftm[5];
            (tcModelData).teproc_.hst[6] = (tcModelData).teproc_.hst[5];
            (tcModelData).teproc_.tst[6] = (tcModelData).teproc_.tst[5];
            # for (i__ = 1; i__ <= 8; ++i__)
            for i__ in range (1,9):
                (tcModelData).teproc_.xst[i__ + 47] =\
                                      (tcModelData).teproc_.xst[i__ + 39];
                (tcModelData).teproc_.fcm[i__ + 47] =\
                                      (tcModelData).teproc_.fcm[i__ + 39];
            # /* L6130: */

            # /*Calculation of Heat Transfer in Reactor*/
            if ((tcModelData).teproc_.vlr / 7.8 > 50.):
                uarlev = 1.
            elif ((tcModelData).teproc_.vlr / 7.8 < 10.):
                uarlev = 0.;
            else:
                uarlev = (tcModelData).teproc_.vlr * .025 / 7.8 -\
            .25;


            # /* Computing 2nd power */
            d__1 = (tcModelData).teproc_.agsp;
            (tcModelData).teproc_.uar = uarlev *\
            (d__1 * d__1 * -.5 +
            (tcModelData).teproc_.agsp * 2.75 - 2.5) * .85549;
            (tcModelData).teproc_.qur = (tcModelData).teproc_.uar *\
            ((tcModelData).teproc_.twr - (tcModelData).teproc_.tcr) *\
            (1. - self.tesub8_(ModelData, self.c__10, time) * .35);

            # /*Calculation of Heat Transfer in Condenser (Separator)*/
            # /* Computing 4th power */
            d__1  = (tcModelData).teproc_.ftm[7] / 3528.73;
            d__1 *= d__1;
            uas   = (1. - 1. / (d__1 * d__1 + 1.)) *  .404655;
            (tcModelData).teproc_.qus = uas *\
            ((tcModelData).teproc_.tws - (tcModelData).teproc_.tst[7]) *\
            (1. - self.tesub8_(ModelData, self.c__11, time) * .25);

            # /*Calculation of Heat Transfer in Stripper*/
            (tcModelData).teproc_.quc = 0.;
            if ((tcModelData).teproc_.tcc < 100.):
                (tcModelData).teproc_.quc = uac *\
                                 (100. - (tcModelData).teproc_.tcc);


            (tcModelData).tlastcomp = time;

        else:
            for i__ in range (1,13):
            # for (i__ = 1; i__ <= 12; ++i__):
                vpos[i__ - 1] = yy[-1+i__ + 38];



        # /*Setting of Measured Values*/
        if((Callflag < 2) and (((tcModelData).MSFlag & 0x8000) == 0)):
            (tcModelData).pv_.xmeas[0] = (tcModelData).teproc_.ftm[2] *\
                          .359 / 35.3145;
            (tcModelData).pv_.xmeas[1] = (tcModelData).teproc_.ftm[0] *\
                          (tcModelData).teproc_.xmws[0] * .454;
            (tcModelData).pv_.xmeas[2] = (tcModelData).teproc_.ftm[1] *\
                          (tcModelData).teproc_.xmws[1] * .454;
            (tcModelData).pv_.xmeas[3] = (tcModelData).teproc_.ftm[3] *\
                          .359 / 35.3145;
            (tcModelData).pv_.xmeas[4] = (tcModelData).teproc_.ftm[8] *\
                          .359 / 35.3145;
            (tcModelData).pv_.xmeas[5] = (tcModelData).teproc_.ftm[5] *\
                          .359 / 35.3145;
            (tcModelData).pv_.xmeas[6] = ((tcModelData).teproc_.ptr -\
            760.) / 760. * 101.325;
            (tcModelData).pv_.xmeas[7] = ((tcModelData).teproc_.vlr -\
            84.6) / 666.7 * 100.;
            (tcModelData).pv_.xmeas[8] = (tcModelData).teproc_.tcr;
            (tcModelData).pv_.xmeas[9] = (tcModelData).teproc_.ftm[9] *\
                          .359 / 35.3145;
            (tcModelData).pv_.xmeas[10] = (tcModelData).teproc_.tcs;
            (tcModelData).pv_.xmeas[11] = ((tcModelData).teproc_.vls -\
            27.5) / 290. * 100.;
            (tcModelData).pv_.xmeas[12] = ((tcModelData).teproc_.pts -\
            760.) / 760. * 101.325;
            (tcModelData).pv_.xmeas[13] = (tcModelData).teproc_.ftm[10] /\
                          (tcModelData).teproc_.dls / 35.3145;
            (tcModelData).pv_.xmeas[14] = ((tcModelData).teproc_.vlc -\
            78.25) / (tcModelData).teproc_.vtc * 100.;
            (tcModelData).pv_.xmeas[15] = ((tcModelData).teproc_.ptv -\
            760.) / 760. * 101.325;
            (tcModelData).pv_.xmeas[16] = (tcModelData).teproc_.ftm[12] /\
                          (tcModelData).teproc_.dlc / 35.3145;
            (tcModelData).pv_.xmeas[17] = (tcModelData).teproc_.tcc;
            (tcModelData).pv_.xmeas[18] = (tcModelData).teproc_.quc *\
                          1040. * .454;
            (tcModelData).pv_.xmeas[19] = (tcModelData).teproc_.cpdh * 392.7;
            (tcModelData).pv_.xmeas[19] = (tcModelData).teproc_.cpdh * 293.07;
            (tcModelData).pv_.xmeas[20] = (tcModelData).teproc_.twr;
            (tcModelData).pv_.xmeas[21] = (tcModelData).teproc_.tws;

            # /*Ьbergabe der zusдtzlichen Ausgдnge*/
            if(((tcModelData).MSFlag & 0x1) == 1):
                (tcModelData).pv_.xmeasadd[0] = (tcModelData).teproc_.tst[2];
                (tcModelData).pv_.xmeasadd[1] = (tcModelData).teproc_.tst[0];
                (tcModelData).pv_.xmeasadd[2] = (tcModelData).teproc_.tst[1];
                (tcModelData).pv_.xmeasadd[3] = (tcModelData).teproc_.tst[3];
                (tcModelData).pv_.xmeasadd[4] = (tcModelData).teproc_.tcwr;
                (tcModelData).pv_.xmeasadd[5] = (tcModelData).teproc_.fwr *\
                 0.003785411784 * 60.;
                (tcModelData).pv_.xmeasadd[6] = (tcModelData).teproc_.tcws;
                (tcModelData).pv_.xmeasadd[7] = (tcModelData).teproc_.fws *\
                 0.003785411784 * 60.;


            # /*Checking of Shut-Down-Constraints*/
            isd = 0.;
            if ((tcModelData).pv_.xmeas[6] > 3e3):
                isd = 1.;
                print((tcModelData).msg, "High Reactor Pressure!!  Shutting down.");


            if ((tcModelData).teproc_.vlr / 35.3145 > 24.):
                isd = 2.;
                print((tcModelData).msg, "High Reactor Liquid Level!!  Shutting down.");


            if ((tcModelData).teproc_.vlr / 35.3145 < 2.):
                isd = 3.;
                print((tcModelData).msg,"Low Reactor Liquid Level!!  Shutting down.");

            if ((tcModelData).pv_.xmeas[8] > 175.):
                isd = 4.
                print((tcModelData).msg,"High Reactor Temperature!!  Shutting down.")


            if ((tcModelData).teproc_.vls / 35.3145 > 12.):
                isd = 5.;
                print((tcModelData).msg,
                "High Separator Liquid Level!!  Shutting down.");


            if ((tcModelData).teproc_.vls / 35.3145 < 1.):
                isd = 6.;
                print((tcModelData).msg,
                "Low Separator Liquid Level!!  Shutting down.");

            if ((tcModelData).teproc_.vlc / 35.3145 > 8.):
                isd = 7.;
                print((tcModelData).msg,
                "High Stripper Liquid Level!!  Shutting down.");

            if ((tcModelData).teproc_.vlc / 35.3145 < 1.):
                isd = 8.;
                print((tcModelData).msg,
                "Low Stripper Liquid Level!!  Shutting down.");


            # /*Adding of Measurement Noise*/
            if (time > 0. and isd == 0.):
                for i__ in range (1,23):
                # for (i__ = 1; i__ <= 22; ++i__){
                    xmns = self.tesub6_(ModelData, (tcModelData).teproc_.xns[i__ - 1], xmns);
                    (tcModelData).pv_.xmeas[i__ - 1] += xmns;
                    # /* L6500: */


                if(((tcModelData).MSFlag & 0x1) == 1):
                    for i__ in range (1,9):
                    # for (i__ = 1; i__ <= 8; ++i__):
                        xmns = self.tesub6_(ModelData, (tcModelData).teproc_.xnsadd[i__ - 1],xmns);
                        (tcModelData).pv_.xmeasadd[i__ - 1] += xmns;
                # /* L6500: */



            # /*Analyzer Outputs*/
            xcmp[22] = (tcModelData).teproc_.xst[48] * 100.;
            xcmp[23] = (tcModelData).teproc_.xst[49] * 100.;
            xcmp[24] = (tcModelData).teproc_.xst[50] * 100.;
            xcmp[25] = (tcModelData).teproc_.xst[51] * 100.;
            xcmp[26] = (tcModelData).teproc_.xst[52] * 100.;
            xcmp[27] = (tcModelData).teproc_.xst[53] * 100.;
            xcmp[28] = (tcModelData).teproc_.xst[72] * 100.;
            xcmp[29] = (tcModelData).teproc_.xst[73] * 100.;
            xcmp[30] = (tcModelData).teproc_.xst[74] * 100.;
            xcmp[31] = (tcModelData).teproc_.xst[75] * 100.;
            xcmp[32] = (tcModelData).teproc_.xst[76] * 100.;
            xcmp[33] = (tcModelData).teproc_.xst[77] * 100.;
            xcmp[34] = (tcModelData).teproc_.xst[78] * 100.;
            xcmp[35] = (tcModelData).teproc_.xst[79] * 100.;
            xcmp[36] = (tcModelData).teproc_.xst[99] * 100.;
            xcmp[37] = (tcModelData).teproc_.xst[100] * 100.;
            xcmp[38] = (tcModelData).teproc_.xst[101] * 100.;
            xcmp[39] = (tcModelData).teproc_.xst[102] * 100.;
            xcmp[40] = (tcModelData).teproc_.xst[103] * 100.;

            if(((tcModelData).MSFlag & 0x1) == 1):
                xcmpadd[0]  = (tcModelData).teproc_.xst[16] * 100.;
                xcmpadd[1]  = (tcModelData).teproc_.xst[17] * 100.;
                xcmpadd[2]  = (tcModelData).teproc_.xst[18] * 100.;
                xcmpadd[3]  = (tcModelData).teproc_.xst[19] * 100.;
                xcmpadd[4]  = (tcModelData).teproc_.xst[20] * 100.;
                xcmpadd[5]  = (tcModelData).teproc_.xst[21] * 100.;

                xcmpadd[6]  = (tcModelData).teproc_.xst[0] * 100.;
                xcmpadd[7]  = (tcModelData).teproc_.xst[1] * 100.;
                xcmpadd[8]  = (tcModelData).teproc_.xst[2] * 100.;
                xcmpadd[9]  = (tcModelData).teproc_.xst[3] * 100.;
                xcmpadd[10] = (tcModelData).teproc_.xst[4] * 100.;
                xcmpadd[11] = (tcModelData).teproc_.xst[5] * 100.;

                xcmpadd[12] = (tcModelData).teproc_.xst[8] * 100.;
                xcmpadd[13] = (tcModelData).teproc_.xst[9] * 100.;
                xcmpadd[14] = (tcModelData).teproc_.xst[10] * 100.;
                xcmpadd[15] = (tcModelData).teproc_.xst[11] * 100.;
                xcmpadd[16] = (tcModelData).teproc_.xst[12] * 100.;
                xcmpadd[17] = (tcModelData).teproc_.xst[13] * 100.;

                xcmpadd[18] = (tcModelData).teproc_.xst[24] * 100.;
                xcmpadd[19] = (tcModelData).teproc_.xst[25] * 100.;
                xcmpadd[20] = (tcModelData).teproc_.xst[26] * 100.;
                xcmpadd[21] = (tcModelData).teproc_.xst[27] * 100.;
                xcmpadd[22] = (tcModelData).teproc_.xst[28] * 100.;
                xcmpadd[23] = (tcModelData).teproc_.xst[29] * 100.;


            if (time == 0.):
                for i__ in range (23, 42):
                # for (i__ = 23; i__ <= 41; ++i__)
                    (tcModelData).teproc_.xdel[i__ - 1] = xcmp[i__ - 1];
                    (tcModelData).pv_.xmeas[i__ - 1] = xcmp[i__ - 1];
                # /* L7010: */


                if(((tcModelData).MSFlag & 0x1) == 1):
                    for i__ in range (1,25):
                    # for (i__ = 1; i__ <= 24; ++i__){
                        (tcModelData).teproc_.xdeladd[i__ - 1] = xcmpadd[i__ - 1];
                        (tcModelData).pv_.xmeasadd[i__ + 7] = xcmpadd[i__ - 1];

                (tcModelData).teproc_.tgas = .1;
                (tcModelData).teproc_.tprod = .25;


            if (time >= (tcModelData).teproc_.tgas):
                for i__ in range (23,37):
                # for (i__ = 23; i__ <= 36; ++i__):
                    (tcModelData).pv_.xmeas[i__ - 1] =\
                                          (tcModelData).teproc_.xdel[i__ - 1];
                    xmns = self.tesub6_(ModelData, (tcModelData).teproc_.xns[i__ - 1], xmns);
                    (tcModelData).pv_.xmeas[i__ - 1] += xmns;

                    (tcModelData).teproc_.xdel[i__ - 1] = xcmp[i__ - 1];
                # /* L7020: */


                if(((tcModelData).MSFlag & 0x1) == 1):
                    for i__ in range (1,25):
                    # for (i__ = 1; i__ <= 24; ++i__):
                        (tcModelData).pv_.xmeasadd[i__ + 7] =\
                                           (tcModelData).teproc_.xdeladd[i__ - 1];
                        xmns = self.tesub6_(ModelData, (tcModelData).teproc_.xnsadd[i__ - 1],xmns);
                        (tcModelData).pv_.xmeasadd[i__ + 7] += xmns;

                        (tcModelData).teproc_.xdeladd[i__ - 1] = xcmpadd[i__ - 1];


                (tcModelData).teproc_.tgas += .1;


            if (time >= (tcModelData).teproc_.tprod):
                for i__ in range (37, 42):
                # for (i__ = 37; i__ <= 41; ++i__){
                    (tcModelData).pv_.xmeas[i__     - 1] =\
                                          (tcModelData).teproc_.xdel[i__ - 1];
                    xmns = self.tesub6_(ModelData, (tcModelData).teproc_.xns[i__ - 1], xmns);
                    (tcModelData).pv_.xmeas[i__ - 1] += xmns;

                    (tcModelData).teproc_.xdel[i__ - 1] = xcmp[i__ - 1];
                # /* L7030: */

                (tcModelData).teproc_.tprod += .25;


            # /*Monitoring Outputs*/
            if(((tcModelData).MSFlag & 0x4) > 1):
                (tcModelData).pv_.xmeasmonitor[0]  =\
                             (tcModelData).teproc_.crxr[0] * .454;
                (tcModelData).pv_.xmeasmonitor[1]  =\
                             (tcModelData).teproc_.crxr[2] * .454;
                (tcModelData).pv_.xmeasmonitor[2]  =\
                             (tcModelData).teproc_.crxr[3] * .454;
                (tcModelData).pv_.xmeasmonitor[3]  =\
                             (tcModelData).teproc_.crxr[4] * .454;
                (tcModelData).pv_.xmeasmonitor[4]  =\
                             (tcModelData).teproc_.crxr[5] * .454;
                (tcModelData).pv_.xmeasmonitor[5]  =\
                             (tcModelData).teproc_.crxr[6] * .454;
                (tcModelData).pv_.xmeasmonitor[6]  =\
                             (tcModelData).teproc_.crxr[7] * .454;
                (tcModelData).pv_.xmeasmonitor[7]  = (tcModelData).teproc_.ppr[0] /\
                                           760. * 101.325;
                (tcModelData).pv_.xmeasmonitor[8]  = (tcModelData).teproc_.ppr[1] /\
                                           760. * 101.325;
                (tcModelData).pv_.xmeasmonitor[9]  = (tcModelData).teproc_.ppr[2] /\
                                           760. * 101.325;
                (tcModelData).pv_.xmeasmonitor[10] = (tcModelData).teproc_.ppr[3] /\
                                           760. * 101.325;
                (tcModelData).pv_.xmeasmonitor[11] = (tcModelData).teproc_.ppr[4] /\
                                           760. * 101.325;
                (tcModelData).pv_.xmeasmonitor[12] = (tcModelData).teproc_.ppr[5] /\
                                           760. * 101.325;
                (tcModelData).pv_.xmeasmonitor[13] = (tcModelData).teproc_.ppr[6] /\
                                           760. * 101.325;
                (tcModelData).pv_.xmeasmonitor[14] = (tcModelData).teproc_.ppr[7] /\
                                           760. * 101.325;
                (tcModelData).pv_.xmeasmonitor[15] = (tcModelData).teproc_.xst[48]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[16] = (tcModelData).teproc_.xst[49]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[17] = (tcModelData).teproc_.xst[50]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[18] = (tcModelData).teproc_.xst[51]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[19] = (tcModelData).teproc_.xst[52]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[20] = (tcModelData).teproc_.xst[53]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[21] = (tcModelData).teproc_.xst[72]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[22] = (tcModelData).teproc_.xst[73]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[23] = (tcModelData).teproc_.xst[74]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[24] = (tcModelData).teproc_.xst[75]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[25] = (tcModelData).teproc_.xst[76]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[26] = (tcModelData).teproc_.xst[77]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[27] = (tcModelData).teproc_.xst[78]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[28] = (tcModelData).teproc_.xst[79]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[29] = (tcModelData).teproc_.xst[99]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[30] =(tcModelData).teproc_.xst[100]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[31] =(tcModelData).teproc_.xst[101]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[32] =(tcModelData).teproc_.xst[102]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[33] =(tcModelData).teproc_.xst[103]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[34] = (tcModelData).teproc_.xst[16]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[35] = (tcModelData).teproc_.xst[17]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[36] = (tcModelData).teproc_.xst[18]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[37] = (tcModelData).teproc_.xst[19]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[38] = (tcModelData).teproc_.xst[20]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[39] = (tcModelData).teproc_.xst[21]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[40] = (tcModelData).teproc_.xst[0] *\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[41] = (tcModelData).teproc_.xst[1] *\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[42] = (tcModelData).teproc_.xst[2] *\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[43] = (tcModelData).teproc_.xst[3] *\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[44] = (tcModelData).teproc_.xst[4] *\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[45] = (tcModelData).teproc_.xst[5] *\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[46] = (tcModelData).teproc_.xst[8] *\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[47] = (tcModelData).teproc_.xst[9] *\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[48] = (tcModelData).teproc_.xst[10]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[49] = (tcModelData).teproc_.xst[11]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[50] = (tcModelData).teproc_.xst[12]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[51] = (tcModelData).teproc_.xst[13]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[52] = (tcModelData).teproc_.xst[24]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[53] = (tcModelData).teproc_.xst[25]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[54] = (tcModelData).teproc_.xst[26]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[55] = (tcModelData).teproc_.xst[27]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[56] = (tcModelData).teproc_.xst[28]*\
                                           100.;
                (tcModelData).pv_.xmeasmonitor[57] = (tcModelData).teproc_.xst[29]*\
                                           100.;

                prate = 211.3 * yy[-1+46] / 46.534 + 1.0e-6;
                (tcModelData).pv_.xmeasmonitor[58] =\
                100 * (.0536 * (tcModelData).pv_.xmeas[19] +\
                        .0318 * (tcModelData).pv_.xmeas[18] +\
                        (tcModelData).pv_.xmeas[9] * .44791 *\
                           (2.209 * (tcModelData).pv_.xmeas[28] +\
                            6.177 * (tcModelData).pv_.xmeas[30] +\
                            22.06 * (tcModelData).pv_.xmeas[31] +\
                            14.56 * (tcModelData).pv_.xmeas[32] +\
                            17.89 * (tcModelData).pv_.xmeas[33] +\
                            30.44 * (tcModelData).pv_.xmeas[34] +\
                            22.94 * (tcModelData).pv_.xmeas[35]) +\
                        prate *\
                           (.2206 * (tcModelData).pv_.xmeas[36] +\
                            .1456 * (tcModelData).pv_.xmeas[37] +\
                            .1789 * (tcModelData).pv_.xmeas[38])
                       ) / prate;
                (tcModelData).pv_.xmeasmonitor[59] =\
                100 * (.0536 * (tcModelData).teproc_.cpdh * 293.07 +\
                      .0318 * (tcModelData).teproc_.quc *  1040. *\
                      .454 +\
                      (tcModelData).teproc_.ftm[9] *\
                      .359 / 35.3145 * .44791 *\
                           (2.209 * xcmp[28] +\
                            6.177 * xcmp[30] +\
                            22.06 * xcmp[31] +\
                            14.56 * xcmp[32] +\
                            17.89 * xcmp[33] +\
                            30.44 * xcmp[34] +\
                            22.94 * xcmp[35]) +\
                      prate *\
                           (.2206 * xcmp[36] +\
                            .1456 * xcmp[37] +\
                            .1789 * xcmp[38])
                     ) / prate;
                (tcModelData).pv_.xmeasmonitor[60] =\
                (.0536 * (tcModelData).pv_.xmeas[19] +\
                .0318 * (tcModelData).pv_.xmeas[18] +\
                (tcModelData).pv_.xmeas[9] * .44791 *\
                (2.209 * (tcModelData).pv_.xmeas[28] +\
                6.177 * (tcModelData).pv_.xmeas[30] +\
                22.06 * (tcModelData).pv_.xmeas[31] +\
                14.56 * (tcModelData).pv_.xmeas[32] +\
                17.89 * (tcModelData).pv_.xmeas[33] +\
                30.44 * (tcModelData).pv_.xmeas[34] +\
                22.94 * (tcModelData).pv_.xmeas[35]) +\
                prate *\
                (.2206 * (tcModelData).pv_.xmeas[36] +\
                .1456 * (tcModelData).pv_.xmeas[37] +\
                .1789 * (tcModelData).pv_.xmeas[38]));
                (tcModelData).pv_.xmeasmonitor[61] =\
                (.0536 * (tcModelData).teproc_.cpdh * 293.07 +\
                .0318 * (tcModelData).teproc_.quc *  1040. * .454 +\
                (tcModelData).teproc_.ftm[9] *\
                .359 / 35.3145 * .44791 *\
                (2.209 * xcmp[28] +\
                6.177 * xcmp[30] +\
                22.06 * xcmp[31] +\
                14.56 * xcmp[32] +\
                17.89 * xcmp[33] +\
                30.44 * xcmp[34] +\
                22.94 * xcmp[35]) +\
                prate *\
                (.2206 * xcmp[36] +\
                .1456 * xcmp[37] +\
                .1789 * xcmp[38]));


            # /*Output of Component Concentrations*/
            if(((tcModelData).MSFlag & 0x8) > 1):
                for i__ in range (1,9):
                # for (i__ = 1; i__ <= 8; ++i__){
                    (tcModelData).pv_.xmeascomp[0  + i__ - 1] =\
                                      (tcModelData).teproc_.xst[0  + i__ - 1];
                    (tcModelData).pv_.xmeascomp[8  + i__ - 1] =\
                                      (tcModelData).teproc_.xst[8  + i__ - 1];
                    (tcModelData).pv_.xmeascomp[16 + i__ - 1] =\
                                      (tcModelData).teproc_.xst[16 + i__ - 1];
                    (tcModelData).pv_.xmeascomp[24 + i__ - 1] =\
                                      (tcModelData).teproc_.xst[24 + i__ - 1];
                    (tcModelData).pv_.xmeascomp[32 + i__ - 1] =\
                                      (tcModelData).teproc_.xst[32 + i__ - 1];
                    (tcModelData).pv_.xmeascomp[40 + i__ - 1] =\
                                      (tcModelData).teproc_.xst[40 + i__ - 1];
                    (tcModelData).pv_.xmeascomp[48 + i__ - 1] =\
                                      (tcModelData).teproc_.xst[56 + i__ - 1];
                    (tcModelData).pv_.xmeascomp[56 + i__ - 1] =\
                                      (tcModelData).teproc_.xst[64 + i__ - 1];
                    (tcModelData).pv_.xmeascomp[64 + i__ - 1] =\
                                      (tcModelData).teproc_.xst[72 + i__ - 1];
                    (tcModelData).pv_.xmeascomp[72 + i__ - 1] =\
                                      (tcModelData).teproc_.xst[80 + i__ - 1];
                    (tcModelData).pv_.xmeascomp[80 + i__ - 1] =\
                                      (tcModelData).teproc_.xst[88 + i__ - 1];
                    (tcModelData).pv_.xmeascomp[88 + i__ - 1] =\
                                      (tcModelData).teproc_.xst[96 + i__ - 1];



        # /*Calculation of State Derivative*/
        if(((Callflag == 2) or (Callflag == 0)) and
        (((tcModelData).MSFlag & 0x8000) == 0)):
            for i__ in range (1,9):
            # for (i__ = 1; i__ <= 8; ++i__)
                yp[-1+i__]      = (tcModelData).teproc_.fcm[i__ + 47] -\
                     (tcModelData).teproc_.fcm[i__ + 55] +\
                     (tcModelData).teproc_.crxr[i__ - 1];
                yp[-1+i__ + 9]  = (tcModelData).teproc_.fcm[i__ + 55] -\
                     (tcModelData).teproc_.fcm[i__ + 63] -\
                     (tcModelData).teproc_.fcm[i__ + 71] -\
                     (tcModelData).teproc_.fcm[i__ + 79];
                yp[-1+i__ + 18] = (tcModelData).teproc_.fcm[i__ + 87] -\
                     (tcModelData).teproc_.fcm[i__ + 95];
                yp[-1+i__ + 27] = (tcModelData).teproc_.fcm[i__ - 1]  +\
                     (tcModelData).teproc_.fcm[i__ + 7]  +\
                     (tcModelData).teproc_.fcm[i__ + 15] +\
                     (tcModelData).teproc_.fcm[i__ + 31] +\
                     (tcModelData).teproc_.fcm[i__ + 63] -\
                     (tcModelData).teproc_.fcm[i__ + 39];
                # /* L9010: */


            yp[-1+9] = (tcModelData).teproc_.hst[6] * (tcModelData).teproc_.ftm[6] -\
            (tcModelData).teproc_.hst[7] * (tcModelData).teproc_.ftm[7] +\
            (tcModelData).teproc_.rh + (tcModelData).teproc_.qur;
            # /* 		Here is the "correct" version of the separator energy balance: */
            # /* 	YP(18)=HST(8)*FTM(8)- */
            # /*    .(HST(9)*FTM(9)-cpdh)- */
            # /*    .HST(10)*FTM(10)- */
            # /*    .HST(11)*FTM(11)+ */
            # /*    .QUS */
            # /* 		Here is the original version */
            yp[-1+18] =\
            (tcModelData).teproc_.hst[7]  * (tcModelData).teproc_.ftm[7] -\
            (tcModelData).teproc_.hst[8]  * (tcModelData).teproc_.ftm[8] -\
            (tcModelData).teproc_.hst[9]  * (tcModelData).teproc_.ftm[9] -\
            (tcModelData).teproc_.hst[10] * (tcModelData).teproc_.ftm[10]+\
            (tcModelData).teproc_.qus;
            yp[-1+27] =\
            (tcModelData).teproc_.hst[3]  * (tcModelData).teproc_.ftm[3] +\
            (tcModelData).teproc_.hst[10] * (tcModelData).teproc_.ftm[10]-\
            (tcModelData).teproc_.hst[4]  * (tcModelData).teproc_.ftm[4] -\
            (tcModelData).teproc_.hst[12] * (tcModelData).teproc_.ftm[12]+\
            (tcModelData).teproc_.quc;
            yp[-1+36] =\
            (tcModelData).teproc_.hst[0] * (tcModelData).teproc_.ftm[0] +\
            (tcModelData).teproc_.hst[1] * (tcModelData).teproc_.ftm[1] +\
            (tcModelData).teproc_.hst[2] * (tcModelData).teproc_.ftm[2] +\
            (tcModelData).teproc_.hst[4] * (tcModelData).teproc_.ftm[4] +\
            (tcModelData).teproc_.hst[8] * (tcModelData).teproc_.ftm[8] -\
            (tcModelData).teproc_.hst[5] * (tcModelData).teproc_.ftm[5];

            yp[-1+37] = ((tcModelData).teproc_.fwr * 500.53 *\
            ((tcModelData).teproc_.tcwr - (tcModelData).teproc_.twr) -\
            (tcModelData).teproc_.qur * 1e6 / 1.8) /\
            (tcModelData).teproc_.hwr;
            yp[-1+38] = ((tcModelData).teproc_.fws * 500.53 *\
            ((tcModelData).teproc_.tcws - (tcModelData).teproc_.tws) -\
            (tcModelData).teproc_.qus * 1e6 / 1.8) /\
            (tcModelData).teproc_.hws;

            (tcModelData).teproc_.ivst[9]  = (tcModelData).dvec_.idv[13];
            (tcModelData).teproc_.ivst[10] = (tcModelData).dvec_.idv[14];
            (tcModelData).teproc_.ivst[4]  = (tcModelData).dvec_.idv[18];
            (tcModelData).teproc_.ivst[6]  = (tcModelData).dvec_.idv[18];
            (tcModelData).teproc_.ivst[7]  = (tcModelData).dvec_.idv[18];
            (tcModelData).teproc_.ivst[8]  = (tcModelData).dvec_.idv[18];
            for i__ in range (1,13):
            # for (i__ = 1; i__ <= 12; ++i__)
                d__1 = (tcModelData).teproc_.vcv[i__ - 1] -\
                  (tcModelData).pv_.xmv[i__ - 1]
                if ((time == 0.) or
                (d__1,abs(d__1)) > (tcModelData).teproc_.vst[i__ - 1] *\
                        (tcModelData).teproc_.ivst[i__ - 1]):
                    (tcModelData).teproc_.vcv[i__ - 1] =\
                                               (tcModelData).pv_.xmv[i__ - 1];


                # /*Constraints of Manipulated Variable*/
                if ((tcModelData).teproc_.vcv[i__ - 1] < 0.):
                    (tcModelData).teproc_.vcv[i__ - 1] = 0.;

                if ((tcModelData).teproc_.vcv[i__ - 1] > 100.):
                    (tcModelData).teproc_.vcv[i__ - 1] = 100.;


                yp[-1+i__ + 38] =\
                   ((tcModelData).teproc_.vcv[i__ - 1] - vpos[i__ - 1]) /\
                   (tcModelData).teproc_.vtau[i__ - 1];
                # /* L9020: */


            if (time > 0. and isd != 0.):
                i__1 = nn;
                for i__ in range (1, i__1+1):
                # for (i__ = 1; i__ <= i__1; ++i__)
                    yp[-1+i__] = 0.;
                # /* L9030: */


        return yp

        #undef isd

        # return 0;
        # } /* tefunc_ */



    def tesub1_(self, ModelData, z__, t, h__,ity, start_index=0):
        # /*----------------------------- Variables -----------------------------*/
        d__1 = 0
        # struct stModelData *tcModelData;
        i__ = 0
        r__ = 0
        hi = 0

        # /*Typcast of Dataset Pointer*/
        tcModelData = ModelData;

        # /*Parameter adjustments*/
        # --z__;

        # /*--------------------------- Function Body ---------------------------*/
        if (ity == 0):
            h__ = 0.;
            for i__ in range (1,9):
            # for (i__ = 1; i__ <= 8; ++i__){
                  # /* Computing 2nd power */
                  d__1 = t;
                  # /*Integration von Enthalpie-Gleichung (temperaturabhдngigkeit) in tesub3_*/
                  hi = t * ((tcModelData).const_.ah[i__ - 1] +
                             (tcModelData).const_.bh[i__ - 1] * t / 2. +
                             (tcModelData).const_.ch[i__ - 1] * (d__1 * d__1) / 3.);
                  hi *= 1.8;
                  h__ += z__[i__-1 + start_index] * (tcModelData).const_.xmw[i__ - 1] * hi;
                # /* L100: */
        else:
            h__ = 0.;
            for i__ in range (1,9):
            # for (i__ = 1; i__ <= 8; ++i__){
                # /* Computing 2nd power */
                d__1 = t;
                hi = t * ((tcModelData).const_.ag[i__ - 1] +
                     (tcModelData).const_.bg[i__ - 1] *t / 2. +
                     (tcModelData).const_.cg[i__ - 1] * (d__1 * d__1) / 3.);
                hi *= 1.8;
                hi += (tcModelData).const_.av[i__ - 1];
                h__ += z__[i__-1+start_index] * (tcModelData).const_.xmw[i__ - 1] * hi;
            # /* L200: */

        if (ity == 2):
            r__ = 3.57696e-6;
            h__ -= r__ * (t + 273.15);

        return h__

    def tesub2_(self,ModelData, z__, t, h__, ity):
  # /*----------------------------- Variables -----------------------------*/
        j = 0;
        htest=0;
        dh=0
        dt=0
        err=0
        tin=0

        # /*Parameter adjustments*/
        # --z__;

        # /*--------------------------- Function Body ---------------------------*/
        tin = t;
        for j in range (1,101):
        # for (j = 1; j <= 100; ++j)
            htest = self.tesub1_(ModelData, z__, t, htest, ity);
            err = htest - h__;
            dh = self.tesub3_(ModelData, z__, t, dh, ity);
            dt = -err / dh;
            t += dt;
            # /* L250: */
            if (abs(dt) < 1e-12):
                return t;

        t = tin;

        return t

    def tesub3_(self,ModelData, z__, t, dh, ity):
  # /*----------------------------- Variables -----------------------------*/
  #       doublereal d__1;
        # struct stModelData *tcModelData;
        # integer i__;
        # doublereal r__, dhi;

        # /*Typcast of Dataset Pointer*/
        tcModelData = ModelData;

        # /*Parameter adjustments*/
        # --z__;

        # /*--------------------------- Function Body ---------------------------*/
        if (ity == 0):
            dh = 0.;
            for i__ in range (1,9):
            # for (i__ = 1; i__ <= 8; ++i__):
            #     /* Computing 2nd power */
                  d__1 = t;
                  dhi = (tcModelData).const_.ah[i__ - 1] +(tcModelData).const_.bh[i__ - 1] * t + (tcModelData).const_.ch[i__ - 1] * (d__1 * d__1)
                  dhi *= 1.8
                  dh += z__[i__-1] * (tcModelData).const_.xmw[i__ - 1] * dhi;
            # /* L100: */

        else:
            dh = 0.;
            for i__ in range(1, 9):
            # for (i__ = 1; i__ <= 8; ++i__){
            # /* Computing 2nd power */
                d__1 = t;
                dhi = (tcModelData).const_.ag[i__ - 1] + (tcModelData).const_.bg[i__ - 1] * t + (tcModelData).const_.cg[i__ - 1] * (d__1 * d__1);
                dhi *= 1.8;
                dh += z__[i__-1] * (tcModelData).const_.xmw[i__ - 1] * dhi;
                # /* L200: */


        if (ity == 2):
            r__ = 3.57696e-6;
            dh -= r__;

        return dh

    def tesub4_(self,ModelData, x, t, r__):
  # /*----------------------------- Variables -----------------------------*/
  #       struct stModelData *tcModelData;
  #       integer i__;
  #       doublereal v;
  #
  #       /*Typcast of Dataset Pointer*/
        tcModelData = ModelData

        # /*Parameter adjustments*/
        # --x;

        # /*--------------------------- Function Body ---------------------------*/
        v = 0.;

        for i__ in range(1, 9):
        # for (i__ = 1; i__ <= 8; ++i__)
            v += x[i__-1] * (tcModelData).const_.xmw[i__ - 1] /((tcModelData).const_.ad[i__ - 1] +
                  ((tcModelData).const_.bd[i__ - 1] +
                   (tcModelData).const_.cd[i__ - 1] *t) *t);
            # /* L10: */

        r__ = 1. / v;

        return r__

    def tesub5_(self,ModelData, s, sp, adist, bdist, cdist, ddist, tlast, tnext, hspan, hzero, sspan, szero,
			       spspan, idvflag):
  # /*----------------------------- Variables -----------------------------*/
  #       doublereal h__;
  #       integer i__;
  #       doublereal s1;
  #       doublereal s1p;
  #       doublereal d__1;
  #
  #       /*--------------------------- Function Body ---------------------------*/
        i__    = -1;
        some_val = self.tesub7_(ModelData, i__)
        h__    = hspan * some_val + hzero;
        s1     = sspan * some_val * idvflag + szero;
        s1p    = spspan * some_val * idvflag;

        # /* Computing 0th power */
        adist = s;
        # /* Computing 1rd power */
        bdist = sp;
        # /* Computing 2nd power */
        d__1   = h__;
        cdist = ((s1 - s) * 3. - h__ * (s1p + sp * 2.)) / (d__1 * d__1);
        # /* Computing 3rd power */
        d__1   = h__;
        ddist = ((s - s1) * 2. + h__ * (s1p + sp)) / (d__1 * (d__1 * d__1));

        tnext = tlast + h__;

        return [adist, bdist, cdist, ddist, tnext]


    def tesub6_(self,ModelData, std, x):
  # /*----------------------------- Variables -----------------------------*/
  # integer i__;
  #
  # /*--------------------------- Function Body ---------------------------*/
        x = 0.;
        for i__ in range(1,13):
        # for (i__ = 1; i__ <= 12; ++i__){
            x += self.tesub7_(ModelData, i__);
        # }

        x = (x - 6.) * std;

        return x

    def tesub7_(self, ModelData, i__):
        # /*----------------------------- Variables -----------------------------*/
        # struct stModelData *tcModelData;
        # doublereal ret_val, *d__1, c_b78;
        #
        # /*Typcast of Dataset Pointer*/
        tcModelData = ModelData

        def d_mod(a, b):
            return a - (np.int(a/b)*b)

        # /*--------------------------- Function Body ---------------------------*/
        c_b78 = 4294967296.;

        # /*Generation of Random Numbers for Measurment Noise*/
        if (i__ >= 0):
            if(((tcModelData).MSFlag & 0x20) > 0):
                d__1 = (tcModelData).randsd_.measnoise;
            else:
                d__1 = (tcModelData).randsd_.g;

            d__1 = d__1 * 9228907.;
            d__1 = d_mod(d__1, c_b78);

            ret_val = d__1 / 4294967296.;

        # /*Generation of Random Numbers for Process Disturbances*/
        if (i__ < 0):
            if(((tcModelData).MSFlag & 0x20) > 0):
                d__1 = (tcModelData).randsd_.procdist;
            else:
                d__1 = (tcModelData).randsd_.g;


            d__1 = d__1 * 9228907.;
            d__1 = d_mod(d__1, c_b78);

            ret_val = d__1 * 2.0 / 4294967296. - 1.;

        return ret_val;

    def tesub8_(self, ModelData, i__, t):
        # /*----------------------------- Variables -----------------------------*/
        # doublereal ret_val;
        # struct stModelData *tcModelData;
        # doublereal h__;
        #
        # /*Typcast of Dataset Pointer*/
        tcModelData = ModelData;

        # /*--------------------------- Function Body ---------------------------*/
        h__ = t - (tcModelData).wlk_.tlast[i__ - 1];
        ret_val = (tcModelData).wlk_.adist[i__ - 1] +  h__ * ((tcModelData).wlk_.bdist[i__ - 1] + h__ * ((tcModelData).wlk_.cdist[i__ - 1] + h__ * (tcModelData).wlk_.ddist[i__ - 1]));

        return ret_val