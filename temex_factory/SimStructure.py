import numpy as np

SFCNPARAM_NOT_TUNABLE = (1 << 0x0)

class _ssSFcnParams:
    dlgNum = 0
    dlgParams = []
    dlgAttribs = []
    numRtp  = 0
    placeholder = []

class _ssSizes:
	numContStates=0
	numDiscStates=0
	numOutputPorts=0
	numY=0
	numInputPorts=0   #/* number of input ports for S-functions          */
	numU=0            #/* Length of the external input vector for models
					 #         i.e. the sum of the widths of the inports.
					 #         For level 1 S-functions, this is the input
					 #         port width.                                    */
	mexApiInt1=0        #  /* reserved for use by Simulink mex api         */

	sysDirFeedThrough=0 # /* Not used by s-functions - only for root models */


	numSampleTimes=0    #/* # of different sample times and/or time offsets*/
	numSFcnParams=0     #/* number of external matrices passed in          */

					 #      /* -------- Work vectors ------------------------ */
	numIWork=0         #/* size of integer work vector                    */
	numRWork=0          #/* size of real_T precision work vector           */
	numPWork=0          #/* size of pointer work vector                    */

					  #     /* -------- Block counts ------------------------ */
	numBlocks=0         #/* number of Blocks in the model                  */
	numSFunctions=0     #/* number of S-Functions                          */

					  #     /* -------- Model bookkeeping ------------------- */
	numBlockIO=0        #/* number of block outputs                        */
	numBlockParams=0    #/* number of block parameters                     */
	checksums=np.zeros(4)   #/* Checksums  of model                            */

				   #        /* -------- Version ----------------------------- */
	simStructVer=0    #/* SimStruct version                              */

				   #        /* -------- Zero Crossings ---------------------- */
	numNonsampledZCs=0#  /* number of non-sampled zero crossings            */
	numZCEvents=0     #  /* number of zero crossing events                 */

				   #        /* -------- Modes ------------------------------- */
	numModes=0        #  /* number of modes                                */

				   #        /* -------- Configuration options --------------- */
	options=0        #/* General options                                */
				  #         /* -------- Vector Sizes In Bytes  -------------- */

	sizeofY=0        #   /* Sizeof of external input, Y, in bytes          */
	sizeofU=0        #   /* Sizeof of external input, U, in bytes          */
	sizeofBlockIO=0  #   /* size of block outputs (number of bytes)        */

	sizeofGlobalBlockIO=0# /* size of the global block outputs in bytes*/

	numDWork=0          #/* size of data type work vectors                 */
	sizeofDWork=0       #/* Size of data type work vector. Depends on
					 #         dwork data types, complex signals, and
					 #         num dworks.                                    */
	RTWGeneratedSFcn=0  #/* Flag which is set for rtw generated s-function */
					 #      /* Remove once all dstates are changed to dworks  */
					 #      /* ------------- Reserved ------------------------*/

class _ssPortInputs:
    width = 0
    directFeedThrough = 0
    dataTypeId = []
    complexSignal = []
    vect = []
    ptrs = []
    connected = 0
    sampleTime = 0
    offsetTime = 0 #          /* block specifies port based ts */
    dims=0 #               /* port dimensions               */
    bufferDstPort = 0

    sampleTimeIndex = 0 #     /* Sample time index when using
                               #            port based sample times       */
    numDims=0 #            /* port number of dimensions     */

class _ssPortOutputs:
    width=0 #               /* Number of elements in output
                  #                         port                          */
    dataTypeId=0 #          /* Data type of outputs          */
    complexSignal=0 #       /* Complex signal (-1=either,
                    #                       0=no, or 1=yes)?              */
    # void           *signalVect;         /* Output signal                 */
    connected=0 #           /* Are the signals leaving the
                     #                      Sfcn driving other blocks?    */
    # struct {
    #     unsigned int optimOpts       :  2;
    #     unsigned int frameData       :  2;
    #     unsigned int cToMergeBlk     :  1;
    #     unsigned int constOutputExprInRTW   :  1;
    #     unsigned int outputExprInRTW :  1;
    #     unsigned int trivialOutputExprInRTW :  1;
    #     unsigned int okToMerge       :  2;
    #     unsigned int cecId           :  2;
    #     unsigned int nonContPort     :  1;
    #     unsigned int dimensionsMode  :  2;
    #     unsigned int fedByBlockWithModesNoZCs : 1;
    #     unsigned int busMode         :  2;
    #     unsigned int optimizeInIR    :  1;
    #     unsigned int reserved13      : 13;
    # } attributes;

    sampleTime=0 #          /* Sample and offset time when   */
    offsetTime=0 #         /* block specifies port based ts */
    dims=0       #        /* port dimensions               */
    sampleTimeIndex=0 #    /* Sample time index when using
                     #                      port based sample times       */
    icPrmIdxPlus1=0 #       /* block IC parameter (if
                #                         * there is) mapped to this
               #                          * port; 0 for none
               #                          */
    numDims=0 #             /* port number of dimensions     */

class _ssPortInfo:
    inputs = []
    outputs = []

    def __init__(self):
        self.inputs = _ssPortInputs
        self.outputs = _ssPortOutputs
    
class SimStructure:
    sfcnParams = []
    sizes = []
    portInfo = []
    def __init__(self):
        self.sfcnParams = _ssSFcnParams()
        self.sizes = _ssSizes()
        self.portInfo = _ssPortInfo()
    def ssGetSFcnParamsCount(self):
        return self.sfcnParams.dlgNum
    def ssGetSFcnParam(self, index):
        return self.sfcnParams.dlgParams[index]
    def ssSetNumSFcnParams(self, nSFcnParams):
        self.sizes.numSFcnParams = nSFcnParams
    def ssSetNumContStates(self, nContStates):
        self.sizes.numContStates = nContStates
    def ssSetNumDiscStates(self, nDiscStates):
        self.sizes.numDiscStates = nDiscStates
    def ssSetInputPortWidth(self, port, val):
        self.portInfo.inputs[port].width = val
    def ssSetInputPortDirectFeedThrough(self, port,dirFeed):
        self.portInfo.inputs[port].directFeedThrough = (dirFeed)
    def ssSetOutputPortWidth(self, port, val):
        self.portInfo.outputs[port].width = val
    def ssSetNumSampleTimes(self,nSampleTimes):
        self.sizes.numSampleTimes=nSampleTimes
    def ssSetNumRWork(self, nRWork):
        self.sizes.numRWork = nRWork
    def ssSetNumIWork(self, nIWork):
        self.sizes.numIWork = nIWork
    def ssSetNumPWork(self, nPWork):
        self.sizes.numPWork = nPWork
    def ssSetNumModes(self, n):
        self.sizes.numModes = n
    def ssSetNumNonsampledZCs(self, nNonsampledZCs):
        self.sizes.numNonsampledZCs = nNonsampledZCs
    def ssSetOptions(self, opts):
        self.sizes.options = opts
    def ssSetSFcnParamNotTunable(self, index):
        self.sfcnParams.dlgAttribs[index] = (self.sfcnParams.dlgAttribs[index] | SFCNPARAM_NOT_TUNABLE)