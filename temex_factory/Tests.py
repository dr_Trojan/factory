import unittest
import temex
import teprob_mod
import temex_structures
import numpy as np

# class temex_tests(unittest.TestCase):

#     def test_tesub1_equal_ity2(self):
#         inputs = [[1,2,3,4,5,6,7,8], 1, 1, 2]
#         temex.teinit(0, 0, np.zeros(50), np.zeros(50))
#         res = temex.tesub1_(inputs[0], inputs[1], inputs[2], inputs[3])
#         res = np.round(res, 6)
#         wanted = 0.318667
#         self.assertEquals(res, wanted)
#
#     def test_tesub1_equal_ity0(self):
#         inputs = [[1,2,3,4,5,6,7,8], 1, 1, 0]
#         temex.teinit(0, 0, np.zeros(50), np.zeros(50))
#         res = temex.tesub1_(inputs[0], inputs[1], inputs[2], inputs[3])
#         res = np.round(res, 7)
#         wanted = 0.0019632
#         self.assertEquals(res, wanted)
#
#     def test_tesub2_equal(self):
#         z = [0.3457, 1.435345, -10563.2654, 1000.457456, -2.654, 0, 34.4562, 0.25235]
#         t = -0.43252
#         h = 0
#         ity = 2
#         temex.teinit(0, 0, np.zeros(50), np.zeros(50))
#         res = temex.tesub2_(z, t, h, ity)
#         res = np.round(res, 4)
#         wanted = 26.5472
#         self.assertEquals(res, wanted)
#
#     def test_tesub3_equal_ity2(self):
#         z = [3.3457, 1.435345, -10563.2654, 1000.457456, -2.654, 0, 34.4562, 0.25235]
#         t = -0.43252
#         dh = 0
#         ity = 2
#         temex.teinit(0, 0, np.zeros(50), np.zeros(50))
#         res = temex.tesub3_(z, t, dh, ity)
#         res = np.round(res, 6)
#         wanted = -0.111435
#         self.assertEquals(res, wanted)
#
#     def test_tesub3_equal_ity0(self):
#         z = [3.3457, 1.435345, -10563.2654, 1000.457456, -2.654, 0, 34.4562, 0.25235]
#         t = -0.43252
#         dh = 0
#         ity = 0
#         temex.teinit(0, 0, np.zeros(50), np.zeros(50))
#         res = temex.tesub3_(z, t, dh, ity)
#         res = np.round(res, 6)
#         wanted = -0.475336
#         self.assertEquals(res, wanted)
#
#     def test_tesub4_equal(self):
#         x = [3.3457, 1.435345, 7.34634, 1000.457456, -2.654, 0, 34.4562, 0.25235]
#         r = 4.578
#         t = 5.103
#         res = temex.tesub4_(x, t, r)
#         res = np.round(res, 9)
#         wanted = 0.000593751
#         self.assertEquals(res, wanted)
#
#     def test_tesub5_equal(self):
#         s = 2
#         sp = 2
#         adist = 2
#         bdist = 2
#         cdist = 2
#         ddist = 2
#         tlast = 2
#         tnext = 2
#         hspan = 2
#         hzero = 2
#         sspan = 2
#         szero = 2
#         spspan = 2
#         idvflag = 4
#
#         temex.teinit(0, 0, np.zeros(50), np.zeros(50))
#         res = temex.tesub5_(s, sp, adist, bdist,
# 			cdist, ddist, tlast,
# 			tnext, hspan, hzero,
# 			sspan, szero, spspan,
# 			idvflag)
#         res = np.array(res)
#         res = np.round(res, 5)
#
#         wanted = np.array([-17.62247, 9.14028, 3.33047])
#         self.assertTrue((res == wanted).all())
#
#     def test_tesub6_equal(self):
#         std = 5
#         x = 4
#         temex.teinit(0, 0, np.zeros(50), np.zeros(50))
#         res = temex.tesub6_(std, x)
#         res = np.round(res, 5)
#         wanted = -6.51298
#         self.assertEquals(res, wanted)
#
#     def test_tesub7_equal_positive_t(self):
#         i = 1
#         temex.teinit(0, 0, np.zeros(50), np.zeros(50))
#         res = temex.tesub7_(i)
#         res = np.round(res, 6)
#         wanted = 0.332617
#         self.assertEquals(res, wanted)
#
#     def test_tesub7_equal_negative_t(self):
#         i = -1
#         temex.teinit(0, 0, np.zeros(50), np.zeros(50))
#         res = temex.tesub7_(i)
#         res = np.round(res, 6)
#         wanted = -0.334766
#         self.assertEquals(res, wanted)
#
#     def test_tesub8_equal(self):
#         i = 1
#         t = 1
#         temex.teinit(0, 0, np.zeros(50), np.zeros(50))
#         res = temex.tesub8_(i, t)
#         wanted = 0.485
#         self.assertEquals(res, wanted)
#
    # def test_teinit_tefunc_equal(self):
    #     nn = 50
    #     time = 12
    #     y = np.zeros(50)
    #     yp = np.zeros(50)
    #     tmx = temex.temexd()
    #     # y = tmx.teinit(nn, time, y, yp)
    #     # res = tmx.tefunc(nn, time, y, yp)
    #     res = tmx.teinit(nn, time, y, yp)
    #     res = np.round(res, 10)
    #
    #     wanted = np.zeros(50)
    #     wanted[0] = -0.000273651
    #     wanted[1] = 0.000438352
    #     wanted[2] = -0.000555662
    #     wanted[3] = -0.000817983
    #     wanted[4] = -4.80167e-005
    #     wanted[5] = 5.42747e-005
    #     wanted[6] = 0.00129005
    #     wanted[7] = 0.00103054
    #     wanted[8] = 0.000108241
    #     wanted[9] = -0.00254426
    #     wanted[10] = -0.00106295
    #     wanted[11] = -0.001808
    #     wanted[12] = -9.65783e-005
    #     wanted[13] = -0.00149476
    #     wanted[14] = -0.000180797
    #     wanted[15] = -0.000663694
    #     wanted[16] = -0.000594885
    #     wanted[17] = -2.71121e-005
    #     wanted[18] = -2.18404e-006
    #     wanted[19] = 1.54708e-008
    #     wanted[20] = -4.34821e-006
    #     wanted[21] = -1.54836e-008
    #     wanted[22] = -5.46291e-007
    #     wanted[23] = -6.02774e-008
    #     wanted[24] = -2.41894e-005
    #     wanted[25] = -9.93831e-006
    #     wanted[26] = -1.30259e-007
    #     wanted[27] = 0.00149362
    #     wanted[28] = 0.000624771
    #     wanted[29] = 0.0010404
    #     wanted[30] = 7.09221e-005
    #     wanted[31] = 0.00104711
    #     wanted[32] = 0.000124917
    #     wanted[33] = 0.000231396
    #     wanted[34] = 6.27575e-005
    #     wanted[35] = 2.07126e-005
    #     wanted[36] = 0.000231654
    #     wanted[37] = -0.00100376
    #     wanted[38] = 0
    #     wanted[39] = 0
    #     wanted[40] = 0
    #     wanted[41] = 0
    #     wanted[42] = 0
    #     wanted[43] = 0
    #     wanted[44] = 0
    #     wanted[45] = 0
    #     wanted[46] = 0
    #     wanted[47] = 0
    #     wanted[48] = 0
    #     wanted[49] = 0
    #     wanted = np.round(wanted, 10)
    #
    #
    #     self.assertEquals(res, wanted)

# class teprob_tests(unittest.TestCase):

    # def tefunc_test(self):
    #     MD = temex_structures.ModelData()
    #     tp = teprob_mod.teprob_mod()
    #     inputs = [[1,2,3,4,5,6,7,8], 1, 1, 0]
    #     res = tp.teinit(MD, 0, 0, np.zeros(50), np.zeros(50), 3.47, 0)
    #     print(res)



    # def test_tesub1_equal_ity0(self):
    #     MD = temex_structures.ModelData()
    #     tp = teprob_mod.teprob_mod()
    #     inputs = [[1,2,3,4,5,6,7,8], 1, 1, 0]
    #     MD = tp.teinit(MD, 0, 0, np.zeros(50), np.zeros(50), 3.47, 0)
    #     res = tp.tesub1_(MD, inputs[0], inputs[1], inputs[2], inputs[3])
    #     res = np.round(res, 6)
    #     wanted = 0.0019632
    #     self.assertEquals(res, wanted)
    #
    # def test_tesub1_equal_ity2(self):
    #     MD = temex_structures.ModelData()
    #     tp = teprob_mod.teprob_mod()
    #     inputs = [[1,2,3,4,5,6,7,8], 1, 1, 2]
    #     MD = tp.teinit(MD, 0, 0, np.zeros(50), np.zeros(50), 3.47, 0)
    #     res = tp.tesub1_(MD, inputs[0], inputs[1], inputs[2], inputs[3])
    #     res = np.round(res, 6)
    #     wanted = 0.318667
    #     self.assertEquals(res, wanted)
    #
    # def test_tesub2_equal(self):
    #     z = [0.3457, 1.435345, -10563.2654, 1000.457456, -2.654, 0, 34.4562, 0.25235]
    #     t = -0.43252
    #     h = 0
    #     ity = 2
    #     MD = temex_structures.ModelData()
    #     tp = teprob_mod.teprob_mod()
    #     MD = tp.teinit(MD, 0, 0, np.zeros(50), np.zeros(50), 3.47, 0)
    #     res = tp.tesub2_(MD, z, t, h, ity)
    #     res = np.round(res, 4)
    #     wanted = 26.5472
    #     self.assertEquals(res, wanted)
    #
    # def test_tesub3_equal_ity0(self):
    #     z = [3.3457, 1.435345, -10563.2654, 1000.457456, -2.654, 0, 34.4562, 0.25235]
    #     t = -0.43252
    #     dh = 0
    #     ity = 0
    #     MD = temex_structures.ModelData()
    #     tp = teprob_mod.teprob_mod()
    #     MD = tp.teinit(MD, 0, 0, np.zeros(50), np.zeros(50), 3.47, 0)
    #     res = tp.tesub3_(MD, z, t, dh, ity)
    #     res = np.round(res, 6)
    #     wanted = -0.475336
    #     self.assertEquals(res, wanted)
    #
    # def test_tesub3_equal_ity2(self):
    #     z = [3.3457, 1.435345, -10563.2654, 1000.457456, -2.654, 0, 34.4562, 0.25235]
    #     t = -0.43252
    #     dh = 0
    #     ity = 2
    #     MD = temex_structures.ModelData()
    #     tp = teprob_mod.teprob_mod()
    #     MD = tp.teinit(MD, 0, 0, np.zeros(50), np.zeros(50), 3.47, 2)
    #     res = tp.tesub3_(MD, z, t, dh, ity)
    #     res = np.round(res, 6)
    #     wanted = -0.111435
    #     self.assertEquals(res, wanted)
    #
    # def test_tesub4_equal(self):
    #     x = [3.3457, 1.435345, 7.34634, 1000.457456, -2.654, 0, 34.4562, 0.25235]
    #     r = 4.578
    #     t = 5.103
    #     MD = temex_structures.ModelData()
    #     tp = teprob_mod.teprob_mod()
    #     MD = tp.teinit(MD, 0, 0, np.zeros(50), np.zeros(50), 3.47, 2)
    #     res = tp.tesub4_(MD, x, t, r)
    #     res = np.round(res, 9)
    #     wanted = 0.000593751
    #     self.assertEquals(res, wanted)
    #
    # def test_tesub5_equal(self):
    #     s = 2
    #     sp = 2
    #     adist = 2
    #     bdist = 2
    #     cdist = 2
    #     ddist = 2
    #     tlast = 2
    #     tnext = 2
    #     hspan = 2
    #     hzero = 2
    #     sspan = 2
    #     szero = 2
    #     spspan = 2
    #     idvflag = 4
    #     MD = temex_structures.ModelData()
    #     tp = teprob_mod.teprob_mod()
    #     MD = tp.teinit(MD, 0, 0, np.zeros(50), np.zeros(50), 3.47, 2)
    #     # temex.teinit(0, 0, np.zeros(50), np.zeros(50))
    #     res = tp.tesub5_(MD, s, sp, adist, bdist,
		# 	cdist, ddist, tlast,
		# 	tnext, hspan, hzero,
		# 	sspan, szero, spspan,
		# 	idvflag)
    #     res = np.array(res)
    #     res = np.round(res, 5)
    #
    #
    #     # wanted = np.array([-17.62247, 9.14028, 3.33047])
    #     wanted = np.array([2,2,-26448,587483,2])
    #     self.assertTrue((res == wanted).all())
    #
    # def test_tesub6_equal(self):
    #     std = 5
    #     x = 4
    #     MD = temex_structures.ModelData()
    #     tp = teprob_mod.teprob_mod()
    #     MD = tp.teinit(MD, 0, 0, np.zeros(50), np.zeros(50), 3.47, 2)
    #     # temex.teinit(0, 0, np.zeros(50), np.zeros(50))
    #     res = tp.tesub6_(MD, std, x)
    #     res = np.round(res, 4)
    #     wanted = -29.5526
    #     self.assertEquals(res, wanted)
    #
    # def test_tesub7_equal_negative_t(self):
    #     i = -1
    #     # temex.teinit(0, 0, np.zeros(50), np.zeros(50))
    #     MD = temex_structures.ModelData()
    #     tp = teprob_mod.teprob_mod()
    #     MD = tp.teinit(MD, 0, 0, np.zeros(50), np.zeros(50), 3.47, 2)
    #     res = tp.tesub7_(MD, i)
    #     res = np.round(res, 6)
    #     wanted = -0.985088
    #     self.assertEquals(res, wanted)
    #
    # def test_tesub8_equal(self):
    #     i = 1
    #     t = 1
    #     # temex.teinit(0, 0, np.zeros(50), np.zeros(50))
    #     MD = temex_structures.ModelData()
    #     tp = teprob_mod.teprob_mod()
    #     MD = tp.teinit(MD, 0, 0, np.zeros(50), np.zeros(50), 3.47, 2)
    #     res = tp.tesub8_(MD, i, t)
    #     wanted = 0.485
    #     self.assertEquals(res, wanted)

if __name__ == '__main__':
    unittest.main()