import numpy as np

class const_class:
    avp = []
    bvp = []
    cvp = []
    ah = []
    bh = []
    ch = []
    ag = []
    bg = []
    cg = []
    av = []
    ad = []
    bd = []
    cd = []
    xmw = []

    # avp = np.zeros(8)
    # bvp = np.zeros(8)
    # cvp = np.zeros(8)
    # ah = np.zeros(8)
    # bh = np.zeros(8)
    # ch = np.zeros(8)
    # ag = np.zeros(8)
    # bg = np.zeros(8)
    # cg = np.zeros(8)
    # av = np.zeros(8)
    # ad = np.zeros(8)
    # bd = np.zeros(8)
    # cd = np.zeros(8)
    # xmw = np.zeros(8)

    def __init__(self):
        self.avp = np.zeros(8)
        self.bvp = np.zeros(8)
        self.cvp = np.zeros(8)
        self.ah = np.zeros(8)
        self.bh = np.zeros(8)
        self.ch = np.zeros(8)
        self.ag = np.zeros(8)
        self.bg = np.zeros(8)
        self.cg = np.zeros(8)
        self.av = np.zeros(8)
        self.ad = np.zeros(8)
        self.bd = np.zeros(8)
        self.cd = np.zeros(8)
        self.xmw = np.zeros(8)

class randsd_class:
    g = 0
    def __init__(self):
        pass

class wlk_class:
    adist = []
    bdist = []
    cdist = []
    ddist = []
    tlast = []
    tnext = []
    hspan = []
    hzero = []
    sspan = []
    szero = []
    spspan = []
    # integer
    idvwlk = []
    rdumm = 0

    # adist = np.zeros(12)
    # bdist = np.zeros(12)
    # cdist = np.zeros(12)
    # ddist = np.zeros(12)
    # tlast = np.zeros(12)
    # tnext = np.zeros(12)
    # hspan = np.zeros(12)
    # hzero = np.zeros(12)
    # sspan = np.zeros(12)
    # szero = np.zeros(12)
    # spspan = np.zeros(12)
    # idvwlk = np.array(np.zeros(12), 'i')

    def __init__(self):
        self.adist = np.zeros(12)
        self.bdist = np.zeros(12)
        self.cdist = np.zeros(12)
        self.ddist = np.zeros(12)
        self.tlast = np.zeros(12)
        self.tnext = np.zeros(12)
        self.hspan = np.zeros(12)
        self.hzero = np.zeros(12)
        self.sspan = np.zeros(12)
        self.szero = np.zeros(12)
        self.spspan = np.zeros(12)
        self.idvwlk = np.array(np.zeros(12), 'i')

class dvec_class:
    # integer
    idv = []
    # idv = np.array(np.zeros(21), 'i')

    def __init__(self):
        self.idv = np.array(np.zeros(21), 'i')

class teproc_class:
    uclr =[]
    ucvr = []
    utlr = 0
    utvr = 0
    xlr = []
    xvr = []
    etr = 0
    esr = 0
    tcr = 0
    tkr = 0
    dlr = 0
    vlr = 0
    vvr = 0
    vtr = 0
    ptr = 0
    ppr = []
    crxr = []
    rr = []
    rh = 0
    fwr = 0
    twr = 0
    qur = 0
    hwr = 0
    uar = 0
    ucls = []
    ucvs = []
    utls = utvs = 0
    xls = []
    xvs = []
    ets = ess= tcs= tks= dls= vls= vvs= vts= pts=0
    pps=[]
    fws= tws= qus= hws=0
    uclc=[]
    utlc=0
    xlc=[]
    etc= esc= tcc=0
    dlc= vlc= vtc= quc=0
    ucvv = []
    utvv=0
    xvv=[]
    etv= esv= tcv= tkv=0
    vtv= ptv = 0
    vcv= []
    vrng= []
    vtau= []
    ftm= []
    fcm=[]
    xst= []
    xmws=[]
    hst= []
    tst= []
    sfr=[]
    cpflmx= cpprmx= cpdh= tcwr= tcws=0
    htr=[]
    agsp=0
    xdel= []
    xns=[]
    tgas= tprod=0
    vst=[]
    # integer
    ivst=[]

    def __init__(self):
        self.uclr= np.zeros(8)
        self.ucvr= np.zeros(8)
        self.xlr= np.zeros(8)
        self.xvr = np.zeros(8)
        self.ppr= np.zeros(8)
        self.crxr= np.zeros(8)
        self.rr = np.zeros(4)
        self.ucls= np.zeros(8)
        self.ucvs= np.zeros(8)
        self.xls= np.zeros(8)
        self.xvs= np.zeros(8)
        self.pps= np.zeros(8)
        self.uclc= np.zeros(8)
        self.xlc= np.zeros(8)
        self.ucvv= np.zeros(8)
        self.xvv=np.zeros(8)
        self.vcv= np.zeros(12)
        self.vrng= np.zeros(12)
        self.vtau=np.zeros(12)
        self.ftm=np.zeros(13)
        self.fcm= np.zeros(104)
        self.xst=np.zeros(104)
        self.xmws= np.zeros(13)
        self.hst= np.zeros(13)
        self.tst=np.zeros(13)
        self.sfr=np.zeros(8)
        self.htr=np.zeros(3)
        self.xdel=np.zeros(41)
        self.xns =np.zeros(41)
        self.vst = np.zeros(12)
        self.ivst = np.array(np.zeros(12), 'i')

class pv_class:
    xmeas = []
    xmv = []

    def __init__(self):
        self.xmeas = np.zeros(41)
        self.xmv = np.zeros(12)


class global_vars:
    idv = 0
    isd = 0
