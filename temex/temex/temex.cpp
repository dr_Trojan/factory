// temex.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "math.h"
//#include "simstruc.h"
#include "teprob.h"
#include "temexd.h"

#include <iostream>

double *y;
int nn;
double *yp;
double time;


void tesub1_try_func()
{
	std::cout << "tesub1_try_func" << std::endl;

	double *z_ = new double[8]{1,2,3,4,5,6,7,8};
	double t = 1;
	double h_ = 1;
	int ity = 0;

	int res = tesub1_(z_, &t, &h_, &ity);
	std::cout << h_ << std::endl;

	delete(z_);
}

void tesub2_try_func()
{
	std::cout << "tesub2_try_func" << std::endl;

	double *z = new double[8]{0.3457, 1.435345, -10563.2654, 1000.457456, -2.654, 0, 34.4562, 0.25235};
	double t = -0.43252;
	double h = 0;
	int ity = 2;

	int res = tesub2_(z, &t, &h, &ity);

	std::cout << t << std::endl;

	delete(z);
}

void tesub3_try_func()
{
	std::cout << "tesub3_try_func" << std::endl;

	double *z = new double[8]{3.3457, 1.435345, -10563.2654, 1000.457456, -2.654, 0, 34.4562, 0.25235};
	double t = -0.43252;
	double dh = 0;
	int ity = 0;

	int res = tesub3_(z, &t, &dh, &ity);

	std::cout << dh << std::endl;

	delete(z);
}

void tesub4_try_func()
{
	std::cout << "tesub4_try_func" << std::endl;

	double *x = new double[8]{3.3457, 1.435345, 7.34634, 1000.457456, -2.654, 0, 34.4562, 0.25235};
	double r = 4.578;
	double t = 5.103;

	int res = tesub4_(x, &t, &r);

	std::cout << r << std::endl;

	delete(x);
}

void tesub5_try_func()
{
	std::cout << "tesub5_try_func" << std::endl;

	double s = 2;
	double sp = 2;
	double adist = 2;
	double bdist = 2;
	double cdist = 2;
	double ddist = 2;
	double tlast = 2;
	double tnext = 2;
	double hspan = 2;
	double hzero = 2;
	double sspan = 2;
	double szero = 2;
	double spspan = 2;
	int idvflag = 4;

	int res = tesub5_(&s, &sp, &adist, &bdist,
		&cdist, &ddist, &tlast,
		&tnext, &hspan, &hzero,
		&sspan, &szero, &spspan,
		&idvflag);

	std::cout << s << std::endl;
	std::cout << sp << std::endl;
	std::cout << adist << std::endl;
	std::cout << bdist << std::endl;
	std::cout << cdist << std::endl;
	std::cout << ddist << std::endl;
	std::cout << tlast << std::endl;
	std::cout << tnext << std::endl;
	std::cout << hspan << std::endl;
	std::cout << hzero << std::endl;
	std::cout << sspan << std::endl;
	std::cout << szero << std::endl;
	std::cout << spspan << std::endl;
	std::cout << idvflag << std::endl;
}

void tesub6_try_func()
{
	std::cout << "tesub6_try_func" << std::endl;

	double std = 5;
	double x = 4;
	int res = tesub6_(&std, &x);
	std::cout << x << std::endl;
}

void tesub7_try_func()
{
	std::cout << "tesub7_try_func" << std::endl;

	int i = -1;
	double res = tesub7_(&i);
	std::cout << res << std::endl;
}

void tesub8_try_func()
{
	std::cout << "tesub8_try_func" << std::endl;

	int i = 1;
	double t = 1;

	double res = tesub8_(&i, &t);
	std::cout << res << std::endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	nn = 50.;
	time = 12.;
	y = new double[50];
	yp = new double[50];
	teinit(&nn, &time, y, yp);

	//tesub1_try_func();
	//tesub2_try_func();
	//tesub3_try_func();
	//tesub4_try_func();
	//tesub5_try_func();
	//tesub6_try_func();
	//tesub7_try_func();
	//tesub8_try_func();

	for (int i = 0; i < 50; i++)
	{
		std::cout << "yp[" << i << "] = " << yp[i] << std::endl;
	}

	std::cin.ignore();

	return 0;
}
